<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'divemusic' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'l*/EZ&qi+{=F l5K@}[LL3a0KIO~zjC|vTYc:U)N>7%._6Q}Hwxg;fihUED<$}eC' );
define( 'SECURE_AUTH_KEY',  'LFiW3(.MMUGclC,Q$fZGEZ8El$KB9H1pG1DAquYRFhZ.RIw+J!|SI.U$-fW:xg};' );
define( 'LOGGED_IN_KEY',    'A @ey$w#U=.T3fgfxm>t=mIip;kcmBpD^S{S>N$td<Pi}ed!/kN2k}R&%!vACDN5' );
define( 'NONCE_KEY',        'QGh`<=Q+3e1trm<z<F[`X]D%iMD8?-gKYkP6@)_Q55xu3xTU.za%sWe#APL+Y?AS' );
define( 'AUTH_SALT',        '-{37v.cKSBc!jm{G/<_7f0.W[}vymAWS}g:O-yA2;`g/LrlHgoGo~}I*<!DJ0vu+' );
define( 'SECURE_AUTH_SALT', '{K)^LGs)O2.+,x{Xx=)^8?ruvI8rJh4RiwPib9<]^~`m2K4dAQl4TJcx8nitC)mM' );
define( 'LOGGED_IN_SALT',   'gE*/;j~S~Gm99IzQv(~9lxyC.TJUs $%TGZl<^<j5t7VLIeA/J>9lyQ&lPPRGFNV' );
define( 'NONCE_SALT',       'hB~=FwJBbxp})$h,O7mb[;&><Ke$O}K,3kauMTa~*!/z|XzA xbf[}0rq3@x{H,#' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define('WP_MEMORY_LIMIT', '2048M');
/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       sonaar.io
 * @since      1.0.0
 *
 * @package    Sonaar_Music_Pro
 * @subpackage Sonaar_Music_Pro/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Sonaar_Music_Pro
 * @subpackage Sonaar_Music_Pro/public
 * @author     Edouard Duplessis <eduplessis@gmail.com>
 */
class Sonaar_Music_Pro_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Sonaar_Music_Pro_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Sonaar_Music_Pro_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_register_style( 'sonaar-music-pro', plugin_dir_url( __FILE__ ) . 'css/sonaar-music-pro-public.css', array(), $this->version, 'all' );
		/* Enqueue Sonnar Music css file on single Album Page */
		if ( is_single() && get_post_type() == 'album' ) {
			wp_enqueue_style( 'sonaar-music-pro' );
		}

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		global $post, $wp;
		
		/* return script when page  not found (404) */
		if ( is_404() ) {
			return;
		}
		wp_register_script( 'sonaar-music-pro', plugin_dir_url( __FILE__ ) . 'js/sonaar-music-pro-public.js', array( 'jquery' ), $this->version, false );
		wp_register_script( 'sonaar-music-scrollbar', plugin_dir_url( __FILE__ ) . 'js/perfect-scrollbar.min.js', array( 'jquery' ), $this->version, false );
		wp_deregister_script( 'sonaar-music-mp3player' );
		wp_register_script( 'sonaar-music-pro-mp3player', plugin_dir_url( __FILE__ ) . 'js/iron-audioplayer/iron-audioplayer.js', array( 'jquery', 'sonaar-music', 'sonaar-music-pro' ,'moments', 'wave', 'jquery-ui-slider' ), $this->version, true );
			
		wp_localize_script( $this->plugin_name . '-mp3player', 'sonaar_music', array(
			'plugin_dir_url'=> plugin_dir_url( __FILE__ ),
			'option' => Sonaar_Music::get_option( 'allOptions' ),
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'current_page' => array(
				'title' => get_the_title( $post->ID ),
				'url' => home_url($_SERVER['REQUEST_URI'])
			)
		));

		//--Player footer----------------
		wp_register_script( 'vuejs', '//cdn.jsdelivr.net/npm/vue@2/dist/vue.min.js' , array(), NULL, true );


		// Register the script
		wp_register_script( 'sonaar_player', plugin_dir_url( __FILE__ ) . 'js/sonaarPlayer.js', array( 'jquery', 'sonaar-music-pro', 'sonaar-music-scrollbar' ,'moments', 'wave','sonaar-music-pro-mp3player','vuejs' ), $this->version, true  );

		// Localize the script with new data
		$sonaar_player_data = array(
			'sonaar_music' => array(
				'currentPostId' => get_the_ID(),
				'color_base' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('music_player_timeline_color', '_iron_music_music_player_options') : '',
				'color_progress' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('music_player_progress_color', '_iron_music_music_player_options') : '',
				'continuous_background' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('continuous_music_player_background', '_iron_music_music_player_options') : 'rgb(0,0,0)',
				'continuous_timeline_background' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('continuous_music_player_timeline_bar', '_iron_music_music_player_options') : 'rgb(255,255,255)',
				'continuous_progress_bar' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('continuous_music_player_progress_bar', '_iron_music_music_player_options') : 'rgb(155,155,155)',
				'continuous_control_color' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('continuous_music_player_control_color', '_iron_music_music_player_options') : 'rgb(255,255,255)',
				'continuous_artist_name' => Sonaar_Music::get_option('show_artist_name'),
				'continuous_playlist_icon' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('continuous_music_player_playlist_icon', '_iron_music_music_player_options') : '',
				'footer_albums' => get_post_meta( $post->ID, 'sonaar_footer_player_meta', true ),
				'footer_albums_shuffle' => get_post_meta( $post->ID, 'sonaar_footer_player_shuffle_meta', true),
				'footer_podcast' => '',// ( Iron_sonaar::getField('footer_podcast', get_the_ID()) == "" || Iron_sonaar::getField('footer_podcast', get_the_ID()) == "null" )? (bool) false : (int) Iron_sonaar::getField('footer_podcast', get_the_ID()),
				'podcast_color_base' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('podcast_player_timeline_color', '_iron_music_podcast_player_options') : '',
				'podcast_color_progress' => ( function_exists('get_ironMusic_option') )? get_ironMusic_option('podcast_player_progress_color', '_iron_music_podcast_player_options') : '',
				'podcast_hide_duration' => (bool)( function_exists('get_ironMusic_option') )? get_ironMusic_option('podcast_label_duration', '_iron_music_podcast_player_options') : '',
				'podcast_hide_category' => (bool)( function_exists('get_ironMusic_option') )? get_ironMusic_option('podcast_label_category', '_iron_music_podcast_player_options') : '',
			),
			'site_url'=> esc_url( home_url('/') )
		);
		wp_localize_script( 'sonaar_player', 'iron_vars', $sonaar_player_data );

		// Enqueued script with localized data.
		

		/* Enqueue Sonnar Music Pro mp3player js file on single Album Page */
		if ( ( is_single() && get_post_type() == 'album' ) || class_exists('\abs\Abs_Plugin' ) ){
			wp_enqueue_script( 'sonaar-music-pro-mp3player' );
			wp_enqueue_script( 'sonaar_player' );
			add_action('wp_footer','sonaar_player', 12);
		}

		/* Enqueue Sonaar Music Pro mp3player js file Conditions */;
		include_once(ABSPATH.'wp-admin/includes/plugin.php');
		$condition_1 = get_post_meta( $post->ID, 'sonaar_footer_player_meta', true ) != '' && get_post_meta( $post->ID, 'sonaar_footer_player_meta', true ) != ['']; // when the option "Display Audio player footer" is enable  
		$condition_2 = class_exists('\abs\Abs_Plugin' );
		$condition_3 = Sonaar_Music::get_option('overall_sticky_playlist') !=''; // when the global player option is enable  
		$condition_4 = Sonaar_Music::get_option('enable_continuous_player') == 'true' && isset($_COOKIE['sonaar_mp3_player_settings']); // when the continuous player is enable and a cookie is already set.  
		if( $condition_1 || $condition_2 || $condition_3 || $condition_4 ){
			wp_enqueue_style( 'sonaar-music' );
			wp_enqueue_style( 'sonaar-music-pro' );
			wp_enqueue_script( 'sonaar-music-mp3player' );
			wp_enqueue_script( 'sonaar-music-pro-mp3player' );
			wp_enqueue_script( 'sonaar_player' );
			if ( function_exists('sonaar_player') ) {
				add_action('wp_footer','sonaar_player', 12);
			}
		}



	}

}

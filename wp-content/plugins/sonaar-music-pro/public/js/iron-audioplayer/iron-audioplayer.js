var elementAudio = document.createElement("audio");
elementAudio.crossOrigin = "anonymous";
//elementAudio.src = sonaar_music.plugin_dir_url + '/js/assets/default.wav';

var elementAudioSingle = document.createElement("audio");
elementAudioSingle.crossOrigin = "anonymous";
//elementAudioSingle.src = sonaar_music.plugin_dir_url + '/js/assets/default.wav';

IRON.audioPlayer = (function ($) {
  "use strict";
  var seekTimeOut;
  var autoplayEnable;
  var audioPlayer;
  var playlist;
  var soundwaveColorBG;
  var soundwaveProgressColor;
  var stickyEnable = false;
  var wavesurferEnable = Boolean(sonaar_music.option.waveformType === "wavesurfer");
  var random_order = false;

  function initPlayer(player) {
    audioPlayer = player;
    $(audioPlayer).css("opacity", 1);
    this.audioPlayer = player;
    var waveContainer = this.audioPlayer.find(".player .wave").attr("id");
    playlist = audioPlayer.find(".playlist");
    this.playlist = playlist;
    this.autoplayEnable = audioPlayer.data("autoplay");
    audioPlayer.id = audioPlayer.data("id");
    audioPlayer.albums = audioPlayer.data("albums");
    audioPlayer.remove_wave = audioPlayer.data("no-wave");
    audioPlayer.shuffle = audioPlayer.data("shuffle") || false;
    audioPlayer.stickyPlayer = IRON.audioPlayer.stickyEnable;
    audioPlayer.notrackskip = audioPlayer.data("notrackskip");
    audioPlayer.hide_progressbar = audioPlayer.data("hide-progressbar") ? true : false;
    if (audioPlayer.shuffle && !audioPlayer.stickyPlayer) {
      random_order = setRandomList(playlist.find("li"));
    }
    if (audioPlayer.data("scrollbar")) {
      var scrollItem = $(audioPlayer).find(".playlist ul")[0];

      var ps = new PerfectScrollbar(scrollItem, {
        wheelSpeed: 0.7,
        swipeEasing: true,
        wheelPropagation: false,
        minScrollbarLength: 20,
        suppressScrollX: true,
      });
      //$('.iron-audioplayer .playlist ul').perfectScrollbar({'suppressScrollX': true});
    }
    if (audioPlayer.data("wave-color") == false) {
      soundwaveColorBG = sonaar_music.option.music_player_timeline_color;
    } else {
      soundwaveColorBG = audioPlayer.data("wave-color");
    }
    if (audioPlayer.data("wave-progress-color") == false) {
      soundwaveProgressColor = sonaar_music.option.music_player_progress_color;
    } else {
      soundwaveProgressColor = audioPlayer.data("wave-progress-color");
    }

    if (wavesurferEnable) {
      var wavesurfer = WaveSurfer.create({
        container: "#" + waveContainer,
        cursorWidth: 0,
        barWidth: 1,
        progressColor: soundwaveProgressColor,
        waveColor: soundwaveColorBG,
        height: 70,
        mediaControls: true,
        backend: "MediaElement",
        mediaControls: false,
      });

      var firstLoad = true;
      wavesurfer.on("ready", function () {
        if (firstLoad) {
          firstLoad = false;
        } else {
          wavesurfer.play();
          $(".sr_selected").addClass("audio-playing");
        }
        audioPlayer.find(".player").addClass("reveal");
        $(".wave").addClass("reveal");
        audioPlayer.find(".progressLoading").css("opacity", "0");
      });
      var currentPlayer = audioPlayer;
      wavesurfer.on("audioprocess", function () {
        var currentTime = wavesurfer.getCurrentTime();
        var duration = wavesurfer.getDuration();
        var time = moment.duration(currentTime, "seconds");

        currentTime = moment(time.minutes() + ":" + time.seconds(), "m:s").format("mm:ss");

        if (duration !== Infinity) {
          var totalTime = moment.duration(wavesurfer.getDuration() - wavesurfer.getCurrentTime(), "seconds");
          if (totalTime.hours() >= 12 || totalTime.hours() <= 0) {
            currentPlayer.find(".totalTime").html("-" + moment(totalTime.minutes() + ":" + totalTime.seconds(), "m:s").format("mm:ss"));
          } else {
            currentPlayer.find(".totalTime").html("-" + moment(totalTime.hours() + ":" + totalTime.minutes() + ":" + totalTime.seconds(), "h:m:s").format("h:mm:ss"));
          }
        } else {
          currentPlayer.find(".totalTime").html(this.list.tracks[this.currentTrack].length);
        }
        currentPlayer.find(".currentTime").html(moment(time.minutes() + ":" + time.seconds(), "m:s").format("mm:ss"));
      });

      wavesurfer.on("loading", function () {
        var progressLoad = arguments[0];
        audioPlayer.find(".progressLoading").css("background", sonaar_music.option.music_player_timeline_color);
        audioPlayer.find(".progressLoading").css("width", "calc( " + progressLoad + "% - 200px )");
      });
    } else {
      var wavesurfer = $("#" + waveContainer).find(".sonaar_media_element")[0];

      fakeWaveUpdate(wavesurfer, audioPlayer, playlist);
      $(audioPlayer).find(".wave").css("opacity", "1");
    }

    this.wavesurfer = wavesurfer;

    setPlaylist(playlist, wavesurfer, audioPlayer);

    if (audioPlayer.shuffle) {
      var trackNumber = Math.floor(Math.random() * playlist.find("li").length);
    } else {
      var trackNumber = playlist.find("li").index();
    }
    var track = playlist.find("li").eq(trackNumber);
    $(audioPlayer).attr("trackselected", trackNumber);
    setCurrentTrack(track, trackNumber, audioPlayer, wavesurfer);
    updatePlaylistTitle(audioPlayer, track.attr("data-albumtitle"));
    if (!audioPlayer.remove_wave && wavesurferEnable) {
      setAudio(playlist.find("li").eq(trackNumber).data("audiopath"), wavesurfer);
    }
    setControl(wavesurfer, audioPlayer, playlist);
    if (wavesurferEnable) {
      setNextSong(wavesurfer, audioPlayer, playlist);
    }

    if (audioPlayer.data("autoplay")) {
      autoplayEnable = true;
    }
    sr_playerCTAresponsive();
  }

  var setNextSong = function (wavesurfer, audioPlayer, playlist) {
    wavesurfer.on("finish", function () {
      if (audioPlayer.notrackskip === "on") {
        wavesurfer.seekTo(0);
        IRON.sonaar.player.wavesurfer.seekTo(0);
        togglePlaying(audioPlayer, wavesurfer);
        IRON.sonaar.player.wavesurfer.pause();
      } else if (!audioPlayer.stickyPlayer) {
        next(audioPlayer, wavesurfer, playlist);
      }
    });
  };

  var triggerPlay = function (wavesurfer, audioPlayer) {
    if (wavesurferEnable) {
      if (!wavesurfer.isPlaying()) togglePlaying(audioPlayer, wavesurfer);
    }
  };
  function setCurrentTrack(track, index, audioPlayer, wavesurfer) {
    var albumArt = audioPlayer.find(".album .album-art");
    var album = audioPlayer.find(".album");
    var trackTitle = audioPlayer.find(".track-title");
    var trackArtist = audioPlayer.find(".sr_it-artists-value");
    var albumReleaseDate = audioPlayer.find(".sr_it-date-value");

    if (track.data("albumart")) {
      albumArt.show();
      album.show();
      albumArt.css("cursor", "pointer");
      if (albumArt.find("img").length) {
        albumArt.find("img").attr("src", track.data("albumart"));
      } else {
        albumArt.css("background-image", "url(" + track.data("albumart") + ")");
      }
    } else {
      album.hide();
      albumArt.hide();
    }
    if (!audioPlayer.hasClass("show-playlist")) {
      albumArt.css("cursor", "pointer");
    }
    audioPlayer.data("currentTrack", index);

    trackTitle.text(track.data("tracktitle"));
    trackArtist.text(track.data("trackartists"));
    albumReleaseDate.text(track.data("releasedate"));
    audioPlayer.find(".player").removeClass("hide");
    audioPlayer.find(".wave").removeClass("reveal");

    if (!track.data("showloading")) {
      audioPlayer.find(".player").addClass("hide");
    } else {
      audioPlayer.find(".progressLoading").css("opacity", "0.75");
    }

    if (!audioPlayer.stickyPlayer && wavesurferEnable) {
      setAudio(track.data("audiopath"), wavesurfer);
    }
    setTime(audioPlayer, wavesurfer);

    hideEmptyAttribut(track.data("releasedate"), audioPlayer.find(".sr_it-playlist-release-date"));

    if (!wavesurferEnable && ($(audioPlayer).find(".sonaar_fake_wave svg").html() == "" || !audioPlayer.stickyPlayer)) {
      //Create FakeWave if the widget doesnt have OR if the stickyPlayer is disable
      createFakeWave(audioPlayer);
    }

    if (!audioPlayer.stickyPlayer) {
      updatePlaylistTitle(audioPlayer, track.attr("data-albumtitle"));
    }
  }

  function updatePlaylistTitle(audioPlayer, playlistTitle) {
    var playlistTitleElement = audioPlayer.find(".sr_it-playlist-title");
    if (typeof audioPlayer.data("playlist_title") !== "undefined" && audioPlayer.data("playlist_title").length) {
      playlistTitleElement.text(audioPlayer.data("playlist_title"));
    } else {
      playlistTitleElement.text(playlistTitle);
    }
  }

  function setRandomList(tracks) {
    var poolTrack = new Array();
    var i = 0;
    $(tracks).each(function () {
      poolTrack.push(i);
      i++;
      poolTrack = poolTrack.sort(function (a, b) {
        return 0.5 - Math.random();
      });
    });
    return poolTrack;
  }

  function setPlaylist(playlist, wavesurfer, audioPlayer) {
    playlist.find("li").each(function () {
      setSingleTrack($(this), $(this).index(), wavesurfer, audioPlayer);
    });
  }

  function setTime(audioPlayer, wavesurfer) {
    if (!wavesurferEnable) {
      $(wavesurfer).on("timeupdate", function () {
        var currentTime = wavesurfer.currentTime;
        var time = moment.duration(currentTime, "seconds");
        audioPlayer.find(".currentTime").html(moment(time.minutes() + ":" + time.seconds(), "m:s").format("mm:ss"));
        if (wavesurfer.duration !== Infinity) {
          var timeLeft = moment.duration(wavesurfer.duration - wavesurfer.currentTime, "seconds");
          audioPlayer.find(".totalTime").html("-" + moment(timeLeft.minutes() + ":" + timeLeft.seconds(), "m:s").format("mm:ss"));
        } else {
          audioPlayer.find(".totalTime").html("");
        }
      });
    }
  }

  function setControl(wavesurfer, audioPlayer, playlist) {
    if (audioPlayer.stickyPlayer) {
      audioPlayer.on("click", ".play, .album .album-art", function (event) {
        if ($(audioPlayer).hasClass("sr_selectedPlayer")) {
          IRON.sonaar.player.play();
        } else {
          play(audioPlayer, wavesurfer, playlist);
        }
        event.preventDefault();
      });
      audioPlayer.on("click", ".previous", function (event) {
        previous(audioPlayer, wavesurfer, playlist);
        event.preventDefault();
      });
      audioPlayer.on("click", ".next", function (event) {
        next(audioPlayer, wavesurfer, playlist);
        event.preventDefault();
      });
    } else {
      audioPlayer.on("click", ".play, .album .album-art", function (event) {
        togglePause(audioPlayer);

        if (!audioPlayer.hasClass("audio-playing")) {
          play(audioPlayer, wavesurfer);
          triggerPlay(wavesurfer, audioPlayer);
        } else {
          togglePause(audioPlayer);
        }
        togglePlaying(audioPlayer, wavesurfer);
        event.preventDefault();
      });
      audioPlayer.on("click", ".previous", function (event) {
        previous(audioPlayer, wavesurfer, playlist);
        event.preventDefault();
      });
      audioPlayer.on("click", ".next", function (event) {
        next(audioPlayer, wavesurfer, playlist);
        event.preventDefault();
      });
    }
  }

  function fakeWaveClick(from) {
    $(".sr_selectedPlayer .sonaar_fake_wave, #sonaar-player .sonaar_fake_wave, .mobileProgress").on("click", function (event) {
      if (from == "sticky") {
        var currentAudio = document.getElementById("sonaar-audio");
      } else {
        var currentAudio = $(this).find(".sonaar_media_element")[0];
      }
      var progressedAudio = $(this).width() / event.offsetX;
      currentAudio.currentTime = currentAudio.duration / progressedAudio;
      event.preventDefault();
    });
  }

  function setSingleTrack(singleTrack, eq, wavesurfer, audioPlayer) {
    singleTrack.find(".audio-track").remove();
    var tracknumber = eq + 1;
    var trackplay = $("<span/>", {
      class: "track-number",
      html:
        '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 17.5 21.2" style="enable-background:new 0 0 17.5 21.2;" xml:space="preserve"><path d="M0,0l17.5,10.9L0,21.2V0z"></path><rect width="6" height="21.2"></rect><rect x="11.5" width="6" height="21.2"></rect></svg><span class="number">' +
        tracknumber +
        "</span>",
    });
    $("");
    $("<a/>", {
      class: "audio-track",

      click: function (event) {
        if ($(this).parent().attr("data-audiopath").length == 0) {
          return;
        }

        if (audioPlayer.stickyPlayer) {
          IRON.sonaar.player.setPlaylist(audioPlayer, eq);
          audioPlayer.data("currentTrack", eq);
        }

        if (ifTrackIsPlaying(wavesurfer) && singleTrack.hasClass("current")) {
          if (audioPlayer.stickyPlayer) {
            play(audioPlayer, wavesurfer);
          } else {
            togglePause(audioPlayer);
            togglePlaying(audioPlayer, wavesurfer);
          }
        } else if (singleTrack.hasClass("current")) {
          play(audioPlayer, wavesurfer);
        } else {
          if (!audioPlayer.stickyPlayer) {
            togglePause(audioPlayer);
          }

          if (!audioPlayer.stickyPlayer && !wavesurferEnable) {
            if (wavesurfer.currentSrc == "") {
              $(audioPlayer).attr("first-track-loading", "true");
            }
            setAudio(singleTrack.data("audiopath"), wavesurfer);
            wavesurfer.play();
          }

          setCurrentTrack(singleTrack, eq, audioPlayer, wavesurfer);

          audioPlayer.find(".playlist li").removeClass("current");
          singleTrack.addClass("current");
          triggerPlay(wavesurfer, audioPlayer);
          togglePlaying(audioPlayer, wavesurfer);
        }
        event.preventDefault();
      },
    })
      .appendTo(singleTrack)
      .prepend(trackplay)
      .append('<div class="tracklist-item-title">' + singleTrack.data("tracktitle") + ' </div><span class="tracklist-item-time">' + singleTrack.data("tracktime") + "</span>");
    singleTrack.find('.store-list').before(singleTrack.find(".audio-track"));
  }

  function trackListItemResize() {
    $(".playlist li").each(function () {
      var storeWidth = $(this).find(".store-list").outerWidth();
      var trackWidth = $(this).find(".track-number").outerWidth();
      $(this)
        .find(".tracklist-item-title")
        .css("max-width", $(this).outerWidth() - storeWidth - trackWidth - 10);
    });
  }

  var setAudio = function (audio, wavesurfer) {
    $(document).trigger("sonaarStats", [audioPlayer]);
    if (wavesurferEnable) {
      elementAudio.src = audio;
      seekTimeOut = setTimeout(function () {
        if (audioPlayer.stickyPlayer) {
          wavesurfer.load(elementAudio);
        } else {
          wavesurfer.load(audio);
        }
      }, 250);
    } else {
      jQuery(wavesurfer).attr("src", audio);
      wavesurfer.load();
      if (!audioPlayer.stickyPlayer && !wavesurferEnable) {
        $(".sr_selectedPlayer").removeClass("sr_selectedPlayer");
        $(wavesurfer).parents(".iron-audioplayer").addClass("sr_selectedPlayer");
      }
      fakeWaveClick("widget");
    }
  };

  function getTime(wavesurfer) {
    return wavesurfer.getCurrentTime();
  }

  function togglePlaying(audioPlayer, wavesurfer) {
    $(".iron-audioplayer").removeClass("sr_selected");
    audioPlayer.addClass("sr_selected");

    $(".iron-audioplayer").removeClass("audio-playing");

    if (ifTrackIsPlaying(wavesurfer)) {
      audioPlayer.addClass("audio-playing");
      return;
    }
    //audioPlayer.removeClass('audio-playing')
    $("#sonaar-player .play").removeClass("audio-playing");
    // sendData( audioPlayer, wavesurfer );
  }

  function togglePause(audioPlayer) {
    $.each(IRON.players, function (index) {
      if (IRON.players[index] != audioPlayer) {
        IRON.players[index].wavesurfer.pause();
      }
    });
  }

  function play(audioPlayer, wavesurfer, playlist) {
    if (audioPlayer.stickyPlayer) {
      var currentTrack = audioPlayer.data("currentTrack");
      wavesurfer.pause();
      if (typeof playlist !== "undefined") {
        playlist.find("li").eq(currentTrack).find("a").click();
      }
    } else {
      if (!wavesurferEnable && wavesurfer.currentSrc == "") {
        setAudio($(audioPlayer).find("li").eq($(audioPlayer).attr('trackselected')).data("audiopath"), wavesurfer);
      }
      if (!audioPlayer.find(".playlist li").hasClass("current")) {
        $(audioPlayer).find("li").eq($(audioPlayer).attr('trackselected')).addClass("current");
      }
      if (ifTrackIsPlaying(wavesurfer)) {
        wavesurfer.pause();
      } else {
        wavesurfer.play();
      }
      togglePlaying(audioPlayer, wavesurfer);
    }
  }

  /*
  function sendData(audioPlayer) {
    var currentTrack = audioPlayer.find('.playlist .current');

    var data = {
      'action': 'post_stats',
      'post_stats': {
        'target_title': currentTrack.data('tracktitle'),
        'target_url': currentTrack.data('audiopath'),
        'page_title': sonaar_music.current_page.title,
        'page_url': sonaar_music.current_page.url
      }
    }

    jQuery.post(sonaar_music.ajax_url, data, function (response) {

    });
  }
  */

  function previous(audioPlayer, wavesurfer, playlist) {
    var currentTrack = audioPlayer.data("currentTrack");
    var nextTrack;
    if (audioPlayer.shuffle) {
      $(random_order).each(function (index) {
        if (this == currentTrack) {
          if (index == 0) {
            //if it is the first track form the shuffle order
            nextTrack = random_order[random_order.length - 1];
          } else {
            nextTrack = random_order[index - 1];
          }
        }
      });
    } else {
      nextTrack = currentTrack - 1;
    }

    if (wavesurferEnable) {
      if ("2" < getTime(wavesurfer)) {
        wavesurfer.seekTo(0);
        return;
      }
    }
    playlist.find("li").eq(nextTrack).find("a").click();
  }

  function next(audioPlayer, wavesurfer, playlist) {
    var currentTrack = audioPlayer.data("currentTrack");
    var nextTrack;
    if (audioPlayer.shuffle) {
      $(random_order).each(function (index) {
        if (this == currentTrack) {
          if (index >= random_order.length - 1) {
            //if it is the last track form the shuffle order
            nextTrack = random_order[0];
          } else {
            nextTrack = random_order[index + 1];
          }
        }
      });
    } else {
      nextTrack = currentTrack + 1;
    }

    if (!playlist.find("li").eq(nextTrack).length) {
      nextTrack = 0;
    }
    wavesurfer.pause();
    playlist.find("li").eq(nextTrack).find("a").click();
  }

  function getPlayer() {
    return this;
  }
  function getplay() {
    play(this.audioPlayer, this.wavesurfer);
  }

  function ifTrackIsPlaying(wavesurfer) {
    if (wavesurferEnable) {
      return wavesurfer.isPlaying();
    } else {
      return !wavesurfer.paused;
    }
  }

  var fakeWaveUpdate = function (wavesurfer, audioPlayer, playlist) {
    $(wavesurfer).on("timeupdate", function () {
      $(audioPlayer)
        .find(".sonaar_wave_cut")
        .width((this.currentTime / this.duration) * 100 + "%");
      if (wavesurfer.ended) {
        //When track ended
        if (audioPlayer.notrackskip !== "on") {
          next(audioPlayer, wavesurfer, playlist);
        }
      }
    });
  };

  function ajaxInitPage() {
    setIronAudioplayers();
    resetIron_vars();
    setStickyPlayer();
    stickyPlayerFromPageOption();
  }

  return {
    init: initPlayer,
    getPlayer: getPlayer,
    play: getplay,
    autoplayEnable: autoplayEnable,
    triggerPlay: triggerPlay,
    stickyEnable: stickyEnable,
    ajaxInitPage: ajaxInitPage,
    createFakeWave: createFakeWave,
    fakeWaveClick: fakeWaveClick,
    updatePlaylistTitle: updatePlaylistTitle,
    setRandomList: setRandomList,
  };
})(jQuery);

function hideEmptyAttribut(string, selector) {
  if (string == "") {
    selector.css("display", "none");
  } else {
    selector.css("display", "block");
  }
}

//Set Sticky player if it is enable once
function setStickyPlayer() {
  var cookieSettingsValue = getCookieValue("sonaar_mp3_player_settings");
  if (
    (iron_vars.sonaar_music.footer_albums != "" && iron_vars.sonaar_music.footer_albums != [""]) ||
    (sonaar_music.option.overall_sticky_playlist != null && sonaar_music.option.overall_sticky_playlist != [""]) ||
    cookieSettingsValue != ""
  ) {
    IRON.audioPlayer.stickyEnable = true;
  } else {
    jQuery(".iron-audioplayer").each(function () {
      if (jQuery(this).data("sticky-player")) {
        IRON.audioPlayer.stickyEnable = true;
      }
    });
  }
}

//Reset iron_vars on the ajaxify navigation
function resetIron_vars() {
  var str = jQuery('script:contains("var iron_vars")').text();
  if (str !== "") {
    str = str.split("var iron_vars =")[1];
    iron_vars = JSON.parse(str.split(";")[0]);
  }
}

//Load Music player Content
function setIronAudioplayers(specificParentSelector) {
  if (typeof specificParentSelector !== "undefined") {
    // set all audioplayers or only players inside a specific selector
    var playerSelector = jQuery(specificParentSelector + " .iron-audioplayer");
    if (IRON.players == "undefined") {
      IRON.players = []; //dont reset the IRON.players if they already exist and the setIronAudioplayers function is executed from sr-scripts.js
    }
  } else {
    var playerSelector = jQuery(".iron-audioplayer");
    IRON.players = [];
  }

  playerSelector.each(function () {
    if (jQuery(this).parents(".elementor-location-popup").length) return;

    if (typeof specificParentSelector == "undefined" && jQuery(this).parents(".elementor-widget-woocommerce-products").length) return;

    if (typeof specificParentSelector == "undefined" && jQuery(this).parents(".elementor-widget-music-player").length) return;

    var player = Object.create(IRON.audioPlayer);
    player.init(jQuery(this));
    IRON.players.push(player);
  });
}

/*Load audioplayer in elementor Popup*/
jQuery(document).on("elementor/popup/show", (event, id, instance) => {
  jQuery('.elementor-location-popup[data-elementor-id="' + id + '"] .iron-audioplayer').each(function () {
    var player = Object.create(IRON.audioPlayer);
    player.init(jQuery(this));
    IRON.players.push(player);
  });
});
jQuery(document).on("elementor/popup/hide", (event, id, instance) => {
  jQuery('.elementor-location-popup[data-elementor-id="' + id + '"] .iron-audioplayer').each(function () {
    IRON.players.splice(-1, 1);
  });
});

//Display stickyplayer from the page option
function stickyPlayerFromPageOption() {
  if (iron_vars.sonaar_music.footer_albums != "" && !IRON.sonaar.player.isPlaying && !IRON.sonaar.player.classes.continued) {
    if (Array.isArray(IRON.sonaar.player.playlistID)) {
      var currentPlaylist = IRON.sonaar.player.playlistID[0];
    } else {
      var currentPlaylist = IRON.sonaar.player.playlistID;
    }
    if (Array.isArray(iron_vars.sonaar_music.footer_albums)) {
      var newPlaylist = iron_vars.sonaar_music.footer_albums[0];
    } else {
      var newPlaylist = iron_vars.sonaar_music.footer_albums;
    }

    if (newPlaylist != currentPlaylist) {
      IRON.sonaar.player.currentTime = "";
      IRON.sonaar.player.totalTime = "";
      IRON.sonaar.player.setPlayer({
        id: iron_vars.sonaar_music.footer_albums,
        autoplay: false,
        soundwave: true,
        shuffle: iron_vars.sonaar_music.footer_albums_shuffle,
      });
    }
    return;
  }

  //Play from overall settings
  if (sonaar_music.option.overall_sticky_playlist != null && !IRON.sonaar.player.isPlaying && !IRON.sonaar.player.classes.continued) {
    IRON.sonaar.player.currentTime = "";
    IRON.sonaar.player.totalTime = "";
    IRON.sonaar.player.setPlayer({
      id: sonaar_music.option.overall_sticky_playlist,
      autoplay: false,
      soundwave: true,
      shuffle: sonaar_music.option.overall_shuffle === "on" ? "1" : "",
    });
  }
}

var sonaarStatsTimeOut;
jQuery(document).on("sonaarStats", function (event, audioPlayer) {
  if ($(audioPlayer).attr("id") == "sonaar-player") {
    var currentTrack = $(audioPlayer);
  } else {
    var currentTrackEQ = audioPlayer.data("currentTrack");
    var currentTrack = audioPlayer.find(".playlist ul li").eq(currentTrackEQ);
  }
  var target = currentTrack.attr("data-trackid") !== "" ? currentTrack.attr("data-trackid") : currentTrack.attr("data-audiopath");
  var data = {
    action: "post_stats",
    post_stats: {
      action: "play",
      target_title: currentTrack.attr("data-tracktitle"),
      target_url: target,
      page_title: sonaar_music.current_page.title,
      page_url: sonaar_music.current_page.url,
    },
  };
  clearTimeout(sonaarStatsTimeOut);
  sonaarStatsTimeOut = setTimeout(function () {
    if (!IRON.sonaar.player.classes.dontCountContinuous) {
      jQuery.post(sonaar_music.ajax_url, data, function (response) { });
    }
    IRON.sonaar.player.classes.dontCountContinuous = false;
  }, 3000);
});
jQuery(document).on("click", ".iron-audioplayer .playlist li .store-list a .fa-download, #sonaar-player .track-store a[download]", function (event) {
  jQuery(document).trigger("sonaarTrackDownload", jQuery(this));
});

jQuery(document).on("sonaarTrackDownload", function (event, target) {
  if ($(target).parents("#sonaar-player").length) {
    var currentTrack = jQuery(target).parents("#sonaar-player");
  } else {
    var currentTrack = jQuery(target).parents("li");
  }
  var target_id = currentTrack.data("trackid") !== "" ? currentTrack.data("trackid") : currentTrack.data("audiopath");

  var data = {
    action: "post_stats",
    post_stats: {
      action: "download",
      target_title: currentTrack.data("tracktitle"),
      target_url: target_id,
      page_title: sonaar_music.current_page.title,
      page_url: sonaar_music.current_page.url,
    },
  };

  jQuery.post(sonaar_music.ajax_url, data, function (response) { });
});

jQuery(document).ready(function () {
  setStickyPlayer();
  setIronAudioplayers();
  if (Boolean(sonaar_music.option.enable_continuous_player === "true") && !$("body").hasClass("wp-admin")) {
    if (!IRON.sonaar.player.preventContinuousUrl()) {
      sr_getCookieSettings();
    }
  }
  stickyPlayerFromPageOption();
});

function sr_setCookieSettings() {
  if (IRON.sonaar.player.classes.feedUrl && !IRON.sonaar.player.elWidgetId) {
    // Escape and delete cookies if the player is not playing a playlist post
    document.cookie = "sonaar_mp3_player_settings" + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    document.cookie = "sonaar_mp3_player_time" + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
    return;
  }

  var cvalue = {
    playlistID: IRON.sonaar.player.elWidgetId ? IRON.sonaar.player.postId : IRON.sonaar.player.playlistID.toString().split(", "),
    trackID: IRON.sonaar.player.currentTrack,
    elWidgetId: IRON.sonaar.player.elWidgetId,
    shuffle: IRON.sonaar.player.shuffle,
    mute: IRON.sonaar.player.mute,
    isPlaying: IRON.sonaar.player.isPlaying,
    minimize: IRON.sonaar.player.minimize,
  };

  if (cvalue.playlistID == "") {
    //Verify cookie value
    return;
  }

  document.cookie = "sonaar_mp3_player_settings" + "=" + JSON.stringify(cvalue) + ";default;path=/";
  document.cookie = "sonaar_mp3_player_volume" + "=" + IRON.sonaar.player.volume + ";default;path=/";
}

function sr_setCookieTime() {
  if (IRON.sonaar.player.classes.continuousPlayer && ((IRON.sonaar.player.elWidgetId && IRON.sonaar.player.classes.feedUrl) || !IRON.sonaar.player.classes.feedUrl)) {
    // If Continuous player is enable AND if playlist is not built through a shortcode.
    var cvalue = sr_getTrackCurrentTime();
    document.cookie = "sonaar_mp3_player_time" + "=" + cvalue + ";default;path=/";
  }
}

function sr_getCookieSettings() {
  var cookieSettingsValue = getCookieValue("sonaar_mp3_player_settings");
  var cookieVolumeValue = getCookieValue("sonaar_mp3_player_volume");
  var cookieTimeValue = getCookieValue("sonaar_mp3_player_time");

  if (cookieSettingsValue != "") {
    cookieSettingsValue = JSON.parse(cookieSettingsValue);
    sr_setPlayerfromCookieSettings(cookieSettingsValue, cookieVolumeValue);
  } else {
    sr_setVolumeSliderManually(cookieVolumeValue);
  }
  if (cookieTimeValue != "") {
    sr_setTrackCurrentTime(cookieTimeValue);
  }
}

function getCookieValue(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(";");
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function sr_setPlayerfromCookieSettings(playerSetting, cookieVolumeValue) {
  IRON.sonaar.player.setPlayer({
    id: playerSetting.playlistID,
    elwidgetid: playerSetting.elWidgetId,
    trackid: playerSetting.trackID,
    shuffle: playerSetting.shuffle,
  });
  var firstLoad = true;
  if (IRON.sonaar.player.classes.wavesurferEnable) {
    //if the wave is dynamic or synthetic;
    IRON.sonaar.player.cookieSetting.mute = playerSetting.mute;
    IRON.sonaar.player.cookieSetting.volume = cookieVolumeValue;
    IRON.sonaar.player.cookieSetting.isPlaying = playerSetting.isPlaying;
  } else {
    sr_setVolumeSliderManually(cookieVolumeValue);
    IRON.sonaar.player.setMute(playerSetting.mute);
    if (playerSetting.isPlaying) {
      $("#sonaar-audio").on("loadeddata", function () {
        //when the audio element is loaded
        if (firstLoad) {
          //Dont want to SET current time on the second audio loading.
          firstLoad = false;
          IRON.sonaar.player.playAudio();
        }
      });
    }
  }
  IRON.sonaar.player.classes.dontCountContinuous = true;
  IRON.sonaar.player.classes.continued = true;
  IRON.sonaar.player.minimize = playerSetting.minimize;
  IRON.sonaar.player.postId = playerSetting.playlistID;
  IRON.sonaar.player.elWidgetId = playerSetting.elWidgetId;
  IRON.sonaar.player.classes.feedUrl = IRON.sonaar.player.elWidgetId ? true : false;
}

function sr_getTrackCurrentTime() {
  if (IRON.sonaar.player.classes.wavesurferEnable) {
    //if the wave is dynamic or synthetic;
    return IRON.sonaar.player.wavesurfer.getCurrentTime();
  } else {
    return document.getElementById("sonaar-audio").currentTime;
  }
}

function sr_getTrackDuration() {
  if (IRON.sonaar.player.classes.wavesurferEnable) {
    //if the wave is dynamic or synthetic;
    return IRON.sonaar.player.wavesurfer.getDuration();
  } else {
    return document.getElementById("sonaar-audio").duration;
  }
}

function sr_setTrackCurrentTime(value) {
  IRON.sonaar.player.cookieSetting.currentTime = value;

  var firstLoad = true;
  if (!IRON.sonaar.player.classes.wavesurferEnable) {
    //if the wave is synthetic;
    $("#sonaar-audio").on("loadeddata", function () {
      //when the audio element is loaded
      if (firstLoad) {
        //Dont want to SET current time on the second audio loading.
        firstLoad = false;

        if (value > sr_getTrackDuration() || sr_getTrackDuration() == "Infinity") return;

        document.getElementById("sonaar-audio").currentTime = value;
      }
    });
  }
}

function sr_setVolumeSliderManually(playerVolume) {
  //Set Volume and position the volume slider without the slider library
  playerVolume = playerVolume ? playerVolume : 1;
  IRON.sonaar.player.setVolume(playerVolume);
  var cssValue = playerVolume * 100 + "%";
  $("#sonaar-player .ui-slider-handle").css("bottom", cssValue);
  $("#sonaar-player .ui-slider-range").css("height", cssValue);
}



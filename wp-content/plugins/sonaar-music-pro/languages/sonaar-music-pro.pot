#, fuzzy
msgid ""
msgstr ""
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"Project-Id-Version: MP3 Music Player by Sonaar - Pro version\n"
"POT-Creation-Date: 2019-09-04 14:11-0400\n"
"PO-Revision-Date: 2019-09-04 14:10-0400\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-Flags-xgettext: --add-comments=translators:\n"
"X-Poedit-WPHeader: sonaar-music-pro.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#. Author of the plugin/theme
#: admin/class-sonaar-music-pro-admin.php:248
msgid "Sonaar Music"
msgstr ""

#: admin/class-sonaar-music-pro-admin.php:252
msgid "Statistics"
msgstr ""

#: admin/class-sonaar-music-pro-admin.php:322
msgid "Published"
msgstr ""

#: admin/class-sonaar-music-pro-admin.php:372
msgid ""
"An Unexpected HTTP Error occurred during the API request.</p> <p><a href=\"?"
"\" onclick=\"document.location.reload(); return false;\">Try again</a>"
msgstr ""

#: admin/class-sonaar-music-pro-admin.php:377
msgid "An unknown error occurred"
msgstr ""

#: sonaar-music-pro.php:82
msgid "Artwork"
msgstr ""

#: sonaar-music-pro.php:89
msgid "Artwork Width"
msgstr ""

#: sonaar-music-pro.php:102
msgid "Artwork Padding"
msgstr ""

#: sonaar-music-pro.php:110
msgid "Artwork Alignment"
msgstr ""

#: sonaar-music-pro.php:114
msgid "Left"
msgstr ""

#: sonaar-music-pro.php:118
msgid "Center"
msgstr ""

#: sonaar-music-pro.php:122
msgid "Right"
msgstr ""

#: sonaar-music-pro.php:134
msgid "Playlist"
msgstr ""

#: sonaar-music-pro.php:141
msgid "Title Options"
msgstr ""

#: sonaar-music-pro.php:146
msgid "Hide Title"
msgstr ""

#: sonaar-music-pro.php:155
msgid "Title Color"
msgstr ""

#: sonaar-music-pro.php:167
msgid "Title Typography"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "MP3 Music Player by Sonaar - Pro version"
msgstr ""

#. Plugin URI of the plugin/theme
#. Author URI of the plugin/theme
msgid "https://goo.gl/tTcJ1Y"
msgstr ""

#. Description of the plugin/theme
msgid ""
"Premium extension for MP3 Music Player by Sonaar to display statistic "
"reports and insights for your tracks and playlists."
msgstr ""

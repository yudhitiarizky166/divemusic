<?php

/**
 *
 * @link              sonaar.io
 * @since             1.0.0
 * @package           Sonaar_Music_Pro
 *
 * @wordpress-plugin
 * Plugin Name:       MP3 Music Player by Sonaar - Pro version
 * Plugin URI:        https://goo.gl/tTcJ1Y
 * Slug:			  sonaar-music-pro
 * Description:       Premium extension for MP3 Music Player by Sonaar to display statistic reports and insights for your tracks and playlists.
 * Version:           2.3.1
 * Author:            Sonaar Music
 * Author URI:        https://goo.gl/tTcJ1Y
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       sonaar-music-pro
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
update_site_option( 'sonaar_music_licence', '************' );
/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'SRMP3PRO_VERSION', '2.3.1'); // important to avoid cache issues on update
define( 'SRMP3_MIN_VERSION', '2.3'); // if SRMP3 public is lower than this, show noticifcation to update plugin
define( 'PLUGIN_INSTALLATION_NAME', plugin_basename(__FILE__) );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-sonaar-music-pro-activator.php
 */
function activate_sonaar_music_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sonaar-music-pro-activator.php';
	Sonaar_Music_Pro_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-sonaar-music-pro-deactivator.php
 */
function deactivate_sonaar_music_pro() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-sonaar-music-pro-deactivator.php';
	Sonaar_Music_Pro_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_sonaar_music_pro' );
register_deactivation_hook( __FILE__, 'deactivate_sonaar_music_pro' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-sonaar-music-pro.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_sonaar_music_pro() {

	$plugin = new Sonaar_Music_Pro();
	$plugin->run();

}

add_filter( 'sr_all_controls', function( $all_controls ) {
	
	$all_controls['artwork_style'] = array(
		'label'		=> __( 'Artwork', 'sonaar-music-pro' ),
		'tab' 		=> \Elementor\Controls_Manager::TAB_STYLE,
		/*'condition' => [
					'playlist_show_artwork!' => '',
				],*/
		'controls' 	=> array(
			'artwork_width' 	=> array(
				'label' 		=> __( 'Artwork Width', 'sonaar-music-pro' ) . ' (px)',
				'type' 			=> \Elementor\Controls_Manager::SLIDER,
				'range'			=> [
					'px' 		=> [
						'min' 	=> 1,
						'max' 	=> 1000,
					],
				],
				'selectors' 	=> [
					'{{WRAPPER}} .iron-audioplayer .album .album-art' => 'max-width: {{SIZE}}px;',
				],
			),
			'artwork_padding' => array(
				'label' 		=> __( 'Artwork Padding', 'sonaar-music-pro' ),
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .iron-audioplayer .sonaar-grid .album' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			),
			'artwork_align' => array(
				'label' 		=> __( 'Artwork Alignment', 'sonaar-music-pro' ),
				'type' 			=> \Elementor\Controls_Manager::CHOOSE,
				'options' 		=> [
					'left'    	=> [
						'title' => __( 'Left', 'elementor' ),
						'icon' 	=> 'fa fa-align-left',
					],
					'center' 	=> [
						'title' => __( 'Center', 'elementor' ),
						'icon' 	=> 'fa fa-align-center',
					],
					'right' 	=> [
						'title' => __( 'Right', 'elementor' ),
						'icon' 	=> 'fa fa-align-right',
					],
				],
				'default' 		=> '',
				'selectors' 	=> [
					'{{WRAPPER}} .iron-audioplayer .sonaar-Artwort-box' => 'justify-self: {{VALUE}}; text-align: {{VALUE}};',
				],
			),
		),
	);
	$all_controls['playlist_style'] = array(
		'label'		=> __( 'Playlist', 'sonaar-music-pro' ),
		'tab' 		=> \Elementor\Controls_Manager::TAB_STYLE,
		'condition' => [
					'playlist_show_playlist!' => '',
		],
		'controls' 	=> array(
			'title_options' 	=> array(
				'label' 		=> __( 'Title Options', 'elementor' ),
				'type' 			=> \Elementor\Controls_Manager::HEADING,
				'separator'		=> 'before',
			),
			'title_btshow' => array(
				'label' 		=> esc_html__( 'Hide Title', 'sonaar-music-pro' ),
				'type' 			=> \Elementor\Controls_Manager::SWITCHER,
				'default' 		=> '',
				'return_value' 	=> 'none',
				'selectors' 	=> [
					'{{WRAPPER}} .playlist .sr_it-playlist-title' => 'display:{{VALUE}};',
				 ],
			),
			'title_color' => array(
				'label'			=> __( 'Title Color', 'sonaar-music-pro' ),
				'type'          => \Elementor\Controls_Manager::COLOR,
				'default'       => '',
				'condition' 	=> [
					'title_btshow' => '',
				],
				'selectors'    	=> [
					'{{WRAPPER}} .playlist .sr_it-playlist-title' => 'color: {{VALUE}}',
				],
			),
			//i am freeze here.
			'title_typography' => array(
				'label' 		=> __( 'Title Typography', 'sonaar-music-pro' ),
				'scheme' 		=> \Elementor\Group_Control_Typography::get_type(),
				'condition' 	=> [
					'title_btshow' => '',
				],
				'selector' => [
					'{{WRAPPER}} .iron-audioplayer .sr_it-playlist-title',
				],
			),
		),
	);

	return $all_controls;
} );

run_sonaar_music_pro();



/*
* META BOX 
*/


add_action( 'load-post.php', 'sonaar_meta_boxes_setup');
add_action( 'load-post-new.php', 'sonaar_meta_boxes_setup' );

/* Meta box setup function. */
function sonaar_meta_boxes_setup() {
	add_action( 'add_meta_boxes', 'sonaar_add_post_meta_boxes' );
	add_action( 'save_post', 'sonaar_save_meta', 10, 2 );
}

/* Create one or more meta boxes to be displayed on the post editor screen. */
function sonaar_add_post_meta_boxes() {

	add_meta_box(
		'meta-footer-player',      // Unique ID
		esc_html__( 'Sticky Audio Player', 'sonaar-music-pro' ),    // Title
		'footer_player_meta_box',   // Callback function
		null,         // Admin page (or post type)
		'side',         // Context
		'default'         // Priority
	);
}

/* Display the post meta box. */
function footer_player_meta_box( $post ) { 

	$current_values = get_post_meta( $post->ID, 'sonaar_footer_player_meta', true);
	if( !is_array($current_values)){
		$current_values = [];
	}

	$args = array(
		'post_type' => ( Sonaar_Music::get_option('srmp3_posttypes') != null ) ? Sonaar_Music::get_option('srmp3_posttypes') : 'album',//array('album', 'post', 'product'),
		'post_status' => 'publish',
		//'orderby' => 'title',
		'order' => 'ASC',
		'posts_per_page'=>-1
	);
	$options = get_posts( $args );	
	?>

	<?php wp_nonce_field( basename( __FILE__ ), 'footer_player_class_nonce' ); ?>
  
	<p>
		<label for="footer-player"><?php _e( 'Display sticky player when page is loaded. Select playlist:', 'sonaar-music-pro' ); ?></label>
		<select multiple name="sonaar_footer_player_meta[]" id="sonaar_footer_player_meta">
		<option value=""><?php _e( '- Select -', 'sonaar-music-pro' ); ?></option>
		<?php
		$sr_postypes = ( Sonaar_Music::get_option('srmp3_posttypes') != null ) ? Sonaar_Music::get_option('srmp3_posttypes') : 'album';
		if( in_array( $post->post_type, $sr_postypes ) ){
			echo '<option value="' . $post->ID . '" ';
			echo ( in_array( $post->ID, $current_values) )?'selected="selected" >': '>';
			echo __( 'Current Post Tracklist', 'sonaar-music-pro' );
			echo '</option>';
		}

		foreach ($options as $value) {
			if (Sonaar_Music::srmp3_check_if_audio($value)){
				echo '<option value="' . $value->ID . '" ';
				echo ( in_array( $value->ID, $current_values) )?'selected="selected" >': '>';
				echo '['.$value->post_type .'] ';
				echo ''. $value->post_title .'</option>';
			}
		}
		?>
		</select>
		<label for="footer-player-shuffle">
			<input type="checkbox" name="sonaar_footer_player_shuffle_meta" id="sonaar_footer_player_shuffle_meta" value="true" <?php echo (get_post_meta( $post->ID, 'sonaar_footer_player_shuffle_meta', true))?'checked="checked"':''; ?> >
			<?php _e( 'Enable Shuffle', 'sonaar-music-pro' );?>
		</label>
	</p>

	<?php }

/* Save the meta box's post metadata. */
function sonaar_save_meta( $post_id ) {
    if(isset($_POST["sonaar_footer_player_meta"])){
        $meta_element_class = $_POST['sonaar_footer_player_meta'];
        update_post_meta($post_id, 'sonaar_footer_player_meta', $meta_element_class);
	}
	if(isset($_POST["sonaar_footer_player_shuffle_meta"])){
        update_post_meta($post_id, 'sonaar_footer_player_shuffle_meta', true);
    }else{
		update_post_meta($post_id, 'sonaar_footer_player_shuffle_meta', false);
	}
}

// -------------------  END META BOXES ----------------//

<?php

/**
* The admin-specific functionality of the plugin.
*
* @link       sonaar.io
* @since      1.0.0
*
* @package    Sonaar_Music_Pro
* @subpackage Sonaar_Music_Pro/admin
*/

/**
* The admin-specific functionality of the plugin.
*
* Defines the plugin name, version, and two examples hooks for how to
* enqueue the admin-specific stylesheet and JavaScript.
*
* @package    Sonaar_Music_Pro
* @subpackage Sonaar_Music_Pro/admin
* @author     Edouard Duplessis <eduplessis@gmail.com>
*/
class Sonaar_Music_Pro_Admin {
    
    /**
    * The ID of this plugin.
    *
    * @since    1.0.0
    * @access   private
    * @var      string    $plugin_name    The ID of this plugin.
    */
    private $plugin_name;
    
    /**
    * The version of this plugin.
    *
    * @since    1.0.0
    * @access   private
    * @var      string    $version    The current version of this plugin.
    */
    private $version;
    
    /**
    * Initialize the class and set its properties.
    *
    * @since    1.0.0
    * @param      string    $plugin_name       The name of this plugin.
    * @param      string    $version    The version of this plugin.
    */
    public function __construct( $plugin_name, $version ) {
        
        $this->plugin_name = $plugin_name;
        $this->version = $version;
		
		global $iron_music_player;
		$iron_music_player = get_option( 'iron_music_player' );		

    }
    
    /**
    * Register the stylesheets for the admin area.
    *
    * @since    1.0.0
    */
    public function enqueue_styles($hook) {
        
        /**
        * This function is provided for demonstration purposes only.
        *
        * An instance of this class should be passed to the run() function
        * defined in Sonaar_Music_Pro_Loader as all of the hooks are defined
        * in that particular class.
        *
        * The Sonaar_Music_Pro_Loader will then create the relationship
        * between the defined hooks and the functions defined in this
        * class.
        */
        if ( $hook == 'album_page_sonaar_music_pro' || $hook == 'album_page_sonaar_music_pro_license' || $hook == 'post.php') {
            wp_enqueue_style( 'daterangepicker', '//cdn.jsdelivr.net/npm/daterangepicker@3/daterangepicker.min.css', array(), $this->version, 'all' );
            wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/sonaar-music-pro-admin.css', array('daterangepicker'), $this->version, 'all' );
        }
    }
    
    /**
    * Register the JavaScript for the admin area.
    *
    * @since    1.0.0
    */
    public function enqueue_scripts($hook) {
        
        /**
        * This function is provided for demonstration purposes only.
        *
        * An instance of this class should be passed to the run() function
        * defined in Sonaar_Music_Pro_Loader as all of the hooks are defined
        * in that particular class.
        *
        * The Sonaar_Music_Pro_Loader will then create the relationship
        * between the defined hooks and the functions defined in this
        * class.
        */
        
        if ($hook == 'album_page_sonaar_music_pro' || $hook == 'album_page_sonaar_music_pro_license' ) {
            wp_enqueue_script( 'vuejs', "//cdn.jsdelivr.net/npm/vue@2/dist/vue.min.js" , array(), $this->version, false );
            wp_enqueue_script( 'polyfill', "//cdn.jsdelivr.net/npm/babel-polyfill@6/dist/polyfill.min.js" , array(), $this->version, false );
            wp_enqueue_script( 'bootstrap-vue', "//cdn.jsdelivr.net/npm/bootstrap-vue@2.0.0-rc.9/dist/bootstrap-vue.min.js" , array(), $this->version, false );
            wp_enqueue_script( 'chart', "//cdn.jsdelivr.net/npm/chart.js@2/dist/Chart.min.js" , array(), $this->version, false );
            wp_enqueue_script( 'daterangepicker-moment', "https://cdn.jsdelivr.net/momentjs/latest/moment.min.js" , array('jquery'), $this->version, false );
            wp_enqueue_script( 'daterangepicker', "https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js" , array('jquery'), $this->version, false );
            // wp_enqueue_script( $this->plugin_name.'-dep', "https://cdn.jsdelivr.net/combine/npm/vue@2,npm/babel-polyfill@6,npm/bootstrap-vue@2.0.0-rc.9,npm/chart.js@2,npm/daterangepicker@3,npm/moment@2" , array(), $this->version, false );
            
            wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/sonaar-music-pro-admin.js', array( 'jquery','vuejs', 'polyfill', 'bootstrap-vue', 'chart', 'moment', 'daterangepicker' ), $this->version, true );
            
            $sonaar_data = new Sonaar_Music_Get;
            
            $date_start = ( isset( $_GET['date_start']) )? $_GET['date_start']: date('Y-m-d H:i:s', strtotime('today - 14 day'));
            $date_end = ( isset( $_GET['date_end']) )? $_GET['date_end']: date('Y-m-d H:i:s', strtotime('today'));
            $sonaar_data->set_date($date_start, $date_end);
            $date1 = new DateTime($sonaar_data->get_date()[0]);
            $date2 = new DateTime($sonaar_data->get_date()[1]);
            $interval = $date2->diff($date1);
            $interval = ( $interval->days < 2 )? 1 : $interval->days;
            
            $sonaar_data->set_interval($interval);
            
            $get_play_count_per_page = $sonaar_data->get_play_count_per_page();
            foreach ( $get_play_count_per_page as $i => $value ) {
                $get_play_count_per_page[$i]->id = url_to_postid( $get_play_count_per_page[$i]->page_url );
            }
            
            $url = ( isset( $_GET['url']) && url_to_postid( $_GET['url'] ) )? $_GET['url']: false;
			
            $play_count_by_day = $sonaar_data->get_play_count_by_day($url) ;
			
            $get_play_count_per_track = $sonaar_data->get_play_count_per_track(array('url' => $url));
            $get_download_count_per_track = $sonaar_data->get_download_count_per_track(array('url' => $url));
            

            $dataDate = array();
            $dataCount = array();
            foreach ( $play_count_by_day as $play ) {
				if ( !empty($play->date) ) {
					array_push($dataDate, $play->date);
				}
				if ( !empty($play->play_count) ) {
					array_push($dataCount, $play->play_count);
				}
            }

            if( count( $dataDate ) == 1 )
                array_push($dataDate, $dataDate[0]);
        
            if( count( $dataCount ) == 1 )
                array_push($dataCount, $dataCount[0]);
            
            foreach ($get_play_count_per_track as $count) {
                $count->track_title =  ( get_the_title( $count->target_url ) )? get_the_title( $count->target_url ): $count->target_title ;
                $count->target_url = ( get_edit_post_link( $count->target_url ) )? admin_url( 'upload.php?item=' . $count->target_url ): FALSE;
            }

            foreach ($get_download_count_per_track as $count) {
                $count->track_title =  ( get_the_title( $count->target_url ) )? get_the_title( $count->target_url ): $count->target_title ;
                $count->target_url = ( get_edit_post_link( $count->target_url ) )? admin_url( 'upload.php?item=' . $count->target_url ): FALSE;
            }

            function get_total_track(){
                $albums = new WP_Query( array(
                    'post_type' => 'album',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                ) );
                $tracks = 0;
                while ($albums->have_posts()) {
                    $albums->the_post();
                    // echo get_the_id();
                    $albumTracks = get_post_meta(get_the_id(),'alb_tracklist', true);
                    // var_dump(count($albumTracks));
                    $tracks = $tracks + count($albumTracks);
                }
                return $tracks;
            }
            
            wp_localize_script( $this->plugin_name, strtr($this->plugin_name, '-', '_'), array(
            'get_play_count_by_day'=> $play_count_by_day,
            'get_play_count_per_page'=> $get_play_count_per_page,
            'get_play_count_per_track'=> $get_play_count_per_track,
            'get_download_count_per_track'=> $get_download_count_per_track,
            'totalPlay' => $sonaar_data->get_play_count($url),
            'totalDownload' => $sonaar_data->get_download_count($url),
            'totalTrack' => get_total_track(),
            'interval'=> array(
                'start'=> $date1->format('m/d/Y'),
                'end'=> $date2->format('m/d/Y')
                ),
            'licence' => get_site_option( 'sonaar_music_licence', '' )
            ));

        }

        wp_enqueue_script( $this->plugin_name. 'global', plugin_dir_url( __FILE__ ) . 'js/sonaar-music-pro-admin-global.js', array( 'jquery' ), $this->version, true );
		
		$locale_settings = array(
			'ajaxurl' 		=> admin_url('admin-ajax.php'),
			'ajax_nonce' 	=> wp_create_nonce('sonaar_music_pro'),					
		);
		wp_localize_script($this->plugin_name. 'global', 'sonaar_music_pro', $locale_settings);
        
    }
    
    
    /**
    * Create custom posttype
    **/
    /*
    public function create_postType(){
        
        $artists_args = array(
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'has_archive'         => true,
        'query_var'           => true,
        'exclude_from_search' => false,
        );
        
        $artists_args['labels'] = array(
        'name'               => esc_html__('Artists', 'sonaar-music-pro'),
        'singular_name'      => esc_html__('Artist', 'sonaar-music-pro'),
        'name_admin_bar'     => esc_html_x('Artist', 'add new on admin bar', 'sonaar-music-pro'),
        'menu_name'          => esc_html__('Artists', 'sonaar-music-pro'),
        'all_items'          => esc_html__('All Artists', 'sonaar-music-pro'),
        'add_new'            => esc_html__('Add Artist', 'video', 'sonaar-music-pro'),
        'add_new_item'       => esc_html__('Add New Artist', 'sonaar-music-pro'),
        'edit_item'          => esc_html__('Edit Artist', 'sonaar-music-pro'),
        'new_item'           => esc_html__('New Project', 'sonaar-music-pro'),
        'view_item'          => esc_html__('View Artist', 'sonaar-music-pro'),
        'search_items'       => esc_html__('Search Artist', 'sonaar-music-pro'),
        'not_found'          => esc_html__('No artists found.', 'sonaar-music-pro'),
        'not_found_in_trash' => esc_html__('No artists found in the Trash.', 'sonaar-music-pro'),
        'parent'             => esc_html__('Parent Artist:', 'sonaar-music-pro')
        );
        
        $artists_args['supports'] = array('title', 'editor', 'revisions','thumbnail');
        $artists_args['menu_icon'] = 'dashicons-groups';
        $artists_args['rewrite'] = array('slug'=> esc_html__('artist', 'sonaar-music-pro') );
        
        register_post_type('artist', $artists_args);
        
    }
    */
    
    public function init_options() {
		
		$cmb_tools_options = new_cmb2_box( array(
        
        'id'           		=> 'sonaar_music_pro_tools_network_option_metabox',
        'title'        		=> esc_html__( 'Sonaar Music', 'sonaar-music-pro' ),
        'object_types' 		=> array( 'options-page' ),
        'option_key'      	=> 'sonaar_music_pro_tools', // The option key and admin menu page slug.
        'icon_url'        	=> 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
        'menu_title'      	=> esc_html__( 'Tools', 'sonaar-music-pro' ), // Falls back to 'title' (above).
        'parent_slug'     	=> 'edit.php?post_type=album', // Make options page a submenu item of the themes menu.
        'capability'      	=> 'manage_options', // Cap required to view options-page.
        'enqueue_js' 		=> false,
        'cmb_styles' 		=> false,
        'display_cb'		=> 'sonaar_music_pro_tools_admin_display',
        'position' 			=> 9999,
        ) );
        
        $cmb_options = new_cmb2_box( array(
        
        'id'           		=> 'sonaar_music_pro_network_option_metabox',
        'title'        		=> esc_html__( 'Sonaar Music', 'sonaar-music-pro' ),
        'object_types' 		=> array( 'options-page' ),
        'option_key'      	=> 'sonaar_music_pro', // The option key and admin menu page slug.
        'icon_url'        	=> 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
        'menu_title'      	=> esc_html__( 'Statistics', 'sonaar-music-pro' ), // Falls back to 'title' (above).
        'parent_slug'     	=> 'edit.php?post_type=album', // Make options page a submenu item of the themes menu.
        'capability'      	=> 'manage_options', // Cap required to view options-page.
        'enqueue_js' 		=> false,
        'cmb_styles' 		=> false,
        'display_cb'		=> 'sonaar_music_pro_admin_display',
        'position' 			=> 9999,
        ) );
		
		$cmb_options_license = new_cmb2_box( array(
        
        'id'           		=> 'sonaar_music_pro_license_network_option_metabox',
        'title'        		=> esc_html__( 'Sonaar Music', 'sonaar-music-pro' ),
        'object_types' 		=> array( 'options-page' ),
        'option_key'      	=> 'sonaar_music_pro_license', // The option key and admin menu page slug.
        'icon_url'        	=> 'dashicons-palmtree', // Menu icon. Only applicable if 'parent_slug' is left empty.
        'menu_title'      	=> esc_html__( 'License', 'sonaar-music-pro' ), // Falls back to 'title' (above).
        'parent_slug'     	=> 'edit.php?post_type=album', // Make options page a submenu item of the themes menu.
        'capability'      	=> 'manage_options', // Cap required to view options-page.
        'enqueue_js' 		=> false,
        'cmb_styles' 		=> false,
        'display_cb'		=> 'sonaar_music_pro_license_admin_display',
        'position' 			=> 9999,
        ) );
        
		function sonaar_music_pro_tools_admin_display() {
			require_once plugin_dir_path( __FILE__ ) . 'partials/sonaar-music-pro-tools-display.php';
		}
        
        
        function sonaar_music_pro_admin_display(){
            require_once plugin_dir_path( __FILE__ ) . 'partials/sonaar-music-pro-admin-display.php';
        }
		
		function sonaar_music_pro_license_admin_display(){
            require_once plugin_dir_path( __FILE__ ) . 'partials/sonaar-music-pro-license-admin-display.php';
        }
        
    }
    
    public static function post_stats(){
        global $wpdb;
        $sonaar_music_post_data = new Sonaar_Music_Post;
        $request = $_POST['post_stats'];
        
        wp_send_json( $sonaar_music_post_data->log( $request ) );
    }
    
    public static function get_stats(){
        $getStats = $_POST['get_stats'];
        
        
        
        $sonaar_music_get_data = new Sonaar_Music_Get;
        
        $interval = ( $getStats['interval'] !== '' )? $getStats['interval'] : 14 ;
        
        $sonaar_music_get_data->set_interval( $interval );
        
        $countPer = $getStats['count_per'];
        
        switch ($countPer) {
            case 'track':
                wp_send_json( $sonaar_music_get_data->get_play_count_per_track() );
                break;
            
            case 'page':
                wp_send_json( $sonaar_music_get_data->get_play_count_per_page() );
                break;
            
            default:
                $play_count_by_day = $sonaar_music_get_data->get_play_count_by_day() ;
                $dataDate = array();
                $dataCount = array();
                foreach ( $play_count_by_day as $play ) {
                    array_push($dataDate, $play->date);
                    array_push($dataCount, $play->play_count);
            }
            wp_send_json( array(
            'date' => $dataDate,
            'count' => $dataCount
            ) );
            break;
        }
    }


    public function manage_album_columns ($columns){
        
        $iron_cols = array('alb_stats' => '');
        
        $columns = Sonaar_Music_Pro::array_insert($columns, $iron_cols, 'date', 'before');
        
        $columns['date'] = esc_html__('Published', 'sonaar-music-pro');   // Renamed date column
        
        return $columns;
    }


    public function manage_album_custom_column ($column, $post_id){
        switch ($column){
            case 'alb_stats':
                echo '<a href="' . esc_url( get_admin_url( null, 'edit.php?post_type=album&page=sonaar_music_pro&url=') . get_permalink( $post_id ) ) . '"><span class="dashicons dashicons-chart-area"></span></a>';
                
                break;
        }
    }

    
	public function check_for_plugin_update($checked_data) {		
	
        if (empty($checked_data->checked))
            return $checked_data;
        
		$sonaar_music_licence = get_site_option('sonaar_music_licence');
		if ( $sonaar_music_licence == '' ) {
			return $checked_data;
		}
        $request_args = array(
            'slug' => $this->plugin_name,
            'version' => $checked_data->checked[ PLUGIN_INSTALLATION_NAME ]
        );
        
        $request_string = $this->prepare_request('basic_check', $request_args);
        
		//$api_url = 'https://sonaar.io/api/';
		$api_url = 'https://sonaar.io/wp-json/wp/v2/sonaar-api/version-check/';
        $raw_response = wp_remote_post($api_url, $request_string);
		
        if (!is_wp_error($raw_response) && ($raw_response['response']['code'] == 200))
			$response = json_decode( $raw_response['body']);
        
        if (!empty($response)) // Feed the update data into WP updater
            $checked_data->response[ PLUGIN_INSTALLATION_NAME ] = $response;
	
        return $checked_data;
    }
    
    
    
    public function plugin_api_call($def, $action, $args) {
		
		$args = (array) $args;
        if (  !isset($args['slug']) || ( isset($args['slug']) &&  $args['slug'] != $this->plugin_name ) )
            return false;
        
        $request = wp_remote_post('http://assets.sonaar.io/plugins/MP3-Music-Player-By-Sonaar-Pro/changelog.txt');
        
        if (is_wp_error($request)) {
            $res = new WP_Error('plugins_api_failed', __('An Unexpected HTTP Error occurred during the API request.</p> <p><a href="?" onclick="document.location.reload(); return false;">Try again</a>'), $request->get_error_message());
        } else {
            $res = $request['body'];
            
            if ($res === false)
                $res = new WP_Error('plugins_api_failed', __('An unknown error occurred'), $request['body']);
        }
        echo nl2br($res);
        die();
    }
    
    
    public function prepare_request($action, $args) {
        global $wp_version;
        
        // var_dump($args);

        return array(
            'body' => array(
                'update_action' => $action, 
                'request' 		=> $args,
                'licence' 		=> get_site_option('sonaar_music_licence'),
                'siteurl' 		=> $_SERVER['SERVER_NAME']
            ),
            'user-agent' => 'WordPress/' . $wp_version . '; ' . get_bloginfo('url')
        );	
    }
	
	public function srmp3_create_mp3_playlists_ajax() {
		
		if ( ! current_user_can( 'manage_options' ) ) {
				wp_die(0); 
		}
		check_ajax_referer( 'sonaar_music_pro', 'wpnonce' );

		$post_type    = isset( $_POST['post_type'] ) ? $_POST['post_type'] : 'album';  
		$type         = 'single';
		$product_type = isset( $_POST['product_type'] ) ? $_POST['product_type'] : 'simple';
		$title 		  = isset( $_POST['title'] ) ? $_POST['title'] : 'simple';
		$price 		  = isset( $_POST['price'] ) ? (float) $_POST['price'] : '9.99';
		$alb_tracklist        = array();
		switch ( $type ) {
			case 'single':
				$id    = isset( $_POST['id'] ) ? (int) $_POST['id'] : 0;				
				$track = wp_get_attachment_metadata( $id );
				$post  = get_post( $id );
				if ( $post->post_title ) {
					$track['title'] = $post->post_title;
				}
                $thumbnail_id        = get_post_thumbnail_id( $id );
                $thumbnail_url       = wp_get_attachment_image_src( get_post_thumbnail_id( $id ))[0];
				$file_url            = wp_get_attachment_url( $id );
				$file_hash           = md5( $file_url );
				$alb_tracklist[] = array(
					'FileOrStream' 	=> 'mp3',
					'track_mp3_id' 	=> $id,
					'track_mp3' 	=> $file_url,
                    'track_image'   => $thumbnail_url,
				);				
				break;
			
			default:
				break;
		}

		$post_id = wp_insert_post(
			array(
				'post_title'  => $track['title'],
				'post_status' => 'draft',
				'post_type'   => $post_type,
			)
		);
		
		update_post_meta( $post_id, 'alb_tracklist', $alb_tracklist );
		update_post_meta( $post_id, '_thumbnail_id', $thumbnail_id );
		
		if ( $post_type == 'product') {
			wp_set_object_terms( $post_id, $product_type, 'product_type' );
			update_post_meta( $post_id, '_visibility', 'visible' );
			update_post_meta( $post_id, '_stock_status', 'instock' );
			update_post_meta( $post_id, 'total_sales', '0' );
			update_post_meta( $post_id, '_virtual', 'yes' );
			update_post_meta( $post_id, '_downloadable', 'yes' );
			update_post_meta( $post_id, '_regular_price', $price );
			update_post_meta( $post_id, '_featured', 'no' );
			update_post_meta( $post_id, '_price', $price );
			update_post_meta( $post_id, '_sold_individually', 'yes' );
            update_post_meta( $post_id, 'wc_add_to_cart', 'true' );
		}

		wp_send_json_success();
		
	}
	public function sonaar_music_pro_get_post_id( $data ) {
	
		global $iron_music_player;	
		$srmp3_posttypes = ( isset($iron_music_player['srmp3_posttypes']) && !empty($iron_music_player['srmp3_posttypes']) ) ? $iron_music_player['srmp3_posttypes'] : array('album');
		
		$post_id = false;

		$args = array(
			'posts_per_page' => 1,
			'post_status'    => 'any',
			'post_type'      => $srmp3_posttypes,
		);

		$meta_query = array();
		$music_type = '';
		if ( isset( $data['file'] ) || isset( $data['id'] ) ) {
			$value	= isset( $data['file'] ) ? $data['file'] : $data['id'];
			
			/*
			$value = '.*s:[0-9]+:"track_mp3";s:[0-9]+:"'.$value.'";*';		
			$meta_query['relation'] = 'AND';
			$meta_query[]           = array(
				'key'     => 'alb_tracklist',
				'value'   => $value,
				'compare' => 'REGEXP',
			);
			*/
			$meta_query[]	= array(
				'key'     => 'alb_tracklist',
				'value'   => $value,
				'compare' => 'LIKE',
			);			
		}
		
		$args['meta_query'] = $meta_query; // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_query
		
		$query = new WP_Query( $args );		
		if ( $query->have_posts() ) {
			$query->the_post();
			$post_id = get_the_ID();
			wp_reset_postdata();
		}
		

		return $post_id;
	}
	public function sonaar_music_pro_get_audio_attachments( $posts_per_page = 20, $paged = 1, $search = '' ) {

		$tracks = array();
		$albums = array();

		$args = array(
			'posts_per_page' => $posts_per_page,
			'paged'          => $paged,
			'post_type'      => 'attachment',
			'post_mime_type' => 'audio',
			'post_status'    => 'any',
		);

		if ( $search ) {
			$args['s'] = $search;
		}
		$query        = new WP_Query( $args );
		$found_tracks = $query->found_posts;

		if ( $query->have_posts() ) {
			foreach ( $query->posts as $attachment ) {
				$id          = $attachment->ID;
				$track       = wp_get_attachment_metadata( $id );
				$track['id'] = $id;
				$post        = get_post( $id );
				if ( $post->post_title ) {
					$track['title'] = $post->post_title;
				}
				$track_file    = wp_get_attachment_url( $id );
				$track['file'] = $track_file;

				$track['product'] = $this->sonaar_music_pro_get_post_id(
					array(
						'file' => $track_file,
						'id'   => $id,
					)
				);				
				
				$tracks[] = $track;
				if ( isset( $track['album'] ) ) {
					$key = $track['album'];
					if ( ! isset( $albums[ $key ] ) ) {
						$albums[ $key ]           = array();
						$albums[ $key ]['count']  = 0;
						$albums[ $key ]['tracks'] = array();
					}
					$albums[ $key ]['count']++;
					$albums[ $key ]['tracks'][] = $track['id'];
				}
			}
			foreach ( $albums as $title => $album ) {
				$album['product'] = $this->sonaar_music_pro_get_post_id( array( 'album' => $title ) );
				$album['tracks']  = implode( ',', $album['tracks'] );
				$albums[ $title ] = $album;
			}
		}

		return array(
			'tracks'       => $tracks,
			'found_tracks' => $found_tracks,
		);
	}
	public function sonaar_music_pro_inputs( $posts_per_page = 20, $paged = 1, $search = ''  ){
		$result = $this->sonaar_music_pro_get_audio_attachments( $posts_per_page, $paged, $search );
		if ( $result ) {
			$tracks       = $result['tracks'];
			$found_tracks = $result['found_tracks'];
			$per_page     = $posts_per_page;
		} else {
			return false;
		}
		$track_inputs = '';
		
		foreach ( $tracks as $track ) {
			$id       = esc_attr( $track['id'] );
			$title    = esc_attr( $track['title'] );
			$length   = esc_html( isset( $track['length_formatted'] ) ? $track['length_formatted'] : '' );
            $album    = esc_html( ( isset( $track['album'] ) && $track['album'] != null) ? $track['album']  . ' - ' : '' );
			$file     = esc_html( basename( $track['file'] ) );
			$product  = $track['product'];
			$disabled = $product > 0 ? 'disabled' : '';

			$track_inputs .= "<div><input type='checkbox' name='music_track_$id' value='$id'  data-title='$title' /><span class='$disabled'><strong>$title</strong> – $album $length ($file) [ID: $id]</span></div>";
		}
		$args = array(
			'base'      => '%_%',
			'format'    => '#%#%',
			'current'   => (int) $paged,
			'total'     => ceil( $found_tracks / $posts_per_page ),
			'prev_text' => '<',
			'next_text' => '>',
		);
		// translators: %s is the number of tracks found.
		$found_tracks_label = '<span class="found-tracks">' . sprintf( esc_attr( _n( '%s track', '%s tracks', $found_tracks, 'sonaar-music-pro' ) ), number_format_i18n( $found_tracks ) ) . '</span>'; //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

		require_once ABSPATH . 'wp-admin/includes/template.php';
		require_once ABSPATH . 'wp-admin/includes/class-wp-screen.php';
		$pagination = new \WP_List_Table( array( 'ajax' => true ) );
		$pagination->set_pagination_args(
			array(
				'total_items' => $found_tracks,
				'total_pages' => ceil( $found_tracks / $posts_per_page ),
				'per_page'    => $posts_per_page,
			)
		);

		ob_start();
		?>

		<div class="tablenav-pages">
			<?php $pagination->pagination( 'top' ); ?>
		</div>

		<?php
		$paginate_links = ob_get_clean(); //phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped

		return array(
			'track_inputs'   => $track_inputs,
			'paginate_links' => $paginate_links,
			'found_tracks'   => $found_tracks,
		);
	}
	public function srmp3_update_mp3_playlists_ajax() {
		
		if ( ! current_user_can( 'manage_options' ) ) {
				wp_die(0); 
		}
		check_ajax_referer( 'sonaar_music_pro', 'wpnonce' );

		$per_page = isset( $_POST['per_page'] ) ? $_POST['per_page'] : 20;  
		$paged    = isset( $_POST['paged'] ) ? $_POST['paged'] : 1;  
		$search   = isset( $_POST['search'] ) ? $_POST['search'] : '';
		
		$mp3_lists_inputs = $this->sonaar_music_pro_inputs($per_page, $paged, $search);;
		if ( $mp3_lists_inputs ) {
			$music_playlists	= $mp3_lists_inputs['track_inputs'];
			$paginate_info		= $mp3_lists_inputs['paginate_links'];
			wp_send_json_success(
				array(
					'music_playlists'   => $music_playlists,
					'paginate_info'		=> $paginate_info,
				)
			);
		}
		wp_send_json_error( array( 'message' => esc_html__( 'An error occurred while retrieving the audio attachments lists.', 'sonaar-music-pro' ) ) );
	}
	
	public function srmp3_create_single_mp3_playlists_ajax() {
		if ( ! current_user_can( 'manage_options' ) ) {
				wp_die(0); 
		}
		check_ajax_referer( 'sonaar_music_pro', 'wpnonce' );

		$post_type    = isset( $_POST['post_type'] ) ? $_POST['post_type'] : 'album';  
		$type         = 'single';
		$product_type = isset( $_POST['product_type'] ) ? $_POST['product_type'] : 'simple';
		$mp3_id = isset( $_POST['mp3_id'] ) ? json_decode(stripslashes($_POST['mp3_id']), true ): '';		
		
		$price = isset( $_POST['price'] ) ? (float) $_POST['price'] : '9.99';
		$alb_tracklist        = array();
		switch ( $type ) {
			case 'single':
				$mp3_id = isset( $_POST['mp3_id'] ) ? json_decode(stripslashes($_POST['mp3_id']), true ): '';
				$track = wp_get_attachment_metadata( $mp3_id[0] );
				$post  = get_post( $mp3_id[0] );
				if ( $post->post_title   ) {
					$track['title'] = $post->post_title;
				}
				
				$attachment_metadata = get_post_meta($mp3_id[0] ,'_wp_attachment_metadata', true );
				if ( isset($attachment_metadata['album']) && $attachment_metadata['album'] != '' ) {
					$track['title'] = $attachment_metadata['album'];
				}
				$thumbnail_id       = get_post_thumbnail_id( $mp3_id[0] );
				
				
				foreach( $mp3_id as $id) {
                    $thumbnail_url       = wp_get_attachment_image_src( get_post_thumbnail_id( $id ))[0];
					$file_url            = wp_get_attachment_url( $id );
					$file_hash           = md5( $file_url );
					$alb_tracklist[] = array(
						'FileOrStream' 	=> 'mp3',
						'track_mp3_id' 	=> $id,
						'track_mp3' 	=> $file_url,
                        'track_image'   => $thumbnail_url,
					);
				}
				
				break;
			
			default:
				break;
		}		
		
		$post_id = wp_insert_post(
			array(
				'post_title'  => $track['title'],
				'post_status' => 'draft',
				'post_type'   => $post_type,
			)
		);
		
		update_post_meta( $post_id, 'alb_tracklist', $alb_tracklist );
		update_post_meta( $post_id, '_thumbnail_id', $thumbnail_id );
		
		if ( $post_type == 'product') {
			wp_set_object_terms( $post_id, $product_type, 'product_type' );
			update_post_meta( $post_id, '_visibility', 'visible' );
			update_post_meta( $post_id, '_stock_status', 'instock' );
			update_post_meta( $post_id, 'total_sales', '0' );
			update_post_meta( $post_id, '_virtual', 'yes' );
			update_post_meta( $post_id, '_downloadable', 'yes' );
			update_post_meta( $post_id, '_regular_price', $price );
			update_post_meta( $post_id, '_featured', 'no' );
			update_post_meta( $post_id, '_price', $price );
			update_post_meta( $post_id, '_sold_individually', 'yes' );
		}

		wp_send_json_success();
	}

}
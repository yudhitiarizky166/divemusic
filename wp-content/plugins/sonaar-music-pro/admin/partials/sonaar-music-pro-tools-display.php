<?php
global $iron_music_player;
$iron_music_player = get_option( 'iron_music_player' );	
$srmp3_posttypes = ( isset($iron_music_player['srmp3_posttypes']) && !empty($iron_music_player['srmp3_posttypes']) ) ? $iron_music_player['srmp3_posttypes'] : array('album');
?>

<div class="wrap cmb2-options-page option-iron_music_player">
	<h2 class="screen-reader-text"></h2>
	<div class="cmb2-wrap form-table">
		<div id="cmb2-metabox-sonaar_music_network_option_metabox" class="cmb2-metabox cmb-field-list">
			<div class="cmb-row cmb-type-title cmb2-id-music-player-title" data-fieldtype="title">

				<div class="cmb-td">
					<h3 class="cmb2-metabox-title" id="music-player-title" data-hash="6qnoam2d0qs0"><?php esc_html_e( 'Batch Creation Tool', 'sonaar-music-pro');?></h3>
				</div>
				
			</div>
			<div class="cmb-row cmb-type-multicheck cmb2-id-srmp3-posttypes">
				<div>
					
					<p><?php esc_html_e( 'Use this tool to quickly add playlist into new post in 1-click! You can either create multiple posts with 1 track each, or create 1 post with multiple tracks. ', 'sonaar-music-pro'); ?></p>
					<p><?php esc_html_e( 'Files below are the ones found in your media library. You can review and publish the draft(s) in bulk once they have been created', 'sonaar-music-pro'); ?></p>
					<p><?php esc_html_e( '* If tracks have been already used into your posts, they will appear in gray below', 'sonaar-music-pro'); ?></p>
				</div>
				<?php
				
				$per_page = isset( $_GET['per_page'] ) ? $_GET['per_page'] : 20;
				$paged    = isset( $_GET['paged'] ) ? $_GET['paged'] : 1;
				$search   = isset( $_GET['search'] ) ? $_GET['search'] : '';
				
				$plugin_admin 		= new Sonaar_Music_Pro_Admin( 'sonaar-music-pro', SRMP3PRO_VERSION );
				$result             = $plugin_admin->sonaar_music_pro_inputs($per_page, $paged, $search);
				$track_inputs       = $result['track_inputs'];
				$paginate_links     = $result['paginate_links'];
				$found_tracks       = $result['found_tracks'];
				
				
				?>
				<div  class="srmp3-music-lists">
					<div class="tablenav top">
						<p class="search-box">
							<label class="screen-reader-text" for="post-search-input">Search Post:</label>
							<input type="search" id="track-search-input" name="s" value="" placeholder="Search tracks...">
						</p>
						<div id="track-pagination" class="tablenav">
								<?php
								echo $paginate_links;
								?>
						</div>
					</div>
					<h2 class="screen-reader-text"><?php esc_html_e('MP3 items list', 'sonaar-music-pro');?></h2>
					<div id="srmp3_music_tracks" class="mp3-music-list">
					<?php
						echo $track_inputs;
					?>
					</div>
					
					<p>
						<a href="#" class="srmp3_toggle_selection" data-type="tracks" data-mode="select"><?php esc_html_e( 'Select all', 'sonaar-music-pro' ); ?></a> | <a href="#" class="srmp3_toggle_selection" data-type="tracks" data-mode="deselect"><?php esc_html_e( 'Deselect all', 'sonaar-music-pro' ); ?></a>
					</p>					
					<br>
					<div id="srmp3-post-type-selection" class="srmp3-post-type-selection">
						<select id="mp3-posttype-selection" name="mp3-posttype-selection" >
							<?php if ( count($srmp3_posttypes) > 1):?>
							<option value=""><?php esc_html_e( 'Select Post Type', 'sonaar-music-pro' );?></option>
							<?php endif;?>
							<?php foreach( get_the_cpt() as $key=>$value):?>
								<?php if( in_array( $key, $srmp3_posttypes)):?>
								<option value="<?php echo esc_attr($key);?>"><?php echo esc_html($value);?></option>
								<?php endif;?>
							<?php endforeach;?>
						</select>
						<?php if ( is_plugin_active( 'woocommerce/woocommerce.php') ) :?>
							<select id="srmp3_woocommerce_product_type" name="srmp3_woocommerce_product_type" style="display:none">
								<option name="srmp3_woocommerce_product_type_simple" value="simple" selected><?php esc_html_e( 'Simple Product', 'sonaar-music-pro' ); ?></option>
								<option name="srmp3_woocommerce_product_type_variable" value="variable"><?php esc_html_e( 'Variable Product', 'sonaar-music-pro' ); ?></option>
							</select>
							<span class="srmp3_woocommerce_product_price" style="display:none">
								<?php esc_html_e( 'Price', 'sonaar-music-pro' ); ?><?php echo esc_html( get_woocommerce_currency_symbol() ); ?><input type="text" id="srmp3_woocommerce_price_tracks" name="srmp3_woocommerce_price_tracks" value="9.99" >
							</span>
						<?php endif;?>
						<br>
						<p>* If your posttype is not listed, make sure it's enabled it in MP3 Player > Settings</p>
					</div>				
					<p>
						<a class="button button-primary srmp3_create_mp3_playlists" disabled><?php esc_html_e( 'Create Post(s) for Each Selected Track(s)', 'sonaar-music-pro' ); ?></a>
						<?php esc_html_e( 'Or', 'sonaar-music-pro');?>
						<a class="button button-primary srmp3_create_single_mp3_playlists" disabled ><?php esc_html_e( 'Create One Post With All Selected Tracks', 'sonaar-music-pro' ); ?></a>
					<p>
					<p><progress class="srmp3_progress srmp3_products_progress_tracks" value="0" max="1"></progress></p>
				</div>
			</div>
		</div>
		
		
	</div>
</div>

<style>

	.mp3-music-list {
		padding: 6px;
		border: 1px solid #8c8f94;
		border-radius: 4px;
		margin-top: 10px;
	}
	.mp3-music-list>div {
		line-height: 1.75rem;
		text-indent: -1.75rem;
		padding-left: 1.75rem;
	}
	.mp3-music-list>div input[type="checkbox"] {
		padding: 10px !important;
		border: 1px solid #9b9d9d;
		background-color: #fff;
		box-shadow: none !important;
	}
	.mp3-music-list>div input[type="checkbox"]:checked {
		background-color: #3692d6;
		border-color: #3692d6;
	}
	.mp3-music-list>div input[type="checkbox"]:checked:before {
		content: url(data:image/svg+xml;utf8,%3Csvg%20xmlns%3D%27http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%27%20viewBox%3D%270%200%20512%20512%27%3E%3Cpath%20fill%3D%27%23fff%27%20d%3D%27M173.898%20439.404l-166.4-166.4c-9.997-9.997-9.997-26.206%200-36.204l36.203-36.204c9.997-9.998%2026.207-9.998%2036.204%200L192%20312.69%20432.095%2072.596c9.997-9.997%2026.207-9.997%2036.204%200l36.203%2036.204c9.997%209.997%209.997%2026.206%200%2036.204l-294.4%20294.401c-9.998%209.997-26.207%209.997-36.204-.001z%27%3E%3C%2Fpath%3E%3C%2Fsvg%3E);
		width: 16px;
		height: 16px;
		margin: -8px 0 0 -8px;
	}
	progress.srmp3_progress {
		width: 100%;
		display: none;
	}
	.srmp3-music-lists {
		margin-top: 10px;
	}
	.srmp3-music-lists .srmp3-post-type-selection > select {
		margin: 0 10px 0 0;
	}
	.srmp3-music-lists #srmp3_woocommerce_price_tracks {
		margin-left: 4px;
	}
	span.disabled {
		opacity: 0.333;
	}
	.srmp3-music-lists .tablenav.top {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 20px;
	}
	.srmp3-music-lists input#track-search-input {
		width: 350px;
		font-size: 16px;
		height: 40px;
	}
	.srmp3-music-lists .button {
		padding: 8px 12px;
		font-size: 16px;
	}
</style>
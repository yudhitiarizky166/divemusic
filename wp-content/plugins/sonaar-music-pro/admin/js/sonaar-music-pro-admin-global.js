jQuery("a[href='edit.php?post_type=album&page=sonaar_music_promo']").parent("li").remove();
jQuery(".wp-admin.post-type-album .postbox#sonaar_promo").remove();
(function( $ ) {
	'use strict';

	var srmp3_updateMp3Playlists = function ( paged = 1, search = '' ) {
		console.log(paged);
		console.log(search);
		var postData = {
					action: 'srmp3_update_mp3_playlists',
					paged,
					search,	
					wpnonce:sonaar_music_pro.ajax_nonce, 
				};
		$.ajax({
				url: ajaxurl,
				type:'post',
				data: postData,
				success: function( result ){
					if ( result.success ) {						
						$('.srmp3-music-lists #track-pagination').html( result.data.paginate_info )
						$('.srmp3-music-lists #srmp3_music_tracks').html( result.data.music_playlists )
					}
				},
		})
		
		
	}

	jQuery( document ).ready(function() {
		eraseSonaarCookie();		
		
		if ($("#mp3-posttype-selection").val() == 'product' ) {
			$( '#srmp3_woocommerce_product_type' ).show();
			$( '.srmp3_woocommerce_product_price' ).show();
		} else {
			$( '#srmp3_woocommerce_product_type' ).hide();
			$( '.srmp3_woocommerce_product_price' ).hide();
		}

		$(document).on( 'change', '#srmp3_woocommerce_product_type', function(e){
			$( '.srmp3_woocommerce_product_price' ).show();
			if( $(this).val() == 'variable'){
				$( '.srmp3_woocommerce_product_price' ).hide();
			}
		});
		$(document).on( 'change', '#mp3-posttype-selection', function(e){
			$( '#srmp3_woocommerce_product_type' ).hide();
			$( '.srmp3_woocommerce_product_price' ).hide();
			if( $(this).val() == 'product'){
				$( '#srmp3_woocommerce_product_type' ).show();
				$( '.srmp3_woocommerce_product_price' ).show();
			}
		});
		$( document ).on('click','#srmp3_music_tracks input[type="checkbox"]', function() {
			var select = false;
			$( '#srmp3_music_tracks input[type="checkbox"]' ).each(function( index ) {
				if ( $(this).prop('checked') === true) {
					select = true;
				}
				
			});
			if (select === true  ) {
				$( '.srmp3_create_mp3_playlists, .srmp3_create_single_mp3_playlists' ).removeAttr('disabled');
			} else {
				$( '.srmp3_create_mp3_playlists, .srmp3_create_single_mp3_playlists' ).attr('disabled', 'disabled');
			}
		});
		$(document).on( 'click', '.srmp3_toggle_selection', function(e){
			e.preventDefault();
			var select = $(this).data('mode') == 'select';
			$('#srmp3_music_tracks input:enabled').each(function(i,e){
				$(e).prop('checked', select);
			});
			if (select === true  ) {
				$( '.srmp3_create_mp3_playlists, .srmp3_create_single_mp3_playlists' ).removeAttr('disabled');
			} else {
				$( '.srmp3_create_mp3_playlists, .srmp3_create_single_mp3_playlists' ).attr('disabled', 'disabled');
			}
			//console.log(select);
		})

		$(document).on( 'keyup keypress', '.srmp3-music-lists .search-box #track-search-input', function(e){

			if (e.keyCode === 13) {
				e.preventDefault();
				srmp3_updateMp3Playlists( 1, e.target.value )
			}

		})
		$(document).on( 'click', '#track-pagination .tablenav-pages .pagination-links a', function(e){
			e.preventDefault();
			var paged = 1
			var page = e.currentTarget.href.match( /paged=(\d+)/ )
			if ( page && page[1] ) {
				paged = page[1]
			}

			srmp3_updateMp3Playlists( paged )
		})

		$(document).on( 'keyup keypress', '#track-pagination #current-page-selector', function(e){

			if (e.keyCode === 13) {
				e.preventDefault();
				srmp3_updateMp3Playlists( e.target.value )
			}

		})

		$(document).on( 'click', '.srmp3_create_mp3_playlists', function(e){
			e.preventDefault();

			if( $('#mp3-posttype-selection').val() == '' ){
				 $('html, body').animate({
                    scrollTop: $("#srmp3-post-type-selection").offset().top - 50
                }, 1000);
				return false;
			}
			var type = 'tracks';
			var index = 0,
				itemsCount = $('#srmp3_music_tracks input:checked').length,
				progress = $('.srmp3_products_progress_tracks');
			var post_type = $('#mp3-posttype-selection').val();
			progress.val(index).prop('max', itemsCount).show();
			$('#srmp3_music_tracks input:checked').each(function(i,e){
				var postData = {
					action: 'srmp3_create_mp3_playlists',
					post_type: $('#mp3-posttype-selection').val(),
					product_type: $('#srmp3_woocommerce_product_type').val(),
					id: $(e).val(),
					price: $('#srmp3_woocommerce_price_tracks').val(),
					tracks: $(e).data('tracks'),
					title: $(e).data('title'),
					wpnonce:sonaar_music_pro.ajax_nonce, 
				}

				$.ajax({
					url: ajaxurl,
					type:'post',
					data: postData,					
					success: function( data ){
					},
				})
				.then( result => {
					index++;
					progress.val(index);
					$(e).prop('checked', false).prop('disabled', true).next().toggleClass('disabled', true);
					if ( index == itemsCount ) {
						progress.parent().append($('<div>', {class: 'notice updated is-dismissible'}).html($('<p>').html(itemsCount + ' ' + post_type + ' drafts successfully created')));
					}
				})
			})
		});
		
		/*  Create Single MP3 Play Lists with multiple track*/
		$(document).on( 'click', '.srmp3_create_single_mp3_playlists', function(e){
			e.preventDefault();

			if( $('#mp3-posttype-selection').val() == '' ){
				 $('html, body').animate({
                    scrollTop: $("#srmp3-post-type-selection").offset().top - 50
                }, 1000);
				return false;
			}
			var mp3_lists_id = [];
			var type = 'tracks';
			var index = 0,
				itemsCount = 1,
				progress = $('.srmp3_products_progress_tracks');
			var post_type = $('#mp3-posttype-selection').val();
			progress.val(index).prop('max', itemsCount).show();
			
			$('#srmp3_music_tracks input:checked').each(function(i,e){				
				mp3_lists_id.push($(e).val()); 
			});
			
			var postData = {
				action: 'srmp3_create_single_mp3_playlists',
				post_type: $('#mp3-posttype-selection').val(),
				product_type: $('#srmp3_woocommerce_product_type').val(),
				mp3_id: JSON.stringify(mp3_lists_id),
				price: $('#srmp3_woocommerce_price_tracks').val(),				
				wpnonce:sonaar_music_pro.ajax_nonce, 
			}

			$.ajax({
				url: ajaxurl,
				type:'post',
				data: postData,				
				success: function( data ){
				},
			})
			.then( result => {
				index++;
				progress.val(index);
				$(e).prop('checked', false).prop('disabled', true).next().toggleClass('disabled', true);
				if ( index == itemsCount ) {
					progress.parent().append($('<div>', {class: 'notice updated is-dismissible'}).html($('<p>').html(itemsCount + ' ' + post_type + ' draft successfully created')));
				}
			})
			
		});

	});

})( jQuery );
function eraseSonaarCookie() {
    jQuery(document).on('click', 'body.album_page_iron_music_player #submit-cmb', function(){
        document.cookie = 'sonaar_mp3_player_settings'+'=; Max-Age=-99999999; path=/';
        document.cookie = 'sonaar_mp3_player_time'+'=; Max-Age=-99999999; path=/';
    })
}




<?php

/**
* The file that defines the core plugin class
*
* A class definition that includes attributes and functions used across both the
* public-facing side of the site and the admin area.
*
* @link       sonaar.io
* @since      1.0.0
*
* @package    Sonaar_Music_Pro
* @subpackage Sonaar_Music_Pro/includes
*/

/**
* The core plugin class.
*
* This is used to define internationalization, admin-specific hooks, and
* public-facing site hooks.
*
* Also maintains the unique identifier of this plugin as well as the current
* version of the plugin.
*
* @since      1.0.0
* @package    Sonaar_Music_Pro
* @subpackage Sonaar_Music_Pro/includes
* @author     Edouard Duplessis <eduplessis@gmail.com>
*/
class Sonaar_Music_Pro {
    
    /**
    * The loader that's responsible for maintaining and registering all hooks that power
    * the plugin.
    *
    * @since    1.0.0
    * @access   protected
    * @var      Sonaar_Music_Pro_Loader    $loader    Maintains and registers all hooks for the plugin.
    */
    protected $loader;
    
    /**
    * The unique identifier of this plugin.
    *
    * @since    1.0.0
    * @access   protected
    * @var      string    $plugin_name    The string used to uniquely identify this plugin.
    */
    protected $plugin_name;
    
    /**
    * The current version of the plugin.
    *
    * @since    1.0.0
    * @access   protected
    * @var      string    $version    The current version of the plugin.
    */
    protected $version;
    
    /**
    * Define the core functionality of the plugin.
    *
    * Set the plugin name and the plugin version that can be used throughout the plugin.
    * Load the dependencies, define the locale, and set the hooks for the admin area and
    * the public-facing side of the site.
    *
    * @since    1.0.0
    */
    public function __construct() {
        
        $this->version = SRMP3PRO_VERSION;
        $this->plugin_name = 'sonaar-music-pro';
        
        
        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
        
        add_action( 'plugins_loaded', array( $this, 'load' ) );
        
        
    }
    public function load() {
        if ( class_exists( 'Sonaar_Music' ) ){
            if ( defined( 'WC_VERSION' ) && get_site_option('SRMP3_ecommerce') == '1') {
                    require_once __DIR__ . '/class-woocommerce.php';
            }
        }
    }
    /**
    * Load the required dependencies for this plugin.
    *
    * Include the following files that make up the plugin:
    *
    * - Sonaar_Music_Pro_Loader. Orchestrates the hooks of the plugin.
    * - Sonaar_Music_Pro_i18n. Defines internationalization functionality.
    * - Sonaar_Music_Pro_Admin. Defines all hooks for the admin area.
    * - Sonaar_Music_Pro_Public. Defines all hooks for the public side of the site.
    *
    * Create an instance of the loader which will be used to register the hooks
    * with WordPress.
    *
    * @since    1.0.0
    * @access   private
    */
    private function load_dependencies() {
       // require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sonaar-music-pro-elementor.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sonaar-music-licences.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sonaar-music-db.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sonaar-music-get.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sonaar-music-post.php';
        
        
        /**
        * The class responsible for orchestrating the actions and filters of the
        * core plugin.
        */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sonaar-music-pro-loader.php';
        
        /**
        * The class responsible for defining internationalization functionality
        * of the plugin.
        */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-sonaar-music-pro-i18n.php';
        
        /**
        * The class responsible for defining all actions that occur in the admin area.
        */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-sonaar-music-pro-admin.php';
        
        /**
        * The class responsible for defining all actions that occur in the public-facing
        * side of the site.
        */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-sonaar-music-pro-public.php';
        

        $this->loader = new Sonaar_Music_Pro_Loader();
        
    }
    
    // public static function post_stats(){
    // 	global $wpdb;
    // 	$sonaar_music_post_data = new Sonaar_Music_Post;
    // 	$request = $_POST['post_stats'];
    
    // 	wp_send_json( $sonaar_music_post_data->log( $request ) );
    // }
    
    /**
    * Define the locale for this plugin for internationalization.
    *
    * Uses the Sonaar_Music_Pro_i18n class in order to set the domain and to register the hook
    * with WordPress.
    *
    * @since    1.0.0
    * @access   private
    */
    private function set_locale() {
        
        $plugin_i18n = new Sonaar_Music_Pro_i18n();
        
        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
        
    }
    
    /**
    * Register all of the hooks related to the admin area functionality
    * of the plugin.
    *
    * @since    1.0.0
    * @access   private
    */
    private function define_admin_hooks() {
        
        $plugin_admin = new Sonaar_Music_Pro_Admin( $this->get_plugin_name(), $this->get_version() );
        
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
        //$this->loader->add_action( 'init', $plugin_admin, 'create_postType' );
        $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'init_options', 9999 );
        $this->loader->add_action( 'wp_ajax_post_stats', $plugin_admin ,'post_stats' );
        $this->loader->add_action( 'wp_ajax_nopriv_post_stats', $plugin_admin ,'post_stats' );
        $this->loader->add_action( 'wp_ajax_get_stats', $plugin_admin ,'get_stats' );
        $this->loader->add_action( 'wp_ajax_nopriv_get_stats', $plugin_admin ,'get_stats' );
        $this->loader->add_action('manage_album_posts_custom_column', $plugin_admin , 'manage_album_custom_column', 10, 2, 9999);
        $this->loader->add_filter('manage_album_posts_columns', $plugin_admin , 'manage_album_columns', 9999);
        $this->loader->add_filter('pre_set_site_transient_update_plugins', $plugin_admin , 'check_for_plugin_update');
        $this->loader->add_filter('plugins_api', $plugin_admin , 'plugin_api_call', 10, 3);
		
		$this->loader->add_action( 'wp_ajax_srmp3_create_mp3_playlists', $plugin_admin ,'srmp3_create_mp3_playlists_ajax' );
		$this->loader->add_action( 'wp_ajax_srmp3_update_mp3_playlists', $plugin_admin ,'srmp3_update_mp3_playlists_ajax' );
		
		$this->loader->add_action( 'wp_ajax_srmp3_create_single_mp3_playlists', $plugin_admin ,'srmp3_create_single_mp3_playlists_ajax' );
        
        
    }
    
    /**
    * Register all of the hooks related to the public-facing functionality
    * of the plugin.
    *
    * @since    1.0.0
    * @access   private
    */
    private function define_public_hooks() {
        
        $plugin_public = new Sonaar_Music_Pro_Public( $this->get_plugin_name(), $this->get_version() );
        
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts', 9999 );
        
    }
    
    /**
    * Run the loader to execute all of the hooks with WordPress.
    *
    * @since    1.0.0
    */
    public function run() {
        $this->loader->run();
    }
    
    /**
    * The name of the plugin used to uniquely identify it within the context of
    * WordPress and to define internationalization functionality.
    *
    * @since     1.0.0
    * @return    string    The name of the plugin.
    */
    public function get_plugin_name() {
        return $this->plugin_name;
    }
    
    /**
    * The reference to the class that orchestrates the hooks with the plugin.
    *
    * @since     1.0.0
    * @return    Sonaar_Music_Pro_Loader    Orchestrates the hooks of the plugin.
    */
    public function get_loader() {
        return $this->loader;
    }
    
    /**
    * Retrieve the version number of the plugin.
    *
    * @since     1.0.0
    * @return    string    The version number of the plugin.
    */
    public function get_version() {
        return $this->version;
    }


    public static function array_insert ( $array, $pairs, $key, $position = 'after' ){
		$key_pos = array_search( $key, array_keys($array) );

		if ( 'after' == $position )
			$key_pos++;

		if ( false !== $key_pos ) {
			$result = array_slice( $array, 0, $key_pos );
			$result = array_merge( $result, $pairs );
			$result = array_merge( $result, array_slice( $array, $key_pos ) );
		}
		else {
			$result = array_merge( $array, $pairs );
		}

		return $result;
	}
    
}

//add_action('wp_footer','sonaar_player', 12);
function sonaar_player(){
    load_template( plugin_dir_path( __FILE__ ) .'player/sonaar-player.php');
}
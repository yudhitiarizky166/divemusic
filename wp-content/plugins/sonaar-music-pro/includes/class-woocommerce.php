<?php
/**
* WC class
*
* SRMP3 WooCommerce class
*
* @link       sonaar.io
* @since      1.0.0
*
* @package    Sonaar_Music_Pro
* @subpackage Sonaar_Music_Pro/includes
*/

defined( 'ABSPATH' ) || exit;

class SRMP3_WooCommerce {

    public static function load() {
		add_action( 'init', array( __CLASS__, 'register_hooks' ) );
	}

    /**
	 * Get  default priority for the product player
	 *
	 * @since  1.0.0
	 * @return array
	 */
	public static function srmp3_get_default_product_player_priority() {
		return array(
			'before'         => 4,
			'after'          => 6,
			'before_rating'  => 9,
			'after_price'    => 11,
			'before_excerpt' => 19,
			'after_excerpt'  => 21,
            'after_add_to_cart'  => 30,
			'before_meta'    => 39,
			'after_meta'     => 41,
		);
	}
    public static function register_hooks() {
        $srmp3_product_player_priority = self::srmp3_get_default_product_player_priority();
        $srmp3_product_player        = self::srmp3_product_player_pos();

        if ($srmp3_product_player !== 'disable'){        
            if ( 'after_summary' === $srmp3_product_player ) {
				add_action( 'woocommerce_after_single_product_summary', array( __CLASS__, 'sr_display_wc_shop_player' ), 10 );
			} else {
				add_action( 'woocommerce_single_product_summary', array( __CLASS__, 'sr_display_wc_shop_player' ), $srmp3_product_player_priority[ $srmp3_product_player ] );
			}
        };

		if (self::srmp3_remove_wc_featured_image()=='true'){
			//add_action( 'woocommerce_before_shop_loop_item_title', array( __CLASS__, 'sr_check_woo_image') , 10);
		}
		add_filter( 'woocommerce_post_class', array( __CLASS__, 'woocommerce_post_class' ), 10, 2 );
        add_action( 'wp_loaded', array( __CLASS__, 'wc_shop_page_hooks' ), 10 );

    }
	/**
	 * Callback function hooked to the 'woocommerce_post_class' filter
	 * Add the 'waveplayer-product' class to a product with preview files
	 *
	 * @since  1.0.0
	 * @param array      $classes The array containing the track info.
	 * @param WC_Product $product The current $product object.
	 * @return array     The filtered array containing the product item classes
	 */
	public static function woocommerce_post_class( $classes, $product ) {
		$srmp3_product_player = Sonaar_Music::srmp3_check_if_audio($product, true);
		
		if($srmp3_product_player ){
			$classes[] = 'srmp3-product';

			if(self::srmp3_remove_wc_featured_image()=='true'){
				$classes[] = 'srmp3-product__hideimage';
			}
		}
		
		
		return $classes;
	}
	/**
	 * Check if featured image shall be removed on our player
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function sr_check_woo_image($product) {
		
		$srmp3_product_player = Sonaar_Music::srmp3_check_if_audio($product, true);
		if( !$srmp3_product_player ){
			//var_dump("these product are NOT using the mp3 player");
			add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
			add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
		}else{
			// Remove product images from the shop loop
			remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
			remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
		}
		
	}

    /**
	 * Get position of the player in wc product template
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function srmp3_product_player_pos() {
		$srmp3player_pos =  Sonaar_Music::get_option('sr_woo_product_position');
		if($srmp3player_pos){
			return Sonaar_Music::get_option('sr_woo_product_position'); 
		}else{
			return 'disable';
		}
        
	}

    /**
	 * Get position of the player in wc shop loop
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function srmp3_shop_player_pos() {
        return Sonaar_Music::get_option('sr_woo_shop_position'); 
	}
	
	/**
	 * Check if featured image shall be removed on our player
	 *
	 * @since 1.0.0
	 * @return string
	 */
	public static function srmp3_remove_wc_featured_image() {
        return Sonaar_Music::get_option('remove_wc_featured_image'); 
	}
    /**
	 * Register action for shop page loop
	 *
	 * @since 1.0.0
	 */
	public static function wc_shop_page_hooks() {

		$srmp3_shop_player = self::srmp3_shop_player_pos(); //disable, before, after

        if ($srmp3_shop_player !== 'disable'){
			//if (self::srmp3_remove_wc_featured_image()=='true' || Sonaar_Music::get_option('sr_woo_shop_position') == 'over_image'){
			if (Sonaar_Music::get_option('sr_woo_shop_position') == 'over_image'){
				add_filter( 'woocommerce_product_get_image', array( __CLASS__, 'filter_srmp3_player_html' ), 10, 2 );				
				
			}else{
			if ($srmp3_shop_player == 'after_item'){ 
				add_action( "woocommerce_after_shop_loop_item", array( __CLASS__, 'sr_display_wc_shop_player' ), 10 );
			}else{	
				add_action( "woocommerce_{$srmp3_shop_player}_shop_loop_item_title", array( __CLASS__, 'sr_display_wc_shop_player' ), 10 );
			}
		}

		}
	
	}

	/**
	 * Return the audio player shortcode
	 *
	 * @since  1.0.0
	 * @param  string	$html   	When used as a filter, the WC markup is replaced.
	 * @param  WC_Product|int $_product The ID or object of the current product.
	 * @return string
	 */
	public static function filter_srmp3_player_html( $image, $_product = null ) {
		if ( self::is_single_product() || is_cart() || self::is_mini_cart() || is_admin() ) {
			return $image;
		}

		global $product;
		if ( is_numeric( $_product ) ) {
			if ( 'attachment' === get_post_type( $_product ) ) {
				$_product = $product;
			} elseif ( 'product' === get_post_type( $_product ) ) {
				$_product = wc_get_product( $_product );
			}
		}

		$woo_srmp3_player = self::woo_srmp3_player ($_product);
		if ($woo_srmp3_player){
			$image = $woo_srmp3_player;
		}
		return $image;
	}

	/**
	 * Check if product related for our filter
	 *
	 * @since  1.0.0
	 * @return boolean
	 */
	public static function is_single_product() {
		if ( is_product() && 'related' !== wc_get_loop_prop( 'name' ) ) {
			return true;
		}
		return false;
	}
	/**
	 * Prevent to filter if its inside mini cart
	 *
	 * @since  1.0.0
	 * @return boolean
	 */
	public static function is_mini_cart() {
		return ( did_action( 'woocommerce_before_mini_cart' ) > did_action( 'woocommerce_after_mini_cart' ) );
	}
	/**
	 * Return the SRMP3 shortcode if it being used in WC product otherwise return false
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public static function woo_srmp3_player( $_product = null ) {
	
		$srmp3_product_player = Sonaar_Music::srmp3_check_if_audio($_product, true);
		
		if( !$srmp3_product_player )
		return;
	
		
		if ( self::is_single_product() ){
			$srmp3_product_skin = Sonaar_Music::get_option('sr_woo_skin_product');
			if ($srmp3_product_skin == 'custom_shortcode'){
				$player_shortcode = Sonaar_Music::get_option('sr_woo_product_player_shortcode');
			}else{
				$player_shortcode = '[sonaar_audioplayer';
				$player_shortcode .= ' hide_timeline="false"';
				$player_shortcode .= ' hide_progressbar="true"';
				$player_shortcode .= ' hide_artwork="true"';
				$player_shortcode .= ' hide_album_title="true"';
				$player_shortcode .= ' hide_album_subtitle="true"';
				$player_shortcode .= ' hide_control_under="true"';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_product_attr_progressbar') == 'true') ? ' hide_progressbar="false"': ' ';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_product_attr_sticky_player') == 'true') ? ' sticky_player="true"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_product_attr_tracklist') == 'true') ? ' show_playlist="true"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_product_attr_albumtitle') == 'true') ? ' hide_album_title="false"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_product_attr_albumsubtitle') == 'true') ? ' hide_album_subtitle="false"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_product_attr_control') == 'true') ? ' hide_control_under="false"': '';
				//$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_product_position') == 'over_image') ? ' hide_artwork="false" display_control_artwork="true" hide_control_under="true"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_product_attr_progress_inline') == 'true') ? ' progressbar_inline="true" hide_timeline="false"': '';
				$player_shortcode .= ' show_album_market="false"';
				$player_shortcode .= ' hide_track_title="true"';
				$player_shortcode .= ' hide_times="true"';
				//$player_shortcode .= ' hide_timeline="true"';

				$player_shortcode .=' ]';
			}			
		}else {
			
			$srmp3_shop_skin = Sonaar_Music::get_option('sr_woo_skin_shop');
			if ($srmp3_shop_skin == 'custom_shortcode'){
				$player_shortcode = Sonaar_Music::get_option('sr_woo_shop_player_shortcode');
			}else{
				$player_shortcode = '[sonaar_audioplayer';
				
				$player_shortcode .= ' hide_artwork="true"';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_shop_attr_progressbar') == 'true') ? ' hide_timeline="false"': ' hide_timeline="true"';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_shop_attr_sticky_player') == 'true') ? ' sticky_player="true"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_shop_attr_tracklist') == 'true') ? ' show_playlist="true"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_shop_position') == 'over_image') ? ' hide_artwork="false" display_control_artwork="true" hide_control_under="true"': '';
				$player_shortcode .= ( Sonaar_Music::get_option('sr_woo_skin_shop_attr_progress_inline') == 'true') ? ' progressbar_inline="true" hide_timeline="false"': '';

				$player_shortcode .= ' hide_album_title="true"';
				$player_shortcode .= ' show_album_market="false"';
				$player_shortcode .= ' hide_track_title="true"';
				$player_shortcode .= ' hide_times="true"';
				//$player_shortcode .= ' hide_timeline="true"';

				$player_shortcode .=' ]';
			}			
		}
		$shortcode = do_shortcode (" $player_shortcode ");
		if (strlen($shortcode) < 10) return false;

		return $shortcode;
	}

    /**
	 * Output SRMP3 player on the shop loop
	 *
	 * @since  1.0.0
	 */
	public static function sr_display_wc_shop_player() {
		$woo_srmp3_player = self::woo_srmp3_player ();
		if ($woo_srmp3_player){
			echo $woo_srmp3_player;
		}
	}
}
SRMP3_WooCommerce::load();
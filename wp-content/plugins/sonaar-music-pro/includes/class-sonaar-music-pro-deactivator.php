<?php

/**
 * Fired during plugin deactivation
 *
 * @link       sonaar.io
 * @since      1.0.0
 *
 * @package    Sonaar_Music_Pro
 * @subpackage Sonaar_Music_Pro/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Sonaar_Music_Pro
 * @subpackage Sonaar_Music_Pro/includes
 * @author     Edouard Duplessis <eduplessis@gmail.com>
 */
class Sonaar_Music_Pro_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}

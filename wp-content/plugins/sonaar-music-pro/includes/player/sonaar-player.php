<div v-cloak id="sonaar-player" :class="[{enable: !minimize , 'show-list': showList, 'hide-track-lenght': hideDuration, 'hide-track-category': hideCategory, sr_hide: classes.emptyPlayer }, classType]" style="display:none;">
  <audio id="sonaar-audio" v-if="!classes.wavesurferEnable"></audio>
  <transition name="sonaar-player-slidefade" v-on:after-enter="playlistAfterEnter">
    <div class="playlist" v-if="showList">
      <div class="scroll">
        <div class="container">
          <div class="boxed">
            <div class="title"  v-if="(playListTitle.length >= 1)">{{playListTitle}}</div>
            <div class="shuffle" @click="enableRandomList">
              <div v-if="shuffle">
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                  <path d="M18.2,13.2c-0.1-0.1-0.4-0.1-0.5,0c-0.1,0.1-0.1,0.4,0,0.5l2.1,2h-3.6c-0.9,0-2.1-0.6-2.7-1.3L10.9,11l2.7-3.4
                  c0.6-0.7,1.8-1.3,2.7-1.3h3.6l-2.1,2c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.2,0.1,0.3,0.1c0.1,0,0.2,0,0.3-0.1L21,6.2
                  c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3L18.2,3c-0.1-0.1-0.4-0.1-0.5,0c-0.1,0.1-0.1,0.4,0,0.5l2.1,2h-3.6
                  c-1.1,0-2.5,0.7-3.2,1.6l-2.6,3.3L7.8,7.1C7.1,6.2,5.7,5.5,4.6,5.5H1.3c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4h3.3
                  c0.9,0,2.1,0.6,2.7,1.3L9.9,11l-2.7,3.4c-0.6,0.7-1.8,1.3-2.7,1.3H1.3c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4h3.3
                  c1.1,0,2.5-0.7,3.2-1.6l2.6-3.3l2.6,3.3c0.7,0.9,2.1,1.6,3.2,1.6h3.6l-2.1,2c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.2,0.1,0.3,0.1
                  c0.1,0,0.2,0,0.3-0.1l2.7-2.7c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3L18.2,13.2z"/>
                </svg>
              </div>
              <div v-else>
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                  <path d="M19,15.4H3.2l2.8-2.7c0.1-0.1,0.1-0.3,0-0.5c-0.1-0.1-0.3-0.1-0.5,0l-3.3,3.3C2.1,15.5,2,15.6,2,15.7c0,0.1,0,0.2,0.1,0.2
                  l3.3,3.3c0.1,0.1,0.1,0.1,0.2,0.1c0.1,0,0.2,0,0.2-0.1c0.1-0.1,0.1-0.3,0-0.5L3.2,16H19c0.2,0,0.3-0.1,0.3-0.3
                  C19.3,15.5,19.1,15.4,19,15.4z M20.3,7.2l-3.3-3.3c-0.1-0.1-0.3-0.1-0.5,0c-0.1,0.1-0.1,0.3,0,0.5l2.8,2.7H3.5
                  c-0.2,0-0.3,0.1-0.3,0.3c0,0.2,0.1,0.3,0.3,0.3h15.8l-2.8,2.7c-0.1,0.1-0.1,0.3,0,0.5c0.1,0.1,0.1,0.1,0.2,0.1c0.1,0,0.2,0,0.2-0.1
                  l3.3-3.3c0.1-0.1,0.1-0.1,0.1-0.2C20.4,7.3,20.3,7.3,20.3,7.2z"/>
                </svg>
              </div>
            </div>
            <button class="play" @click="play">{{ isPlaying ? 'pause': 'play' }}</button>
            <div class="trackscroll">
              <ul class="tracklist">
                <li v-for="(track, index) in list.tracks" :key="track.id" @click="currentTrack = index; playAudio();" :class="index == currentTrack ? 'active' : '' ">
                  <span class="track-status">{{ index + 1 }}</span>
                  <span class="track-title"><span class="content" @mouseover="scroll">{{ track.track_title }}</span></span>
                  <span class="track-artist"  v-if="classes.author"><span class="content" v-if="track.track_artist">{{ track.track_artist }}</span></span>
                  <span class="track-album"><span class="content">{{ track.album_title }}</span></span>
                  <span class="track-lenght" v-if="track.lenght"><span class="content">{{ track.lenght }}</span></span>
                  <span class="track-store" v-if="(track.song_store_list.length >= 1 )">
                    <a v-for="store in track.song_store_list" :href="store['store-link']" :target="!store['store-target'] ? '_blank' : '_self'" :download="store['store-icon'] == 'fas fa-download' ? '' : false"><i class="" :class="store['store-icon']"></i></a>
                  </span>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

  </transition>

  <div class="close btn_playlist" v-if="showList" @click="setshowList"></div>
  <div class="close btn-player" :class="{enable: !minimize, 'storePanel':list.tracks.length >= 1 && playerCallToAction.length >= 1}" @click="closePlayer" v-if="list.tracks.length >= 1">
    <svg class="audioBar" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 17 17" enable-background="new 0 0 17 17" xml:space="preserve">
      <rect x="0" width="2" height="16" transform="translate(0)">
        <animate attributeName="height" attributeType="XML" dur="1s" values="2;16;2" repeatCount="indefinite" />
      </rect>
      <rect x="5" width="2" height="16" transform="translate(0)">
        <animate attributeName="height" attributeType="XML" dur="1s" values="2;16;2" repeatCount="indefinite" begin="0.3s" />
      </rect>
      <rect x="10" width="2" height="16" transform="translate(0)">
        <animate attributeName="height" attributeType="XML" dur="1s" values="2;16;2" repeatCount="indefinite" begin="0.5s" />
      </rect>
      <rect x="15" width="2" height="16" transform="translate(0)">
        <animate attributeName="height" attributeType="XML" dur="1s" values="2;16;2" repeatCount="indefinite" begin="0.3s" />
      </rect>
    </svg>
  </div>

  <div :class="(list.tracks.length >= 2)?'player ':'player no-list '">
    <div class="mobileProgress">
      <div class="skip" @mouseup="skipTo"></div>
      <div class="mobileProgressing wave-cut" :style=" css.wavecut "></div>
      <div class="progressDot" :style=" css.progressDot "></div>
    </div>
    <div class="player-row">

      <div class="playerNowPlaying" v-if="list.tracks.length >= 1">
        <div class="album-art" :class="{'loading-enable': !classes.waveEnable, 'loading': loading < 100 }" v-if="hasArtwork">
          <i class="fa fa-circle-o-notch fa-spin fa-2x fa-fw loading-icon"></i>
          <img class="hover" :src="list.tracks[currentTrack].poster" />
          <img :src="list.tracks[currentTrack].poster" />
        </div>
        <div :class="(hasArtwork)?'metadata ':'metadata no-image '">
          <div class="track-name" @mouseover="scroll">{{list.tracks[currentTrack].track_title}}{{list.tracks[currentTrack].track_artist? list.artist_separator :''}}{{list.tracks[currentTrack].track_artist}}</div>
          <div class="track-album" @mouseover="scroll" v-if="list.tracks[currentTrack].album_title">{{list.tracks[currentTrack].album_title}}</div>
          <div class="track-artist" @mouseover="scroll" v-html="'by ' + list.tracks[currentTrack].album_artist" v-if="!classes.author && list.tracks[currentTrack].album_artist"></div>
        </div>
      </div>
      <div class="playerNowPlaying" v-else></div>
      <div class="control">
        <div class="list control--item" @click="setshowList" v-if="!classes.playlist && list.tracks.length > 1">
          <svg width="100%" height="100%" viewBox="0 0 24 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve">
            <g>
              <rect x="0" y="0" width="24" height="2" />
              <rect x="0" y="6" width="24" height="2" />
              <rect x="0" y="12" width="24" height="2" />
              <rect x="0" y="18" width="15" height="2" />
            </g>
          </svg>
        </div>
        <div class="previous control--item" @click="previous" v-if="!classes.playlist && list.tracks.length > 1">
          <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 10.2 11.7" style="enable-background:new 0 0 10.2 11.7;" xml:space="preserve">
            <polygon points="10.2,0 1.4,5.3 1.4,0 0,0 0,11.7 1.4,11.7 1.4,6.2 10.2,11.7"></polygon>
          </svg>
        </div>
        <div class="play control--item" @click="play" :class="{'audio-playing': isPlaying }">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 17.5 21.2" style="enable-background:new 0 0 17.5 21.2;" xml:space="preserve">
            <path d="M0,0l17.5,10.9L0,21.2V0z"></path>
            <rect width="6" height="21.2"></rect>
            <rect x="11.5" width="6" height="21.2"></rect>
          </svg>
        </div>
        <div class="next control--item" @click="next" v-if="!classes.playlist && list.tracks.length > 1">
          <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 10.2 11.7" style="enable-background:new 0 0 10.2 11.7;" xml:space="preserve">
            <polygon points="0,11.7 8.8,6.4 8.8,11.7 10.2,11.7 10.2,0 8.8,0 8.8,5.6 0,0"></polygon>
          </svg>
        </div>
        <div class="shuffle control--item" @click="enableRandomList" v-if="!classes.playlist && list.tracks.length > 1">
          <div v-if="shuffle">
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
              <path d="M18.2,13.2c-0.1-0.1-0.4-0.1-0.5,0c-0.1,0.1-0.1,0.4,0,0.5l2.1,2h-3.6c-0.9,0-2.1-0.6-2.7-1.3L10.9,11l2.7-3.4
              c0.6-0.7,1.8-1.3,2.7-1.3h3.6l-2.1,2c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.2,0.1,0.3,0.1c0.1,0,0.2,0,0.3-0.1L21,6.2
              c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3L18.2,3c-0.1-0.1-0.4-0.1-0.5,0c-0.1,0.1-0.1,0.4,0,0.5l2.1,2h-3.6
              c-1.1,0-2.5,0.7-3.2,1.6l-2.6,3.3L7.8,7.1C7.1,6.2,5.7,5.5,4.6,5.5H1.3c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4h3.3
              c0.9,0,2.1,0.6,2.7,1.3L9.9,11l-2.7,3.4c-0.6,0.7-1.8,1.3-2.7,1.3H1.3c-0.2,0-0.4,0.2-0.4,0.4c0,0.2,0.2,0.4,0.4,0.4h3.3
              c1.1,0,2.5-0.7,3.2-1.6l2.6-3.3l2.6,3.3c0.7,0.9,2.1,1.6,3.2,1.6h3.6l-2.1,2c-0.1,0.1-0.1,0.4,0,0.5c0.1,0.1,0.2,0.1,0.3,0.1
              c0.1,0,0.2,0,0.3-0.1l2.7-2.7c0.1-0.1,0.1-0.2,0.1-0.3c0-0.1,0-0.2-0.1-0.3L18.2,13.2z"/>
            </svg>
          </div>
          <div v-else>
            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
            viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
              <path d="M19,15.4H3.2l2.8-2.7c0.1-0.1,0.1-0.3,0-0.5c-0.1-0.1-0.3-0.1-0.5,0l-3.3,3.3C2.1,15.5,2,15.6,2,15.7c0,0.1,0,0.2,0.1,0.2
              l3.3,3.3c0.1,0.1,0.1,0.1,0.2,0.1c0.1,0,0.2,0,0.2-0.1c0.1-0.1,0.1-0.3,0-0.5L3.2,16H19c0.2,0,0.3-0.1,0.3-0.3
              C19.3,15.5,19.1,15.4,19,15.4z M20.3,7.2l-3.3-3.3c-0.1-0.1-0.3-0.1-0.5,0c-0.1,0.1-0.1,0.3,0,0.5l2.8,2.7H3.5
              c-0.2,0-0.3,0.1-0.3,0.3c0,0.2,0.1,0.3,0.3,0.3h15.8l-2.8,2.7c-0.1,0.1-0.1,0.3,0,0.5c0.1,0.1,0.1,0.1,0.2,0.1c0.1,0,0.2,0,0.2-0.1
              l3.3-3.3c0.1-0.1,0.1-0.1,0.1-0.2C20.4,7.3,20.3,7.3,20.3,7.2z"/>
            </svg>
          </div>
        </div>
      </div>
      <div class="wavesurfer">
        <div class="timing">
            <div class="time timing_currentTime">{{ currentTime }}</div>
            <div class="time timing_totalTime">{{ totalTime }}</div>
        </div>
        <div class="sr-progress" v-show="!classes.waveEnable">
          <div class="sr-wavebar progress_totalTime" :style=" css.waveColor "></div>
          <div class="sr-wavebar progress_currentTime" :style=" css.progressColor "></div>
          <div class="skip" @mouseup="skipTo"></div>
        </div>
        <div class="volume">
          <div class="icon" @click="muteTrigger">
            <div v-if="mute">
              <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                  viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                <path d="M11.7,19c0,0.3-0.1,0.6-0.3,0.8c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.6-0.1-0.8-0.3l-4.1-4.1H1.1c-0.3,0-0.6-0.1-0.8-0.3
                  C0.1,15.2,0,14.9,0,14.6V8c0-0.3,0.1-0.6,0.3-0.8C0.5,7,0.8,6.9,1.1,6.9h4.7l4.1-4.1c0.2-0.2,0.5-0.3,0.8-0.3c0.3,0,0.6,0.1,0.8,0.3
                  c0.2,0.2,0.3,0.5,0.3,0.8V19z"/>
                <g>
                  <path d="M17.2,11.2l1.7,1.7c0.1,0.1,0.1,0.2,0.1,0.4c0,0.1,0,0.3-0.1,0.4L18.5,14c-0.1,0.1-0.2,0.1-0.4,0.1c-0.1,0-0.3,0-0.4-0.1
                    l-1.7-1.7L14.4,14c-0.1,0.1-0.2,0.1-0.4,0.1c-0.1,0-0.3,0-0.4-0.1l-0.4-0.4c-0.1-0.1-0.1-0.2-0.1-0.4c0-0.1,0-0.3,0.1-0.4l1.7-1.7
                    l-1.7-1.7c-0.1-0.1-0.1-0.2-0.1-0.4c0-0.1,0-0.3,0.1-0.4l0.4-0.4c0.1-0.1,0.2-0.1,0.4-0.1c0.1,0,0.3,0,0.4,0.1l1.7,1.7l1.7-1.7
                    c0.1-0.1,0.2-0.1,0.4-0.1c0.1,0,0.3,0,0.4,0.1l0.4,0.4C18.9,8.9,19,9.1,19,9.2c0,0.1,0,0.3-0.1,0.4L17.2,11.2z"/>
                </g>
              </svg>
            </div>
            <div v-else >
                <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 22 22" style="enable-background:new 0 0 22 22;" xml:space="preserve">
                <g>
                  <path d="M11.7,19c0,0.3-0.1,0.6-0.3,0.8c-0.2,0.2-0.5,0.3-0.8,0.3c-0.3,0-0.6-0.1-0.8-0.3l-4.1-4.1H1.1c-0.3,0-0.6-0.1-0.8-0.3
                    C0.1,15.2,0,14.9,0,14.6V8c0-0.3,0.1-0.6,0.3-0.8C0.5,7,0.8,6.9,1.1,6.9h4.7l4.1-4.1c0.2-0.2,0.5-0.3,0.8-0.3
                    c0.3,0,0.6,0.1,0.8,0.3c0.2,0.2,0.3,0.5,0.3,0.8V19z M17.1,9.2c-0.4-0.7-0.9-1.2-1.6-1.6c-0.3-0.2-0.7-0.3-1.1-0.2
                    C14,7.5,13.7,7.7,13.5,8c-0.2,0.4-0.3,0.7-0.2,1.1c0.1,0.4,0.3,0.7,0.7,0.9c0.5,0.3,0.7,0.7,0.7,1.2c0,0.5-0.2,0.9-0.6,1.2
                    c-0.3,0.2-0.5,0.6-0.6,1s0,0.8,0.3,1.1c0.2,0.3,0.5,0.5,0.9,0.6c0.4,0.1,0.8,0,1.1-0.2c0.6-0.4,1-1,1.4-1.6c0.3-0.6,0.5-1.3,0.5-2
                    C17.6,10.6,17.4,9.8,17.1,9.2z M20.9,7c-0.8-1.3-1.8-2.4-3.1-3.2c-0.3-0.2-0.7-0.3-1.1-0.2c-0.4,0.1-0.7,0.3-0.9,0.7
                    c-0.2,0.4-0.3,0.7-0.2,1.1c0.1,0.4,0.3,0.7,0.7,0.9c0.9,0.5,1.5,1.2,2,2.1c0.5,0.9,0.8,1.8,0.8,2.9c0,0.9-0.2,1.8-0.7,2.7
                    c-0.4,0.9-1.1,1.6-1.9,2.1c-0.3,0.2-0.5,0.6-0.6,1c-0.1,0.4,0,0.8,0.3,1.1c0.3,0.4,0.7,0.6,1.2,0.6c0.3,0,0.6-0.1,0.8-0.3
                    c1.2-0.8,2.1-1.9,2.8-3.2c0.7-1.3,1-2.6,1-4.1C22,9.8,21.6,8.3,20.9,7z"/>
                </g>
                </svg>
            </div>

          </div>
          <div class="slider-container">
            <div class="slide"></div>
          </div>
        </div>
        <div id="sPlayer" class="wave" :class="{reveal: classes.waveEnable, sr_hide: !classes.waveEnable, sonaar_fake_wave: !classes.wavesurferEnable}">
          <div class="sonaar_wave_base" v-if="!classes.wavesurferEnable"><svg></svg>
            <canvas id="splayer-wave-container" height="60" width="2540"></canvas>
          </div>
          <div class="sonaar_wave_cut" v-if="!classes.wavesurferEnable"><svg></svg>
          <canvas id="splayer-wave-progress" height="60" width="2540"></canvas>
          </div>
        </div>
      </div>
      <div class="sonaar-extend-button" @click="showCTA" v-if="list.tracks.length >= 1 && playerCallToAction.length >= 1">
        <i class="fas fa-ellipsis-v"></i>
      </div>
      <!--<transition name="sonaar-player-storefade">
      <div class="store" v-if="list.tracks.length >= 1 && playerCallToAction.length >= 1" :style="{width: storeWidth }">
        <ul class="track-store" v-if="playerCallToAction.length >= 1">
          <li v-for="store in playerCallToAction">
           <a :href="store['store-link']" :target="!store['store-target'] ? '_blank' : '_self'" :download="store['store-icon'] == 'fas fa-download' ? '' : false"><i :class="store['store-icon']"></i>{{store['store-name']}}</a>
          </li>
        </ul>
      </div>
      </transition>-->
      
      <transition name="sonaar-player-storefade">
      <div class="store" v-if="list.tracks.length >= 1 && albumStoreList.length >= 1">
          <ul class="track-store" v-if="albumStoreList.length >= 1">
            <li v-for="store in albumStoreList">
            <a :href="store['store-link']" :target="!store['store-target'] ? '_blank' : '_self'" v-bind:class="{ sr_store_round_bt : store['store-type']=== 'wc'}" :download="store['store-icon'] == 'fas fa-download' ? '' : false"><i :class="store['store-icon']"></i>{{store['store-name']}}</a>
            </li>
          </ul>
        </div>
      </div>
      </transition>
  </div>
</div>
=== MP3 Audio Player for Music, Radio & Podcast by Sonaar ===
Contributors: sonaar, eduplessis
Tags: MP3, audio player, HTML5 mp3 player, HTML5 audio player, audioplayer, elementor, podcast, audio, woo commerce, beat store, beat, music store, gutenberg
Donate link: https://sonaar.io
Requires at least: 4.7
Tested up to: 5.8
Requires PHP: 5.6
Stable tag: 2.4.3
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

The most advanced Audio Player. Support Elementor, Gutenberg Block Editor, WooCommerce & Shortcode. Add nice soundwave design, music & playlist to posts and CPT.

== Description ==

MP3 Music Player by Sonaar is a very easy Audio Player for WordPress. It gives you the ability to add unlimited numbers of playlists, albums, and audio tracks to any post, WooCommerce products, or custom posts using our Elementor Widget, Block editor, or native shortcode with tons of attributes. It's very flexible and easy to use. It's the perfect companion for your WooCommerce store if you plan to sell beats and music on your website.

You can display an optional jaw-dropping waveform bar under any of your audio players powered by WaveSurfer.js. Our MP3 Player is super easy to use, includes tons of features and the design and UX are very professional. You can choose between a super-nice-looking waveform and a very simple progress bar design.

Upload your MP3 file from your page, post, WC product, custom post, or directly in Elementor!

[youtube https://youtu.be/5_d59QwubT0]

There are 4 easy ways to use the audio player. 

1) If you use Gutenberg, just add the MP3 player block in the Gutenberg Block Editor.
2) If you use Elementor, just add the MP3 Player Elementor Widget.
3) You can also use our shortcode anywhere on your site.
4) If you use the Pro version, you can use our bulk playlist creation tool: Select multiple MP3 files and it will create posts, products and custom post in 1-click!

★★★★★
> “If you need an mp3 player for your website, just use MP3 Music Player by Sonaar. You aren't going to find anything better...” - [Intarwebsdeveloper](https://wordpress.org/support/users/intarwebsdeveloper/)

★★★★★
> “This is hands down the best Audio Player plugin I have found for WP ... for anyone who is attempting to build a large music catalogue. Thanks guys!!” - [Nebsounds](https://wordpress.org/support/users/nebsounds/)

★★★★★
> “This plugin is really AMAZING. Thanks for build this!!” - [Walterk7](https://wordpress.org/support/users/walterk7/)

### MP3 PLAYER DEMO & EXAMPLE

[View live demos here](https://goo.gl/mVUJEJ)
[Watch Videos Tutorial](https://youtu.be/5_d59QwubT0)

### EASY-TO-USE AUDIO PLAYER

No XML, JSON or FTP is required to use this HTML5 audio player! Just upload your MP3, M4A or FLAC tracks within WordPress and add the playlist anywhere on your site using Gutenburg, shortcode or by using Elementor! This free plugin will automatically fetch metadata from your files to autocomplete the album titles & track names.

It's a fully responsive MP3 player and it works on desktop, tablets, and other mobile devices!

### AWESOME DESIGN

This professional audio player has been designed by our design experts. With over 15 years of experience as UI/UX Design and WP Development, we have come up one of the best, easy-to-use, feature-rich plugin to play audio file on your website. It's a pixel-perfect Music Player for WordPress with awesome waveform.( Waveform is also known as audio spectrum visualizer, soundwave, visual equalizer, wavesurfer or audio sound graphic). 

### TONS OF FEATURES

### FREE MP3 PLAYER - FEATURES

* Create unlimited audio albums & playlists
* Upload unlimited MP3/M4A files
* Support MP3, audio streaming and radio streaming
* Upload audio tracks from any posts, custom posts or pages
* Embed audio players using short-codes
* Embed audio players using our Elementor Widget (NEW!)
* Embed audio players using our Gutenberg Block Editor (NEW!)
* Support for Elementor Page Builder
* Add Download Now buttons for each track & album
* Real-time Dynamic Soundwave FX (Waveform by wavesurfer.js) or Synthetic Soundwave for faster loading
* Unlimited color for your MP3 widget
* Support for Easy Digital Downloads
* Support for Google Fonts 
* Automatically fetchs ID3 Tags from your MP3 files and show an optional MP3 waveform chart.
* No XML, Json or FTP is required. Upload and setup everything through WordPress.
* Show/Hide tracklist of your MP3 audio player
* Cover album beside the audio widget is optional
* Add social icons for each of your tracks such as "Download", "Buy tracks", "SoundCloud", etc.
* Add call-to-action buttons for each of your album such as "Listen on Spotify, SoundCloud, BandCamp", etc."
* HTML5 Player so it's fully responsive, mobile-friendly and play across all plateforms.
* Check our PRO version for more exciting features...

### SUPPORTED FILES AND STREAMING SERVICES

We support any MP3 files and these streaming platforms:
- Icecast
- Libsyn
- Stitcher
- Shoutcast
- Acast
- Amazon S3
- FMStream.org
- Podbean
- SoundCloud Podcasts
- Buzzsprout
- Simplecast
- Spreaker
- Audioboom
- CastBox
- Pippa.io
- Anchor

You cannot stream directly from these services:
x Spotify
x SoundCloud Music
x Apple Podcast
x Google Podcast
x YouTube
x MixCloud

### SHORTCODES PARAMETERS

Official Shortcode:
`[sonaar_audioplayer albums="YOUR_PLAYLIST_ID"]`


Attributes:

albums - The post ID of your playlist you want to display. Required. IDs only separated by a comma
show_track_market - Display your track's store icons beside each of your track. Value: true or false. Default: true
show_album_market - Display your playlist's store icons below your playlist. Value: true or false. Default: true
store_title_text - Change Available Now by any string. Value: String. Default: "Available Now"
hide_artwork - Hide your album cover. Value: true or false. Default: false
remove_player - Disable and remove the waveform below your player. Value: true or false. Default: false
display_control_artwork - Display controls over your player artwork. Value: true or false. Default: false
progressbar_inline - Display controls inline with the soundwave. Value: true or false. Default: false
hide_progress - Remove progress bar but keep Prev/Play/Next controls. Value: true or false. Default: false
hide_times - Remove time durations in the progress bar. Value: true or false. Default: false
hide_control_under - Remove Prev/Play/Next controls under the progress bar and keep progress bar only. Value: true or false. Default: false
wave_color - Specify the color of your waveform to bypass the plugin setting's color: Value. hexa color. Default: Plugin settings
wave_progress_color - Specify the color of your progress bar over your waveform to bypass the plugin setting's color. Value: hexa color. Default: Plugin settings
show_playlist - Either or not display the playlist above your player control. Value: true or false. Default: true

eg:
`[sonaar_audioplayer albums="6" hide_artwork="" show_playlist="true" show_track_market="true" show_album_market="true" wave_color="#000000" wave_progress_color="#CCCCCC"][/sonaar_audioplayer]`

Click here for a [complete list of Shortcode Attributes](https://sonaar.ticksy.com/article/15282)

WAIT! We have also a [Premium WordPress version](https://goo.gl/mVUJEJ).
**Features of the Premium Version**

### PRO MP3 PLAYER - FEATURES

Everything in free, plus:

* Sticky Footer Player with Soundwave
* (NEW!) Continuous Audio Playback / Persistent Player
* (NEW!) Full Support for WooCommerce. [View WooCommerce Demo](https://beatstore.sonaar.io/rebirth/)
* (NEW!) Add Audio Player for WooCommerce in product image, shop page and single product template automatically!. Add Buy Now, Add to Cart button with price for your WC products
* (NEW!) Customize the look and feel of the player directly within Gutenberg Block Editor! No custom CSS is required! Over 70 styling options are available.
* (NEW!) Add popup lightbox option for call-to-action buttons beside your tracks. Embed anything from lyrics, videos, html or shortcodes in the Lightbox.
* Elementor Widget with 70+ Styling Options and dynamic fields!
* Scrollbar option to scroll within your track-list
* Display thumbnail images beside each of your tracks in the playlist
* Volume Control
* Shuffle Tracks
* Tracklist View with the Sticky Player
* Option to automatically stop player when track is complete
* Option to set an overall sticky player that will load on all your pages
* Tool to create posts in bulk only by selecting the audio files
* Importation tool from MP3 files.
* Support ACF - Advanced Custom Fields
* Statistic Reports
* View listen counts on every tracks, playlists and audio player within WordPress
* View tracks downloads statistic
* Top Played Tracks/ & MP3player charts
* Filter charts by days, weeks and months
* Get insights reports directly in your dashboard for all of your audio players.
* 12 months of priority support through our live chat!
* 12 months of automatic plugin updates!

[View Demo of Pro Version](https://goo.gl/mVUJEJ)

### WHO CAN USE THIS AUDIO MUSIC PLAYER?

This plugin is designed for:

* Music Producers
* Musicians
* Artists
* Record Labels
* eCommerce
* Beat Store
* Audio Studio
* Recording Studio
* Bloggers
* DJs
* Preachers
* Sermonizers
* Audio & eBook Websites
* Podcasters
* Online Course Creators
* Music Store Owner
* Membership sites
* Digital product stores
* Agencies
* Freelancers
* Non-Profits
* Charities

**Anyone who can benefit from adding audio on their website can benefit from using this audio player!**

### PREMIUM SUPPORT
The Sonaar Team does not always provide active support for the FREE MP3 Player plugin on the WordPress.org forums, as we prioritize our dedicated helpdesk support. Priority support is available to people who bought [MP3 Player PRO](https://goo.gl/mVUJEJ) only. 


### ABOUT SONAAR MUSIC

This free WordPress MP3 Player plugin has been developed by [Sonaar Music](https://goo.gl/tTcJ1Y). Our award-winning Music WordPress Themes & Plugins empower thousands of artists around the world.We provide stunning WordPress themes crafted for DJs, Artists, Podcasters, Music Bands and Record Labels. By offering beautiful and unique themes and plugins to the music industry and providing outstanding friendly customer support, we help our backers to build a strong brand awareness so they can engage more fans and followers. Our design templates for WordPress can be adapted to any style of music from Hip-hop, Jazz, DJ, Techno, Electro, R&B, Rap and EDM Music.

== Installation ==
1. Install *MP3 Music Player for WordPress by Sonaar* like any other plugin. [Check out the codex](https://codex.wordpress.org/Managing_Plugins#Installing_Plugins) if you have any questions.
2. After installing and activating the plugin, you will see a new menu item called *MP3 Player*
3. Add a new playlist and add your MP3 tracks. You can also upload your track from any pages and posts.
4. To add an album cover to a player, upload your album cover (.JPG, .PNG only) in the featured image of the post. The recommended size is a square image of 600x600. If you upload an image less than 600x600, it will be pixelated.
5. To display the mp3 audioplayer on any of your page, add the MP3 Player Elementor Widget if you use Elementor - OR - the MP3 player Gutenberg block if you use Gutenberg - OR - insert the `[sonaar_audioplayer]` shortcode. To help you to generate the shortcode, simply click the small music icon. [see screensot](https://d.pr/i/BRsRBL). Make sure to select which album you want to use by selecting at least one album. 


== Screenshots ==
1. Audio Player for Gutenberg Block Editor
2. Unlimited Look and feel possibilities
3. MP3 player with Waveform
4. Sell Music Online. Support for WooCommerce.
5. Infinite Options to customize your music player
6. Elementor Widget Available
7. Custom Fields to upload or stream tracks
8. Playlist Post Type CPT
9. Playlist Statistic with Pro
10. General Settings Dashboard

== Changelog ==
= 2.4.3 =
- Fix issue with URL links in the call-to-action buttons since version 2.4.2
- Added better Polylang compatibility by adding lang parameter on get_posts


= 2.4.2 =
- Fix XSS vulnerabilities. Thanks Robert from WPScan.
- New! Add Elementor Option: Center Cover Image Vertically with tracklist
- New! Add option to show text label beside each CTA button
- Tweak: WooCommerce. Remove add-to-cart strings from URL when product has variation
- Update: CMB2 to 2.9.0
- Fix: Compatibility issue with FontAwesome 5 within Elementor
- Fix: Prevent image cover to be offset when its larger than the playlist
- Tweak: Add svg size in the DOM for CLS improvement

= 2.4.1 =
- Fix player width issue where the player is too large for the page container in some case

= 2.4 =
- New! [Pro Feature] Gutenberg block has now all the options to customize your player the way you want! Create stunning and unique player look and feel without leaving Gutenberg editor!
- New! [Pro Feature] Added popup Lightbox option for call-to-action buttons beside the tracks. Embed anything from lyrics, videos, html or shortcode in the Lightbox. Edit your playlist track's call-to-action and you will see the popup option available.
- Tweak: Removed Gutenberg Select2 Playlist option and replaced it for selectControl Block Editor Native component
- Tweak: If no album custom post type are created, the default CPT name will be sr_playlist
- Tweak: MP3 Player is now fully compatible with the Sonaar WordPress Theme. Enjoy a true AJAX Continuous Player when using this player along with the Sonaar WP Theme
- Fix: Issue when using 2 gutenberg MP3 Player blocks in the same page.
- Fix: Print issue: When playlist and artwork are hidden, the browser print option doesnt work. Too many pages to print.
- Fix: Issue with audio player not loading when used in Elementor Popups or lightboxes.
- Fix: shuffle issue when using the Gutenberg block
- Fix: when using very long track title, it overlapped the control buttons
- Tweak: Updated moment.js library to 2.29.1

= 2.3.2 =
- New! [Pro Feature] Added new sticky players layouts: Introducing the Float Interactive Sticky Player with drag and drop option and Full-width Mini Sticky
- Tweak: Add Flex CSS on sticky player for more controls on CSS (pro)
- Tweak: Load sr-scripts.js file only if using Elementor
- Tweak: Added Current Post option in Playlist Source dropdown in Elementor Widget. Useful when using dynamic page templates. Eg: Elementor Pro Theme Builder
- Tweak: Renamed Google Play for YouTube Music, iTunes for Apple Music
- Tweak: improvement to the bulk import tool (pro)
- Tweak: Do not sanitize CTA URLs textfield to prevent ascii character from being stripped
- Tweak: tracklist item li 100vw instead of 100% so all li displays with same width
- Tweak: Prevent theme text-decoration on button CTA
- Fix issue with cmb2_render_select_multiple_field_type classname conflict with other third party plugins
- Fix: Optional Track image option now displayed when editing Pages
- Fix issue when playing same track number with 2 different player
- Fix issue with the progress bar displacement caused by the time progress
- Fix to prevent track numbers to wrap on track 10th and more
- Fix to prevent time duration to wrap
- Fix PHP error / blank page on front-end when using pro without free version activated
- Fix issue with 1 track playlist that was always looping even if the option do not skip to next track was disabled

= 2.3.1 =
- No change

= 2.3 =
- New! Add optional track cover images for each of your track
- New! Simple progress bar design option
- New! Option to adjust the bar width and spacing of the synthetic waveform
- New! Option to adjust the height of the all waveform types.
- New! Shortcode attributes: display_control_artwork, progressbar_inline, hide_progress, hide_track_title, hide_times . More info https://sonaar.ticksy.com/article/15282/
- New! [Pro Feature] WooCommerce full support: Add option to show player on shop page (loop) and in the single product page automatically.
- New! [Pro Feature] Tool for Import/Create Bulk Playlist for any post, playlist and WooCommerce product. Simply select which audio file to use and it will automatically create new post draft in 1-click.
- New! [Pro Feature] Display track thumbnails beside each tracks in the playlist.
- New! Support for ACF - Advanced Custom Fields: Learn more https://sonaar.ticksy.com/article/17004/
- Tweak: Better responsive display. Prevent track titles to wrap. Add ... if track title is too long.
- Tweak: Show number of tracks in admin columns for enabled CPT
- Tweak: We now use flex grid CSS for the player controls
- Tweak: You can select any kind of playlist's post-type in the shortcode generator or Elementor widget.
- Tweak: Major Speed Improvement with Synthetic waveform type: We have replaced waveform's thousands of SVG nodes by Canvas HTML.
- Tweak: Replaced a tags for div on control buttons
- Tweak: Removed widget class in the iron-audioplayer div to prevent conflicts with certain themes
- Fix issue where playlisttitle flashes when loading page on mobile.
- Fix issue where typography color was not applied in some case. It was always white.
- Fix minor issue with image encoding on some website
- Fix minor CSS incoherence

= 2.2.4 =
- Add compatibility for PHP 8.0
- Update CMB2 to 2.8.0
- Fix color picker issue in settings
- Fix issue with player not playing when used in Elementor Popup
- Fix issue with AddFontStyle function to prevent conflict with Visual Composer
- Remove Next and Previous button when there is only 1 track in the playlist

= 2.2.3 =
- Fix widgets PHP error since Elementor 3.1.0

= 2.2.2 =
- Now the audio tracks launch much faster when clicking play, especialy on mobile.
- Fix issue with register_location archive page for Elementor Pro
- Fix URL Encode and Decode for artwork images to resolve issue with some URL Queries
- Pro! New option to exclude Continuous Audio Playback from specific URL

= 2.2.1 =
- Fix issue with No playlist selected warning when shortcode has not attributes
- Fix typography settings that was not working in some case
- Added option to play latest playlist post in the native Elementor button widget

= 2.2 =
- New! Our Elementor Player Widget has a new facelift! You can now upload tracks directly into Elementor Editor ! No need to add a playlist anymore!
- New! Feed attribute added in the shortcode. This open doors to dynamic contents. Example: Use [sonaar_audioplayer feed="https://domain.com/01.mp3 || https://domain.com/02.mp3" feed_title="Track Title 01 || Track Title 02"] without having to create playlists.
- New! [Pro Feature] Continuous Audio Playback with persistent player is finally here! Song will continue to play accross any of your pages.
- New! You can add specific cover image for each tracks within Elementor. 
- Better UI in the Shortcode generator
- Better UI in the Elementor Widget
- Added Patreon icon option
- Fix issue with Gutenburg block in some case. Now the block should appear in Gutenburg.
- Fix when no playlist is selected we use the most recent playlist post published.
- Fix issue with track being not stopped at the end when no-skip-track option was enabled
- Fix download file name when using the download icon. It now takes the full filename when downloading
- Fix issue with z-index with some themes
- Fix issue with some themes where there is a blinking timeline progress bar
- Code optimization


= 2.1 =
- New! Full Gutenburg Support. We have added a Gutenburg block called MP3 Player
- New! Add scrollbars within your tracklist ( Pro feature )
- Fix issue with soundwaves width that are cutted in certain cases
- Fix issue when using special character in the audio URL


= 2.0.2 =
- Fix issue with post update not working when no audio is set
- Prevent PHP error if no playlist selected in the default WP Widget
- Prevent PHP Warning after plugin update to 2.x

= 2.0.1 =
- Fix minor issue which prevent checkbox to display in the widget in some case
- Make the artwork clickable so it can start and pause the audio track

= 2.0 =
- New! You can upload audio tracks from any posts, custom posts, or pages! Enable media uploads for your post types in WP-Admin > MP3 Player > Settings. Before this update, you had to create a playlist, upload your tracks and then embed the playlist widget or shortcode on the posts you wanted to display the player. Now simply edit any post, upload tracks and add the player widget, all at once. See quick video here: https://d.pr/v/l2NKHa
- New! We now fully support long audio tracks and streaming files. The player used to crash on mobile or not display waveforms when using heavy mp3 files. We have added a new waveform skin called Synthetic Waveform in WP-Admin > MP3 Player > Settings
- New! Waveform display intanstanly without lag time. Make sure to select Synthetic Waveform in WP-Admin > MP3 Player > Settings.
- New! SEO improvement: You can now control the playlist and soundwave title HTML tag and specifiy which heading to use. 
- Improved: MP3 is not being preloaded anymore if you are not displaying the waveform
- Improved: Condition logics for the setting panel
- Improved: Added query strings for the CSS and JS files to avoid cache issues when updating
- Improved: Minor UI work on the settings page
- Fix: When no playlist/album is specified in the shortcode, try to play the current post tracklist
- Fix: Condition JS library for better Gutenburg support
- Fix: Issue with font loading in some case
- Fix: Color pickers now display correctly
- Fix: No more 404 page not found on the Playlist custom posts.
- Fix: Featured Album was disable on some theme that don't support featured image
- Fix: Issue with UTF8 character encoding in sticky player
- Fix: Issue with sticky typography color not applied on the sticky player

= 1.5 =
- Fix for Elementor 2.9
- New! Option to add artist name in the tracklist. You can enable this option in WP-Admin > MP3 Player > Settings
- New! Option to choose progress bar color on mobile
- New! Option to open call-to-action links in _blank or _self target window. Default is _blank

= 1.4.2 =
- Fixed playlist limited to only 5 albums for some people
- Fixed hours duration label if song is longer than 1 hour
- Fixed PHP notice in Settings panel when font-size was selected without a color
- Align vertical play button

= 1.4.1 =
- Fixed waveform not showing up in some case
- Refactored the Ask for a Rating notice panel. It was too much annoying

= 1.4 =
- Fixed waveform not showing up when using more than 1 player on the same page
- Fixed PHP warning when no stream file is selected
- Remove artist fields in the backend

= 1.3 =
- Added Single Page Template for each albums
- Added Elementor Pro Support for the single template
- Added fade-in animation when waveform load for smoother appearance
- Fixed issue with the waveform not showing up when using external server files
- Fixed click area issue on the store-list icons beside each track
- Optimized code for better performance including enqueuing the plugin files only if player is present
- Add partial support for Playlist Categories


= 1.2 =
- Now fully compatible with WP 5.2.x
- Now fully compatible with PHP 7.3
- Added Elementor Page Builder Compatibility. You can now embed Audio Players using Elementor (no shortcode required). The Style tab of Elementor is available in the MP3 Player PRO version only.
- Added option to disable continuous playing through your tracks within a playlist
- Added option to change the Available Now text through shortcode
- Added option to remove / hide the album cover through shortcode and the shortcode generator
- Fixed Store Buttons showing even if no store icon was set
- Renamed Album Release Date to Album Subtitle for better usage
- Renamed menu Playlist for MP3 Player
- Removed WPBakery class (vc_col-md6)
- Refactored the player HTML using CSS flex-grid for better and smoother responsive
- Minor change to the Admin UI
- Refactored MP3 Player PRO ! It's showtime !

= 1.1.1 =
- Fixed minor issue with CMB2 throwing warning notices

= 1.1 =
- Fixed the multiple players in one page that play the same songs even if they are different players.
- Fixed Some Google Fonts were not loading. Now they load correctly.
- Added shortcodes directly in the Playlist listing so its easier to grab the shortcode.

= 1.0 =
* Initial Release

<?php
/**
 * Radio Block Class
 *
 * @since 1.6.0
 * @todo  - Add Block
 */

defined( 'ABSPATH' ) || exit;

/**
 * Sonaar Block Class
 */

class Sonaar_Block {
	/**
	 * Contruction
	 */
	public function __construct() {
		$this->version = SRMP3_VERSION;
		add_action( 'init', array( $this, 'sonaar_block_editor_style_script' ) );

        add_action( 'enqueue_block_editor_assets', array($this, 'sonaar_block_editor_scripts') );
	}
    
    function sonaar_block_editor_scripts() {
		$sonaar_mp3player = 'sonaar-music-mp3player';

        // Register Script for elementor
		// other scripts
		wp_register_script( 'sonaar-music', plugin_dir_url( dirname( __FILE__ ) ) . 'public/js/sonaar-music-public.js', array( 'jquery' ), $this->version, true );		
		wp_register_script( 'moments', plugin_dir_url( dirname( __FILE__ ) ) . 'public/js/iron-audioplayer/00.moments.min.js', array(), $this->version, true );
		wp_register_script( 'wave', plugin_dir_url( dirname( __FILE__ ) ) . 'public/js/iron-audioplayer/00.wavesurfer.min.js', array(), $this->version, true );
		wp_register_script( 'sonaar-music-mp3player', plugin_dir_url( dirname( __FILE__ ) ) . 'public/js/iron-audioplayer/iron-audioplayer.js', array( 'jquery', 'sonaar-music' ,'moments', 'wave', 'd3' ), $this->version, true );

		wp_enqueue_script('d3', '//cdn.jsdelivr.net/npm/d3@5/dist/d3.min.js', array(), NULL, true);
		/* Enqueue Sonnar Music mp3player js file on single Album Page */
		if ( is_single() && get_post_type() == SR_PLAYLIST_CPT ) {
			wp_enqueue_script( 'sonaar-music-mp3player' );			
		}

		wp_localize_script( 'sonaar-music-mp3player', 'sonaar_music', array(
			'plugin_dir_url'=> plugin_dir_url( dirname( __FILE__ ) ),
			'option' => Sonaar_Music::get_option( 'allOptions' )
		));

		/* Enqueue Sonaar Music related CSS and Js file */
		wp_enqueue_style( 'sonaar-music' );
		wp_enqueue_script( 'sonaar-music-mp3player' );

		if ( function_exists( 'run_sonaar_music_pro' ) ){
			$sonaar_music_pro = new Sonaar_Music_Pro_Public( 'sonaar-music-pro', '2.0.2' );
			$sonaar_music_pro->enqueue_styles();
	    	$sonaar_music_pro->enqueue_scripts();

			
			wp_enqueue_style( 'sonaar-music-pro' );
			wp_enqueue_script( 'sonaar-music-pro-mp3player' );
			wp_enqueue_script( 'sonaar_player' );

			$sonaar_mp3player = 'sonaar-music-pro-mp3player';
	   }

		if ( function_exists('sonaar_player') ) {
			add_action('wp_footer','sonaar_player', 12);
        }
        
		wp_enqueue_style( 'select2-css', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css' );
		wp_enqueue_script( 'select2-js', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js','1.0.0', true );
        
        wp_enqueue_script(
            'sonaar-block-js',
            plugin_dir_url( dirname( __FILE__ ) ) . 'admin/js/sonaar-block.js',
            array(
                $sonaar_mp3player,
                )
        );
        
    }

	/**
	 * Regester Block Scripts
	 *
	 * @return void
	 */
	function sonaar_block_editor_style_script() {

		$sonaar_mp3player = 'sonaar-music-mp3player';

		if ( function_exists( 'run_sonaar_music_pro' ) ){
			$sonaar_mp3player = 'sonaar-music-pro-mp3player';
	   }

		// Register required js and css files
		wp_register_style( 'sonaar-music', plugin_dir_url( dirname( __FILE__ ) ) . 'public/css/sonaar-music-public.css', array(), $this->version, 'all' );
	
		// Register the tb1 block
		wp_register_script( 'sonaar-block-script', plugin_dir_url( dirname( __FILE__ ) ) . 'build/index.js', array( 'jquery', $sonaar_mp3player,'wp-blocks', 'wp-i18n', 'wp-element', 'wp-components', 'wp-data', 'wp-editor'), $this->version );
		
	    
		if ( function_exists( 'register_block_type' ) ) {
			register_block_type(
				'sonaar/sonaar-block',
				array(
					'attributes'    => $this->sr_plugin_block_attribute(),
					'editor_script' => 'sonaar-block-script',
					'editor_style'  => 'sonaar-block-editor-style',
                    'style'         => 'sonaar-block-frontend-style',
                    'render_callback' => array($this, 'render_sonaar_block'),
				)
			);
		}
    }
    
    function render_sonaar_block( $attributes ) {

		ob_start();
        $album_id = ( isset( $attributes['album_id'] ) && $attributes['album_id'] ) ? $attributes['album_id'] : '';
        $playlist_show_album_market = ( isset( $attributes['playlist_show_album_market'] ) && $attributes['playlist_show_album_market'] ) ? true : false;
		$sr_player_on_artwork = ( isset( $attributes['sr_player_on_artwork'] ) && $attributes['sr_player_on_artwork'] ) ? true : false;
        $playlist_hide_artwork      = ( isset( $attributes['playlist_hide_artwork'] ) && $attributes['playlist_hide_artwork'] ) ? true : false;
        $playlist_show_playlist     = ( isset( $attributes['playlist_show_playlist'] ) && $attributes['playlist_show_playlist'] ) ? true : false;
        $playlist_show_soundwave    = ( isset( $attributes['playlist_show_soundwave'] ) && $attributes['playlist_show_soundwave'] ) ? true : false;
		$play_current_id            = ( isset( $attributes['play_current_id'] ) && $attributes['play_current_id'] ) ? true : false;
		
		
		$sticky_player  = false;
		$shuffle = false;
		$scrollbar = false;
		$scrollbar_height = '200px';

		$move_playlist_below_artwork = false;
		$track_artwork_show = false;
		$track_artwork_size = 45;
		$title_btshow = false;
		$subtitle_btshow = false;
		$title_html_tag_playlist = 'h3';
		$title_color = '';
		$subtitle_color = '';
		$track_title_color = '';
		$tracklist_hover_color = '';
		$tracklist_active_color = '';
		$track_separator_color = '';
		$tracklist_spacing = 8;
		$duration_color = '';
		$title_align = 'flex-start';
		$title_indent = 0;
		$title_fontsize = 0;
		$subtitle_fontsize = 0;
		$track_title_fontsize = 0;
		$duration_fontsize = 0;
		$store_title_fontsize = 0;
		$store_button_fontsize = 0;
		$duration_soundwave_fontsize = 0;
		$title_soundwave_fontsize = 0;
		$hide_number_btshow = false;
		$hide_time_duration = false;
		$play_pause_bt_show = false;
		$tracklist_controls_color = '';
		$tracklist_controls_size = 12;
		$hide_track_market = false;
		$wc_bt_show = true;
		$wc_icons_color = '';
		$wc_icons_bg_color = '';
		$view_icons_alltime = true;
		$popover_icons_store = '';
		$tracklist_icons_color = '';
		$tracklist_icons_spacing = '';
		$tracklist_icons_size = '';
		$title_soundwave_show = false;
		$title_html_tag_soundwave = 'div';
		$title_soundwave_color = '';
		$soundwave_show = false;
		$soundWave_progress_bar_color = '';
		$soundWave_bg_bar_color = '';
		$progressbar_inline = false;
		$duration_soundwave_show = false;
		$duration_soundwave_color = '';
		$audio_player_controls_spacebefore = 0;
		$artwork_width = 300;
		$artwork_padding = 0;
		$artwork_radius = 0;
		$audio_player_artwork_controls_color = '';
		$audio_player_artwork_controls_scale = 1;
		$audio_player_controls_color = '';
		$playlist_justify = 'center';
		$artwork_align = 'center';
		$playlist_width = 100;
		$playlist_margin = 0;
		$tracklist_margin = 0;
		$store_title_btshow = false;
		$store_title_text = esc_html__('Available now on:', 'sonaar-music');
		$store_title_color = '';
		$store_title_align = 'center';
		$album_stores_align = 'center';
		$button_text_color = '';
		$background_color = '';
		$button_hover_color = '';
		$button_background_hover_color = '';
		$button_hover_border_color = '';
		$button_border_style = 'none';
		$button_border_width = 3;
		$button_border_color = '#000000';
		$button_border_radius = 0;
		$store_icon_show = false;
		$icon_font_size = 0;
		$icon_indent = 10;
		$album_stores_padding = 22;

		if ( function_exists( 'run_sonaar_music_pro' ) ){
			$sticky_player  = ( isset( $attributes['enable_sticky_player'] ) && $attributes['enable_sticky_player'] ) ? true : false;
			$shuffle = ( isset( $attributes['enable_shuffle'] ) && $attributes['enable_shuffle'] ) ? true : false;
			$scrollbar = ( isset( $attributes['enable_scrollbar'] ) && $attributes['enable_scrollbar'] ) ? true : false;
			$scrollbar_height = ( isset( $attributes['scrollbar_height'] ) && $attributes['scrollbar_height'] ) ? $attributes['scrollbar_height'] .'px' : '200px';

			$move_playlist_below_artwork = ( isset( $attributes['move_playlist_below_artwork'] ) && $attributes['move_playlist_below_artwork'] ) ? true : false;
			$track_artwork_show = ( isset( $attributes['track_artwork_show'] ) && $attributes['track_artwork_show'] ) ? true : false;
			$track_artwork_size = ( isset( $attributes['track_artwork_size'] ) && $attributes['track_artwork_size'] ) ? $attributes['track_artwork_size'] : 45;
			$title_btshow = ( isset( $attributes['title_btshow'] ) && $attributes['title_btshow'] ) ? true : false;
			$title_html_tag_playlist = ( isset( $attributes['title_html_tag_playlist'] ) && $attributes['title_html_tag_playlist'] ) ? $attributes['title_html_tag_playlist'] : 'h3';
			$title_color = ( isset( $attributes['title_color'] ) && $attributes['title_color'] ) ? $attributes['title_color'] : '';
			$subtitle_color = ( isset( $attributes['subtitle_color'] ) && $attributes['subtitle_color'] ) ? $attributes['subtitle_color'] : '';
			$track_title_color = ( isset( $attributes['track_title_color'] ) && $attributes['track_title_color'] ) ? $attributes['track_title_color'] : '';
			$tracklist_hover_color = ( isset( $attributes['tracklist_hover_color'] ) && $attributes['tracklist_hover_color'] ) ? $attributes['tracklist_hover_color'] : '';
			$tracklist_active_color = ( isset( $attributes['tracklist_active_color'] ) && $attributes['tracklist_active_color'] ) ? $attributes['tracklist_active_color'] : '';
			$track_separator_color = ( isset( $attributes['track_separator_color'] ) && $attributes['track_separator_color'] ) ? $attributes['track_separator_color'] : '';
			$tracklist_spacing = ( isset( $attributes['tracklist_spacing'] ) && $attributes['tracklist_spacing'] ) ? $attributes['tracklist_spacing'] : 8;
			$duration_color = ( isset( $attributes['duration_color'] ) && $attributes['duration_color'] ) ? $attributes['duration_color'] : '';
			$title_align = ( isset( $attributes['title_align'] ) && $attributes['title_align'] ) ? $attributes['title_align'] : 'flex-start';
			$title_indent = ( isset( $attributes['title_indent'] ) && $attributes['title_indent'] ) ? $attributes['title_indent'] : 0;
			$title_fontsize = ( isset( $attributes['title_fontsize'] ) && $attributes['title_fontsize'] ) ? $attributes['title_fontsize'] : 0;
			$subtitle_fontsize = ( isset( $attributes['subtitle_fontsize'] ) && $attributes['subtitle_fontsize'] ) ? $attributes['subtitle_fontsize'] : 0;
			$track_title_fontsize = ( isset( $attributes['track_title_fontsize'] ) && $attributes['track_title_fontsize'] ) ? $attributes['track_title_fontsize'] : 0;
			$duration_fontsize = ( isset( $attributes['duration_fontsize'] ) && $attributes['duration_fontsize'] ) ? $attributes['duration_fontsize'] : 0;
			$store_title_fontsize = ( isset( $attributes['store_title_fontsize'] ) && $attributes['store_title_fontsize'] ) ? $attributes['store_title_fontsize'] : 0;
			$store_button_fontsize = ( isset( $attributes['store_button_fontsize'] ) && $attributes['store_button_fontsize'] ) ? $attributes['store_button_fontsize'] : 0;
			$duration_soundwave_fontsize = ( isset( $attributes['duration_soundwave_fontsize'] ) && $attributes['duration_soundwave_fontsize'] ) ? $attributes['duration_soundwave_fontsize'] : 0;
			$title_soundwave_fontsize = ( isset( $attributes['title_soundwave_fontsize'] ) && $attributes['title_soundwave_fontsize'] ) ? $attributes['title_soundwave_fontsize'] : 0;
			$subtitle_btshow = ( isset( $attributes['subtitle_btshow'] ) && $attributes['subtitle_btshow'] ) ? true : false;
			$hide_number_btshow = ( isset( $attributes['hide_number_btshow'] ) && $attributes['hide_number_btshow'] ) ? true : false;
			$hide_time_duration = ( isset( $attributes['hide_time_duration'] ) && $attributes['hide_time_duration'] ) ? true : false;
			$play_pause_bt_show = ( isset( $attributes['play_pause_bt_show'] ) && $attributes['play_pause_bt_show'] ) ? true : false;
			$tracklist_controls_color = ( isset( $attributes['tracklist_controls_color'] ) && $attributes['tracklist_controls_color'] ) ? $attributes['tracklist_controls_color'] : '';
			$tracklist_controls_size = ( isset( $attributes['tracklist_controls_size'] ) && $attributes['tracklist_controls_size'] ) ? $attributes['tracklist_controls_size'] : 12;
			$hide_track_market = ( isset( $attributes['hide_track_market'] ) && $attributes['hide_track_market'] ) ? true : false;
			$view_icons_alltime = ( isset( $attributes['view_icons_alltime'] ) && $attributes['view_icons_alltime'] ) ? true : false;
			$popover_icons_store = ( isset( $attributes['popover_icons_store'] ) && $attributes['popover_icons_store'] ) ? $attributes['popover_icons_store'] : '';
			$tracklist_icons_color = ( isset( $attributes['tracklist_icons_color'] ) && $attributes['tracklist_icons_color'] ) ? $attributes['tracklist_icons_color'] : '';
			$tracklist_icons_spacing = ( isset( $attributes['tracklist_icons_spacing'] ) && $attributes['tracklist_icons_spacing'] ) ? $attributes['tracklist_icons_spacing'] : 0;
			$tracklist_icons_size = ( isset( $attributes['tracklist_icons_size'] ) && $attributes['tracklist_icons_size'] ) ? $attributes['tracklist_icons_size'] : 0;
			$title_soundwave_show = ( isset( $attributes['title_soundwave_show'] ) && $attributes['title_soundwave_show'] ) ? true : false;
			$title_html_tag_soundwave = ( isset( $attributes['title_html_tag_soundwave'] ) && $attributes['title_html_tag_soundwave'] ) ? $attributes['title_html_tag_soundwave'] : 'div';
			$title_soundwave_color = ( isset( $attributes['title_soundwave_color'] ) && $attributes['title_soundwave_color'] ) ? $attributes['title_soundwave_color'] : '';
			$soundwave_show = ( isset( $attributes['soundwave_show'] ) && $attributes['soundwave_show'] ) ? true : false;
			$soundWave_progress_bar_color = ( isset( $attributes['soundWave_progress_bar_color'] ) && $attributes['soundWave_progress_bar_color'] ) ? $attributes['soundWave_progress_bar_color'] : '';
			$soundWave_bg_bar_color = ( isset( $attributes['soundWave_bg_bar_color'] ) && $attributes['soundWave_bg_bar_color'] ) ? $attributes['soundWave_bg_bar_color'] : '';
			$progressbar_inline = ( isset( $attributes['progressbar_inline'] ) && $attributes['progressbar_inline'] ) ? true : false;
			$duration_soundwave_show = ( isset( $attributes['duration_soundwave_show'] ) && $attributes['duration_soundwave_show'] ) ? true : false;
			$duration_soundwave_color = ( isset( $attributes['duration_soundwave_color'] ) && $attributes['duration_soundwave_color'] ) ? $attributes['duration_soundwave_color'] : '';
			$audio_player_controls_spacebefore = ( isset( $attributes['audio_player_controls_spacebefore'] ) && $attributes['audio_player_controls_spacebefore'] ) ? $attributes['audio_player_controls_spacebefore'] : 0;
			$artwork_width = ( isset( $attributes['artwork_width'] ) && $attributes['artwork_width'] ) ? $attributes['artwork_width'] : 300;
			$artwork_padding = ( isset( $attributes['artwork_padding'] ) && $attributes['artwork_padding'] ) ? $attributes['artwork_padding'] : 0;
			$artwork_radius = ( isset( $attributes['artwork_radius'] ) && $attributes['artwork_radius'] ) ? $attributes['artwork_radius'] : 0;
			$audio_player_artwork_controls_color = ( isset( $attributes['audio_player_artwork_controls_color'] ) && $attributes['audio_player_artwork_controls_color'] ) ? $attributes['audio_player_artwork_controls_color'] : '';
			$audio_player_artwork_controls_scale = ( isset( $attributes['audio_player_artwork_controls_scale'] ) && $attributes['audio_player_artwork_controls_scale'] ) ? $attributes['audio_player_artwork_controls_scale'] : 1;
			$audio_player_controls_color = ( isset( $attributes['audio_player_controls_color'] ) && $attributes['audio_player_controls_color'] ) ? $attributes['audio_player_controls_color'] : '';
			$playlist_justify = ( isset( $attributes['playlist_justify'] ) && $attributes['playlist_justify'] ) ? $attributes['playlist_justify'] : 'center';
			$artwork_align = ( isset( $attributes['artwork_align'] ) && $attributes['artwork_align'] ) ? $attributes['artwork_align'] : 'center';
			$playlist_width = ( isset( $attributes['playlist_width'] ) && $attributes['playlist_width'] ) ? $attributes['playlist_width'] : 100;
			$playlist_margin = ( isset( $attributes['playlist_margin'] ) && $attributes['playlist_margin'] ) ? $attributes['playlist_margin'] : 0;
			$tracklist_margin = ( isset( $attributes['tracklist_margin'] ) && $attributes['tracklist_margin'] ) ? $attributes['tracklist_margin'] : 0;
			$store_title_btshow = ( isset( $attributes['store_title_btshow'] ) && $attributes['store_title_btshow'] ) ? true : false;
			$store_title_text = ( isset( $attributes['store_title_text'] ) && $attributes['store_title_text'] ) ? $attributes['store_title_text'] : esc_html__('Available now on:', 'sonaar-music');
			$store_title_color = ( isset( $attributes['store_title_color'] ) && $attributes['store_title_color'] ) ? $attributes['store_title_color'] : '';
			$store_title_align = ( isset( $attributes['store_title_align'] ) && $attributes['store_title_align'] ) ? $attributes['store_title_align'] : 'center';
			$album_stores_align = ( isset( $attributes['album_stores_align'] ) && $attributes['album_stores_align'] ) ? $attributes['album_stores_align'] : 'center';
			$button_text_color = ( isset( $attributes['button_text_color'] ) && $attributes['button_text_color'] ) ? $attributes['button_text_color'] : '';
			$background_color = ( isset( $attributes['background_color'] ) && $attributes['background_color'] ) ? $attributes['background_color'] : '';
			$button_hover_color = ( isset( $attributes['button_hover_color'] ) && $attributes['button_hover_color'] ) ? $attributes['button_hover_color'] : '';
			$button_background_hover_color = ( isset( $attributes['button_background_hover_color'] ) && $attributes['button_background_hover_color'] ) ? $attributes['button_background_hover_color'] : '';
			$button_hover_border_color = ( isset( $attributes['button_hover_border_color'] ) && $attributes['button_hover_border_color'] ) ? $attributes['button_hover_border_color'] : '';
			$button_border_style = ( isset( $attributes['button_border_style'] ) && $attributes['button_border_style'] ) ? $attributes['button_border_style'] : '';
			$button_border_width = ( isset( $attributes['button_border_width'] ) && $attributes['button_border_width'] ) ? $attributes['button_border_width'] : 3;
			$button_border_color = ( isset( $attributes['button_border_color'] ) && $attributes['button_border_color'] ) ? $attributes['button_border_color'] : '#000000';
			$button_border_radius = ( isset( $attributes['button_border_radius'] ) && $attributes['button_border_radius'] ) ? $attributes['button_border_radius'] : 0;
			$store_icon_show = ( isset( $attributes['store_icon_show'] ) && $attributes['store_icon_show'] ) ? true : false;
			$icon_font_size = ( isset( $attributes['icon_font_size'] ) && $attributes['icon_font_size'] ) ? $attributes['icon_font_size'] : 0;
			$icon_indent = ( isset( $attributes['icon_indent'] ) && $attributes['icon_indent'] ) ? $attributes['icon_indent'] : 10;
			$album_stores_padding = ( isset( $attributes['album_stores_padding'] ) && $attributes['album_stores_padding'] ) ? $attributes['album_stores_padding'] : 22;
		}
		
		$classes = ''; 

		if( function_exists( 'run_sonaar_music_pro' ) ) { 

			if( $move_playlist_below_artwork ) {
				$classes .= ' sr_playlist_below_artwork_auto'; 
			}
			if( $title_btshow ) {
				$classes .= ' sr_player_title_hide'; 
			}
			if( $subtitle_btshow ) {
				$classes .= ' sr_player_subtitle_hide'; 
			}
			if( $hide_number_btshow ) {
				$classes .= ' sr_player_track_num_hide'; 
			}
			if( $hide_time_duration ) {
				$classes .= ' sr_player_time_hide';
			}
			if( $play_pause_bt_show ) {
				$classes .= ' sr_play_pause_bt_hide';
			}

			if( $view_icons_alltime ) {
				$classes .= ' sr_track_inline_cta_bt__yes';
			}
		}
		
		if( function_exists( 'run_sonaar_music_pro' ) ) {			
			$wave_color = ( $soundWave_bg_bar_color != ''  ) ? $soundWave_bg_bar_color : false;
			$wave_progress_color = ( $soundWave_progress_bar_color != ''  ) ? $soundWave_progress_bar_color : false;
		} else {			
			$wave_color = false;
			$wave_progress_color = false;
		}

		$show_track_market = ( $hide_track_market ) ? false : true;
		$show_track_market = ( function_exists( 'run_sonaar_music_pro' ) ) ? $show_track_market : true;

		
		$shortcode = '<div class="sonaar_audioplayer_block_cover'. $classes .'">';
		$shortcode .= '[sonaar_audioplayer titletag_soundwave="'. $title_html_tag_soundwave .'" titletag_playlist="'. $title_html_tag_playlist .'" hide_artwork="' . $playlist_hide_artwork .'" show_playlist="' . $playlist_show_playlist .'" show_album_market="' . $playlist_show_album_market .'" hide_timeline="' . $playlist_show_soundwave .'" sticky_player="' . $sticky_player .'" wave_color="'. $wave_color .'" wave_progress_color="'. $wave_progress_color .'" shuffle="' . $shuffle .'" show_track_market="'. $show_track_market .'" ';
		
		if( $scrollbar && function_exists( 'run_sonaar_music_pro' ) ) {
			$shortcode .= 'scrollbar="true" ';
		}

		if( $sr_player_on_artwork ) {
			$shortcode .= 'display_control_artwork="true" ';
		}

		if( $track_artwork_show && function_exists( 'run_sonaar_music_pro' ) ) {
			$shortcode .= 'track_artwork="true" ';
		}

		if( ! $playlist_show_soundwave && $title_soundwave_show && function_exists( 'run_sonaar_music_pro' ) ) {
			$shortcode .= 'hide_track_title="true" ';
		}

		if( $soundwave_show && function_exists( 'run_sonaar_music_pro' ) ) {
			$shortcode .= 'hide_progressbar="true" ';
		}

		if( ! $soundwave_show && $progressbar_inline && function_exists( 'run_sonaar_music_pro' ) ) {
			$shortcode .= 'progressbar_inline="true" ';
		}
		if( ! $soundwave_show && $duration_soundwave_show && function_exists( 'run_sonaar_music_pro' ) ) {
			$shortcode .= 'hide_times="true" ';
		}

		if( ! $store_title_btshow && $store_title_text && function_exists( 'run_sonaar_music_pro' ) ) {
			$shortcode .= 'store_title_text="'. $store_title_text .'" ';
		}
		
		
		if ( $play_current_id  ){ //If "Play its own Post ID track" option is enable
			$postid = get_the_ID();
			$shortcode .= 'albums="' . $postid . '" ';
		} else {
			$display_playlist_ar = $album_id;

			if(is_array($display_playlist_ar)){
                $display_playlist_ar = implode(", ", $display_playlist_ar);
			}

			// WIP
			if (!$display_playlist_ar) { //If no playlist is selected, play the latest playlist
				$shortcode .= 'play-latest="yes" ';
			}else{
                $shortcode .= 'albums="' . $display_playlist_ar . '" ';
			}
        }
        
		$shortcode .= ']';
		$shortcode .= '</div>';

		$renadom_number = rand(10,100);
		$block_id = 'sonaar_music_' . $renadom_number;

		if( $scrollbar && function_exists( 'run_sonaar_music_pro' ) ) {
			$scrollbar_css = " #$block_id .iron-audioplayer .playlist ul {
				height: $scrollbar_height;
				overflow-y: hidden;
				overflow-x: hidden;
			} ";

			if( is_admin() ) {
			} else {
				wp_add_inline_style( 'sonaar-music-pro', $scrollbar_css );
			}

			echo '<style>';
			echo $scrollbar_css;
			echo '</style>';
			
		}
		$custom_css = '';

		if( function_exists( 'run_sonaar_music_pro' ) ) { 


			if( ! $playlist_hide_artwork ) {
				$custom_css .= '#'.$block_id .' .album .album-art { max-width: '.$artwork_width.'px; width: '.$artwork_width.'px;}';
				$custom_css .= ' #'.$block_id .' .album .album-art img { border-radius: '.$artwork_radius.'px;}';
				$custom_css .= ' #'.$block_id .' .sonaar-grid .album { padding: '.$artwork_padding.'px;}';
			}
			
			$custom_css .= ' #'.$block_id .' .playlist li .sr_track_cover { width: '.$track_artwork_size.'px;}';
			$custom_css .= ' #'.$block_id .' .sonaar-grid { justify-content: '.$playlist_justify.'; }';
			$custom_css .= ' #'.$block_id .' .sr_playlist_below_artwork_auto .sonaar-grid { align-items: '.$playlist_justify.'; }';
			$custom_css .= ' #'.$block_id .' .playlist, #'.$block_id .' .buttons-block { width: '.$playlist_width.'%; }';
			$custom_css .= ' #'.$block_id .' .playlist { margin: '.$playlist_margin.'px; }';
			$custom_css .= ' #'.$block_id .' .playlist ul { margin: '.$tracklist_margin.'px; }';
			$custom_css .= ' #'.$block_id .' .sr_it-playlist-title, #'.$block_id .' .sr_it-playlist-artists, #'.$block_id .' .sr_it-playlist-release-date { text-align: '.$title_align.' !important; }';
			$custom_css .= ' #'.$block_id .' .sr_it-playlist-title, #'.$block_id .' .sr_it-playlist-artists, #'.$block_id .' .sr_it-playlist-release-date { margin-left: '.$title_indent.'px; }';
			$custom_css .= ' #'.$block_id .' .playlist li { padding-top: '.$tracklist_spacing.'px; padding-bottom: '.$tracklist_spacing.'px; }';
			$custom_css .= ' #'.$block_id .' .track-number svg { width: '.$tracklist_controls_size.'px; height: '.$tracklist_controls_size.'px; }';
			$custom_css .= ' #'.$block_id .' .track-number { padding-left: calc( '.$tracklist_controls_size.'px + 12px ); }';
			$custom_css .= ' #'.$block_id .' .ctnButton-block { justify-content: '.$store_title_align.'; align-items: '.$store_title_align.'; }';
			$custom_css .= ' #'.$block_id .' .buttons-block { justify-content: '.$album_stores_align.'; align-items: '.$album_stores_align.'; }';
			$custom_css .= ' #'.$block_id .' .buttons-block .store-list li .button { border-style: '.$button_border_style.'; }';		
			$custom_css .= ' #'.$block_id .' .show-playlist .ctnButton-block { margin: '.$album_stores_padding.'px; }';

			if( $show_track_market ) {
				if( ! $wc_bt_show ) {
					$custom_css .= ' #'.$block_id .' .playlist a.song-store.sr_store_wc_round_bt { display: none; }';
				}
				if( $wc_bt_show  && $wc_icons_color != '' ) {
					$custom_css .= ' #'.$block_id .' .playlist a.song-store.sr_store_wc_round_bt { color: '.$wc_icons_color.'; }';	
				}
				if( $wc_bt_show  && $wc_icons_bg_color != '' ) {
					$custom_css .= ' #'.$block_id .' .playlist a.song-store.sr_store_wc_round_bt { background-color: '.$wc_icons_color.'; }';	
				}
			}

			if( ! $progressbar_inline ) {
				$custom_css .= ' #'.$block_id .' .player .control { top: '.$audio_player_controls_spacebefore.'px; position: relative; }';
			}

			if( $sr_player_on_artwork && $audio_player_artwork_controls_color != ''  ) {
				$custom_css .= ' #'.$block_id .' .sr_player_on_artwork .control path, #'.$block_id .' .sr_player_on_artwork .control rect, #'.$block_id .' .sr_player_on_artwork .control polygon { fill: '.$audio_player_artwork_controls_color.'; }';
				$custom_css .= ' #'.$block_id .' .sr_player_on_artwork .control .play { border-color: '.$audio_player_artwork_controls_color.'; }';
			}
			if( $sr_player_on_artwork  ) {
				$custom_css .= ' #'.$block_id .' .sr_player_on_artwork .control { transform:scale('. $audio_player_artwork_controls_scale .'); }';
			}
			

			if( $button_border_style != 'none' ) {
				$custom_css .= ' #'.$block_id .' .buttons-block .store-list li .button { border-width: '.$button_border_width.'px; }';
				$custom_css .= ' #'.$block_id .' .buttons-block .store-list li .button { border-color: '.$button_border_color.'; }';
				$custom_css .= ' #'.$block_id .' .store-list .button { border-radius: '.$button_border_radius.'px; }';
			}
			
			if( $sr_player_on_artwork && ! $playlist_hide_artwork && $playlist_show_playlist && $move_playlist_below_artwork  ) {
				$custom_css .= ' #'.$block_id .' .sonaar-Artwort-box { justify-content: '.$artwork_align.'; }';
			}

			if( $title_color != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist .sr_it-playlist-title { color: '.$title_color.'; }';
			}

			if( $title_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' .playlist .sr_it-playlist-title { font-size: '.$title_fontsize.'px; }';
			}

			if( $subtitle_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' .sr_it-playlist-release-date { font-size: '.$subtitle_fontsize.'px; }';
			}

			if( $track_title_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' .playlist .audio-track, #'.$block_id .' .playlist .track-number, #'.$block_id .' .track-title { font-size: '.$track_title_fontsize.'px; }';
			}

			if( ! $hide_time_duration && $duration_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' .tracklist-item-time { font-size: '.$duration_fontsize.'px; }';
			}

			if( ! $store_title_btshow && $store_title_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' .available-now { font-size: '.$store_title_fontsize.'px; }';
			}

			if( $store_button_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' a.button { font-size: '.$store_button_fontsize.'px; }';
			}

			if( ! $soundwave_show && ! $duration_soundwave_show && $duration_soundwave_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' .player { font-size: '.$duration_soundwave_fontsize.'px; }';
			}

			if( ! $soundwave_show && $audio_player_controls_color != ''  ) {
				$custom_css .= ' #'.$block_id .' .player .control path, #'.$block_id .' .player .control rect, #'.$block_id .' .player .control polygon { fill: '.$audio_player_controls_color.'; }';
			}
			if( !$title_soundwave_show && $title_soundwave_fontsize > 0 ) {
				$custom_css .= ' #'.$block_id .' .album-player .track-title { font-size: '.$title_soundwave_fontsize.'px; }';
			}


			if( $subtitle_color != '' ) {
				$custom_css .= ' #'.$block_id .' .sr_it-playlist-release-date { color: '.$subtitle_color.'; }';
			}

			if( $track_title_color != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist .audio-track, #'.$block_id .' .playlist .track-number, #'.$block_id .' .track-title, #'.$block_id .' .player { color: '.$track_title_color.'; }';
			}

			if( $tracklist_hover_color != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist .audio-track:hover, #'.$block_id .' .playlist .audio-track:hover .track-number, #'.$block_id .' .playlist a.song-store:not(.sr_store_wc_round_bt):hover, #'.$block_id .' .playlist .current a.song-store:not(.sr_store_wc_round_bt):hover { color: '.$tracklist_hover_color.'; }';
				$custom_css .= ' #'.$block_id .' .playlist .audio-track:hover path, #'.$block_id .' .playlist .audio-track:hover rect {  fill: '.$tracklist_hover_color.'; }';
			}

			if( $tracklist_active_color != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist .current .audio-track, #'.$block_id .' .playlist .current .audio-track .track-number, #'.$block_id .' .playlist .current a.song-store { color: '.$tracklist_active_color.'; }';
				$custom_css .= ' #'.$block_id .' .playlist .current .audio-track path, #'.$block_id .' .playlist .current .audio-track rect {  fill: '.$tracklist_active_color.'; }';
			}

			if( $track_separator_color != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist li { border-bottom: solid 1px '.$track_separator_color.'; }';
			}

			if( $duration_color != '' ) {
				$custom_css .= ' #'.$block_id .' .tracklist-item-time { color: '.$duration_color.'; }';
			}

			if( $tracklist_controls_color != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist .audio-track path, #'.$block_id .' .playlist .audio-track rect { fill: '.$tracklist_controls_color.'; }';
			}

			if( $store_title_btshow )  {
				$custom_css .= ' #'.$block_id .' .available-now { display: none; }';
			}

			if( $store_title_color != '' ) {
				$custom_css .= ' #'.$block_id .' .available-now { color: '.$store_title_color.'; }';
			}
			
			if( $button_text_color != '' ) {
				$custom_css .= ' #'.$block_id .' a.button { color: '.$button_text_color.'; }';
			}
			if( $background_color != '' ) {
				$custom_css .= ' #'.$block_id .' a.button { background: '.$background_color.'; }';
			}
			if( $button_hover_color != '' ) {
				$custom_css .= ' #'.$block_id .' a.button:hover { color: '.$button_hover_color.'; }';
			}
			if( $button_background_hover_color != '' ) {
				$custom_css .= ' #'.$block_id .' a.button:hover { background: '.$button_background_hover_color.'; }';
			}
			if( $button_hover_border_color != '' && $button_border_style != 'none' ) {
				$custom_css .= ' #'.$block_id .' a.button:hover { border-color: '.$button_hover_border_color.' !important; }';
			}

			if( $store_icon_show ) {
				$custom_css .= ' #'.$block_id .' .store-list .button i { display: none; }';
			}
			if( $icon_font_size > 0 ) {
				$custom_css .= ' #'.$block_id .' .buttons-block .store-list i { font-size: '.$icon_font_size.'px; }';
				$custom_css .= ' #'.$block_id .' .buttons-block .store-list i { margin-right: '.$icon_indent.'px; }';
			}

			if( $title_soundwave_color != '' )  {
				$custom_css .= ' #'.$block_id .' .track-title, #'.$block_id .' .player { color: '.$title_soundwave_color.'; }';
			}

			if( ! $soundwave_show && $soundWave_progress_bar_color != '' )  {
				$custom_css .= ' #'.$block_id .' .sonaar_wave_cut rect { fill: '.$soundWave_progress_bar_color.'; }';
				$custom_css .= ' #'.$block_id .' .sr_waveform_simplebar .sonaar_wave_cut { background-color: '.$soundWave_progress_bar_color.'; }';
			}
			if( ! $soundwave_show && $soundWave_bg_bar_color != '' )  {
				$custom_css .= ' #'.$block_id .' .sonaar_wave_base rect { fill: '.$soundWave_bg_bar_color.'; }';
				$custom_css .= ' #'.$block_id .' .sr_waveform_simplebar .sonaar_wave_base { background-color: '.$soundWave_bg_bar_color.'; }';
			}

			if( ! $soundwave_show && ! $duration_soundwave_show && $duration_soundwave_color != '' ) {
				$custom_css .= ' #'.$block_id .' .player { color: '.$duration_soundwave_color.'; }';
			}

			if( ! $hide_track_market && ! $view_icons_alltime && $popover_icons_store != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist .song-store-list-menu .fa-ellipsis-v { color: '.$popover_icons_store.'; }';
			}
			if( ! $hide_track_market && $tracklist_icons_color != '' ) {
				$custom_css .= ' #'.$block_id .' .playlist a.song-store:not(.sr_store_wc_round_bt) { color: '.$tracklist_icons_color.'; }';
			}
			if( ! $hide_track_market && $tracklist_icons_spacing > 0 ) {
				$custom_css .= ' #'.$block_id .' .playlist .store-list .song-store-list-container { column-gap: '.$tracklist_icons_spacing.'px; }';
			}
			if( ! $hide_track_market && $tracklist_icons_size > 0 ) {
				$custom_css .= ' #'.$block_id .' .playlist .store-list .song-store .fab, #'.$block_id .' .playlist .store-list .song-store .fas { font-size: '.$tracklist_icons_size.'px; }';
			}
			

			echo '<style>';
			echo $custom_css;
			echo '</style>';
		}

		echo '<div id="'. $block_id .'">';
	    echo do_shortcode( $shortcode );
	    echo '</div>';

       return ob_get_clean();
    }
	
	private function sr_plugin_block_attribute() {
		$attributes_pro = array();

		$attributes = array(
			'run_pro' => array(
				'type'    => 'boolean',
				'default' => false,
			),
			'wc_enable' => array(
				'type'    => 'boolean',
				'default' => false,
			),
			'album_id' => array(
				'type' => 'array',
				'default' => [],
				'items'   => [
					'type' => 'integer',
				]
			),
			'playlist_list' => array(
				'type'    => 'array',
				'default' => $this->sr_plugin_block_select_playlist()
			),
			'enable_sticky_player' => array(
				'type'    => 'boolean',
				'default' => false,
			),
			'enable_shuffle' => array(
				'type'    => 'boolean',
				'default' => false,
			),
			'enable_scrollbar' => array(
				'type'    => 'boolean',
				'default' => false,
			),				
			'scrollbar_height' => array(
				'type'    => 'integer',
				'default' => 200,
			),
			'playlist_show_playlist' => array(
				'type'    => 'boolean',
				'default' => true,
			),
			'playlist_show_album_market' => array(
				'type'    => 'boolean',
				'default' => true,
			),
			'sr_player_on_artwork' => array(
				'type'    => 'boolean',
				'default' => false,
			),
			'playlist_hide_artwork' => array(
				'type'    => 'boolean',
				'default' => false,
			),
			'playlist_show_soundwave' => array(
				'type'    => 'boolean',
				'default' => false,
			),
			'play_current_id' => array(
				'type'    => 'boolean',
				'default' => false,
			),
		);

		
			
			$attributes['html_tags'] = array(
				'type'    => 'array',
				'default' => array(
					array(
						'label' => 'H1',
                    	'value' => 'h1',
					),
					array(
						'label' => 'H2',
                    	'value' => 'h2',
					),
					array(
						'label' => 'H3',
                    	'value' => 'h3',
					),
					array(
						'label' => 'H4',
                    	'value' => 'h4',
					),
					array(
						'label' => 'H5',
                    	'value' => 'h5',
					),
					array(
						'label' => 'H6',
                    	'value' => 'h6',
					),					
					array(
						'label' => 'div',
                    	'value' => 'div',
					),
					array(
						'label' => 'span',
                    	'value' => 'span',
					),
					array(
						'label' => 'p',
                    	'value' => 'p',
					),
				)
			);
			$attributes['sr_text_alignments'] = array(
                'type'    => 'array',
                'default' => array(
                    array(
                        'label' => esc_html__( 'Left', 'sonaar-music' ),
                        'value' => 'left',
                    ),
                    array(
                        'label' => esc_html__( 'Center', 'sonaar-music' ),
                        'value' => 'center',
                    ),
                    array(
                        'label' => esc_html__( 'Right', 'sonaar-music' ),
                        'value' => 'right',
                    ),
                )
			);
			$attributes['sr_alignments'] = array(
                'type'    => 'array',
                'default' => array(
                    array(
                        'label' => esc_html__( 'Left', 'sonaar-music' ),
                        'value' => 'flex-start',
                    ),
                    array(
                        'label' => esc_html__( 'Center', 'sonaar-music' ),
                        'value' => 'center',
                    ),
                    array(
                        'label' => esc_html__( 'Right', 'sonaar-music' ),
                        'value' => 'flex-end',
                    ),
                )
			);
			$attributes['colors'] = array(
				'type'    => 'array',
				'default' => array(
					array(
                        'name' => esc_html__( 'Black', 'sonaar-music' ),
                        'slug' => 'black',
                        'color' => '#000000'
                    ),
                    array(
                        'name' => esc_html__( 'White', 'sonaar-music' ),
                        'slug' => 'white',
                        'color' => '#ffffff'
                    ),
                    array(
                        'name' => esc_html__( 'Blue', 'sonaar-music' ),
                        'slug' => 'blue',
                        'color' => '#0073aa'
                    ),
				)
			);
			$attributes['border_types'] = array(
				'type'    => 'array',
				'default' => array(
					array(
                        'label' => esc_html__( 'None', 'sonaar-music' ),
                        'value' => 'none',
                    ),
                    array(
                        'label' => esc_html__( 'Solid', 'sonaar-music' ),
                        'value' => 'solid',
                    ),
                    array(
                        'label' => esc_html__( 'Double', 'sonaar-music' ),
                        'value' => 'double',
                    ),					
                    array(
                        'label' => esc_html__( 'Dotted', 'sonaar-music' ),
                        'value' => 'dotted',
                    ),
					array(
                        'label' => esc_html__( 'Dashed', 'sonaar-music' ),
                        'value' => 'dashed',
                    ),
					array(
                        'label' => esc_html__( 'Groove', 'sonaar-music' ),
                        'value' => 'groove',
                    ),
				)
			);

			$attributes['move_playlist_below_artwork'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['track_artwork_show'] = array(
				'type'    => 'boolean',
				'default' => false,
			);			
            $attributes['track_artwork_size'] = array(
                'type' => 'integer',
                'default' => 45,
			);
			$attributes['title_btshow'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['subtitle_btshow'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['title_html_tag_playlist'] = array(
				'type'    => 'string',
				'default' => 'h3',
			);
			$attributes['title_color'] = array(
				'type'    => 'string',
				'default' => '',
			);	
			$attributes['subtitle_color'] = array(
				'type'    => 'string',
				'default' => '',
			);			
			$attributes['track_title_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
			$attributes['tracklist_hover_color'] = array(
				'type'    => 'string',
				'default' => '',
			);	
			$attributes['tracklist_active_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
			$attributes['track_separator_color'] = array(
				'type'    => 'string',
				'default' => '',
			);					
            $attributes['tracklist_spacing'] = array(
                'type' => 'integer',
                'default' => 8,
			);			
			$attributes['duration_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
            $attributes['title_align'] = array(
                'type' => 'string',
                'default' => 'left',
			);
            $attributes['title_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['subtitle_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['track_title_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['duration_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['store_title_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);			
            $attributes['store_button_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);	
            $attributes['duration_soundwave_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);			
            $attributes['title_soundwave_fontsize'] = array(
                'type' => 'integer',
                'default' => 0,
			);			
            $attributes['title_indent'] = array(
                'type' => 'integer',
                'default' => 0,
			);
			$attributes['hide_number_btshow'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['hide_time_duration'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['play_pause_bt_show'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['tracklist_controls_color'] = array(
				'type'    => 'string',
				'default' => '',
			);	
            $attributes['tracklist_controls_size'] = array(
                'type' => 'integer',
                'default' => 12,
			);			
			$attributes['hide_track_market'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['view_icons_alltime'] = array(
				'type'    => 'boolean',
				'default' => true,
			);
			$attributes['popover_icons_store'] = array(
				'type'    => 'string',
				'default' => '',
			);
			$attributes['tracklist_icons_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
            $attributes['tracklist_icons_spacing'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['tracklist_icons_size'] = array(
                'type' => 'integer',
                'default' => 0,
			);
			$attributes['title_soundwave_show'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['title_html_tag_soundwave'] = array(
				'type'    => 'string',
				'default' => 'div',
			);
			$attributes['title_soundwave_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
			$attributes['soundwave_show'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['soundWave_progress_bar_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
			$attributes['soundWave_bg_bar_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
			$attributes['progressbar_inline'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['duration_soundwave_show'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['duration_soundwave_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
            $attributes['audio_player_controls_spacebefore'] = array(
                'type' => 'integer',
                'default' => 0,
			);			
            $attributes['artwork_width'] = array(
                'type' => 'integer',
                'default' => 300,
			);
            $attributes['artwork_padding'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['artwork_radius'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['audio_player_artwork_controls_color'] = array(
                'type' => 'string',
                'default' => '',
			);
            $attributes['audio_player_artwork_controls_scale'] = array(
                'type' => 'number',
                'default' => 1,
			);
			$attributes['audio_player_controls_color'] = array(
                'type' => 'string',
                'default' => '',
			);
            $attributes['playlist_justify'] = array(
                'type' => 'string',
                'default' => 'center',
			);
            $attributes['artwork_align'] = array(
                'type' => 'string',
                'default' => 'center',
			);
            $attributes['playlist_width'] = array(
                'type' => 'integer',
                'default' => 100,
			);
            $attributes['playlist_margin'] = array(
                'type' => 'integer',
                'default' => 0,
			);
            $attributes['tracklist_margin'] = array(
                'type' => 'integer',
                'default' => 0,
			);

			$attributes['store_title_btshow'] = array(
				'type'    => 'boolean',
				'default' => false,
			);			
            $attributes['store_title_text'] = array(
                'type' => 'string',
                'default' => esc_html__('Available now on:', 'sonaar-music'),
			);
			$attributes['store_title_color'] = array(
				'type'    => 'string',
				'default' => '',
			);			
			$attributes['store_title_align'] = array(
				'type'    => 'string',
				'default' => 'center',
			);						
			$attributes['album_stores_align'] = array(
				'type'    => 'string',
				'default' => 'center',
			);					
			$attributes['button_text_color'] = array(
				'type'    => 'string',
				'default' => '',
			);					
			$attributes['background_color'] = array(
				'type'    => 'string',
				'default' => '',
			);					
			$attributes['button_hover_color'] = array(
				'type'    => 'string',
				'default' => '',
			);					
			$attributes['button_background_hover_color'] = array(
				'type'    => 'string',
				'default' => '',
			);				
			$attributes['button_hover_border_color'] = array(
				'type'    => 'string',
				'default' => '',
			);							
			$attributes['button_border_style'] = array(
				'type'    => 'string',
				'default' => 'none',
			);
			$attributes['button_border_width'] = array(
				'type'    => 'integer',
				'default' => 3,
			);			
			$attributes['button_border_color'] = array(
				'type'    => 'string',
				'default' => 'black',
			);
			$attributes['button_border_radius'] = array(
				'type'    => 'integer',
				'default' => 0,
			);
			$attributes['store_icon_show'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['icon_font_size'] = array(
				'type'    => 'integer',
				'default' => 0,
			);
			$attributes['icon_indent'] = array(
				'type'    => 'integer',
				'default' => 10,
			);
			$attributes['album_stores_padding'] = array(
				'type'    => 'integer',
				'default' => 22,
			);

			$attributes['enable_sticky_player'] = array(
				'type'    => 'boolean',
				'default' => true,
			);
			$attributes['enable_shuffle'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['enable_scrollbar'] = array(
				'type'    => 'boolean',
				'default' => false,
			);
			$attributes['scrollbar_height'] = array(
				'type'    => 'integer',
				'default' => 200,
			);

			$attributes['wc_bt_show'] = array(
				'type'    => 'boolean',
				'default' => true,
			);
			$attributes['wc_icons_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
			$attributes['wc_icons_bg_color'] = array(
				'type'    => 'string',
				'default' => '',
			);
			

		if ( function_exists( 'run_sonaar_music_pro' ) ) {

			$attributes['run_pro'] = array(
				'type'    => 'boolean',
				'default' => true,
			);

			if( Sonaar_Music::get_option('wc_bt_type') != 'wc_bt_type_inactive' && ( defined( 'WC_VERSION' ) && get_site_option('SRMP3_ecommerce') == '1' ) ){

				$attributes['wc_enable'] = array(
					'type'    => 'boolean',
					'default' => true,
				);
			}

			
		}

		return $attributes;
	}

	private function insertValueAtPosition($arr, $insertedArray, $position) {
		$i = 0;
		$new_array=[];
		foreach ($arr as $key => $value) {
			if ($i == $position) {
				foreach ($insertedArray as $ikey => $ivalue) {
					$new_array[$ikey] = $ivalue;
				}
			}
			$new_array[$key] = $value;
			$i++;
		}
		return $new_array;
	}

    private function sr_plugin_block_select_playlist() {
        $sr_playlist_list = get_posts(array(
            'post_type' => SR_PLAYLIST_CPT,
            'showposts' => 999,
        ));
        $options = array();

        if ( ! empty( $sr_playlist_list ) && ! is_wp_error( $sr_playlist_list ) ){
            
            foreach ( $sr_playlist_list as $post ) {
                $options[] = array(
                    'label' => $post->post_title,
                    'value' => $post->ID,
                );
            }
        } else {
            $options[0] = esc_html__( 'Create a Playlist First', 'sonaar-music' );
        }
        return $options;
	}

}

new Sonaar_Block();

<?php
/**
* Radio Widget Class
*
* @since 1.6.0
* @todo  - Add options
*/

class Sonaar_Music_Widget extends WP_Widget{
    /**
    * Widget Defaults
    */
    
    public static $widget_defaults;
    
    /**
    * Register widget with WordPress.
    */
    
    function __construct (){
    
        
        $widget_ops = array(
        'classname'   => 'sonaar_music_widget',
        'description' => esc_html_x('A simple radio that plays a list of songs from selected albums.', 'Widget', 'sonaar-music')
        );
        
        self::$widget_defaults = array(
            'title'        => '',
            'store_title_text' => '',
            'albums'     	 => array(),
            'show_playlist' => 0,
            'hide_artwork' => 0,
            'sticky_player' => 0,
            'show_album_market' => 0,
            'show_track_market' => 0,
            //'remove_player' => 0, // deprecated and replaced by hide_timeline
            'hide_timeline' =>0,
            
            
            );
            
            if ( isset($_GET['load']) && $_GET['load'] == 'playlist.json' ) {
                $this->print_playlist_json();
        }
        
        parent::__construct('sonaar-music', esc_html_x('Sonaar: Music Player', 'Widget', 'sonaar-music'), $widget_ops);
        
    }
        
    /**
    * Front-end display of widget.
    */
    public function widget ( $args, $instance ){
            $instance = wp_parse_args( (array) $instance, self::$widget_defaults );
            $elementor_widget = (bool)( isset( $instance['hide_artwork'] ) )? true: false; //Return true if the widget is set in the elementor editor 
            $args['before_title'] = "<span class='heading-t3'></span>".$args['before_title'];
            $args['before_title'] = str_replace('h2','h3',$args['before_title']);
            $args['after_title'] = str_replace('h2','h3',$args['after_title']);
            /*$args['after_title'] = $args['after_title']."<span class='heading-b3'></span>";*/
            //if ( function_exists( 'run_sonaar_music_pro' ) ){
            $feed = ( isset( $instance['feed'] ) )? $instance['feed']: '';
            $feed_title =  ( isset( $instance['feed_title'] ) )? $instance['feed_title']: '';
            $feed_img =  ( isset( $instance['feed_img'] ) )? $instance['feed_img']: '';
            $el_widget_id = ( isset( $instance['el_widget_id'] ) )? $instance['el_widget_id']: '';
            $playlatestalbum = ( isset( $instance['play-latest'] ) ) ? true : false;
            $title = apply_filters( 'widget_title', $instance['title'], $instance, $this->id_base );
            $albums = $instance['albums'];

            if($playlatestalbum){
                $recent_posts = wp_get_recent_posts(array('post_type'=>SR_PLAYLIST_CPT, 'post_status' => 'publish', 'numberposts' => 1));
                if (!empty($recent_posts)){
                    $albums = $recent_posts[0]["ID"];
                }

            }
            
            if( empty($albums) ) {
                // SHORTCODE IS DISPLAYED BUT NO ALBUMS ID ARE SET. EITHER GET INFO FROM CURRENT POST OR RETURN NO PLAYLIST SELECTED
                $trackSet = '';
                $albums = get_the_ID();
                $album_tracks =  get_post_meta( $albums, 'alb_tracklist', true);

                if (is_array($album_tracks)){
                    $fileOrStream =  $album_tracks[0]['FileOrStream'];
                       
                    switch ($fileOrStream) {
                        case 'mp3':
                            if ( isset( $album_tracks[0]["track_mp3"] ) ) {
                                $trackSet = true;
                            }
                            break;

                        case 'stream':
                            if ( isset( $album_tracks[0]["stream_link"] ) ) {
                                $trackSet = true;
                            }
                            break;
                    }
                }                
                if (isset($feed) && strlen($feed) > 1 ){
                     $trackSet = true;

                }
                if ( ($album_tracks == 0 || !$trackSet) && (!isset($feed) && strlen($feed) < 1 )){
                    echo esc_html__("No playlist selected", 'sonaar-music');
                    return;
                }
                if (!$feed && !$trackSet){
                    return;
                }
            }
            $scrollbar = ( isset( $instance['scrollbar'] ) )? $instance['scrollbar']: false;
            $show_album_market = (bool) ( isset( $instance['show_album_market'] ) )? $instance['show_album_market']: 0;
            $show_track_market = (bool) ( isset( $instance['show_track_market'] ) )? $instance['show_track_market']: 0;
            $store_title_text = $instance['store_title_text'];
            $hide_artwork = (bool)( isset( $instance['hide_artwork'] ) )? $instance['hide_artwork']: false;
            $displayControlArtwork = (bool)( isset( $instance['display_control_artwork'] ) )? $instance['display_control_artwork']: false;
            $hide_control_under = (bool)( isset( $instance['hide_control_under'] ) )? $instance['hide_control_under']: false;
            $hide_track_title = (bool)( isset( $instance['hide_track_title'] ) )? $instance['hide_track_title']: false;
            $hide_progressbar = (bool)( isset( $instance['hide_progressbar'] ) )? $instance['hide_progressbar']: false;
            $hide_times = (bool)( isset( $instance['hide_times'] ) )? $instance['hide_times']: false;
            $artwork= (bool)( isset( $instance['artwork'] ) )? $instance['artwork']: false;
            $track_artwork = (bool)( isset( $instance['track_artwork'] ) )? $instance['track_artwork']: false;
            $remove_player = (bool) ( isset( $instance['remove_player'] ) )? $instance['remove_player']: false; // deprecated and replaced by hide_timeline. keep it for fallbacks
            $hide_timeline = (bool) ( isset( $instance['hide_timeline'] ) )? $instance['hide_timeline']: false;
            $notrackskip = (bool) ( isset( $instance['notrackskip'] ) )? $instance['notrackskip']: false;
            $progressbar_inline = (bool) ( isset( $instance['progressbar_inline'] ) )? $instance['progressbar_inline']: false;
            $sticky_player = (bool)( isset( $instance['sticky_player'] ) )? $instance['sticky_player']: false;
            $shuffle = (bool)( isset( $instance['shuffle'] ) )? $instance['shuffle']: false;
            $wave_color = (bool)( isset( $instance['wave_color'] ) )? $instance['wave_color']: false;
            $wave_progress_color = (bool)( isset( $instance['wave_progress_color'] ) )? $instance['wave_progress_color']: false;
            $show_playlist = (bool)( isset( $instance['show_playlist'] ) )? $instance['show_playlist']: false;
            $title_html_tag_playlist = ( isset( $instance['titletag_playlist'] ) )? $instance['titletag_playlist']: 'h3';
            $title_html_tag_soundwave = ( isset( $instance['titletag_soundwave'] ) )? $instance['titletag_soundwave']: 'div';
            $title_html_tag_playlist = ($title_html_tag_playlist == '') ? 'div' : $title_html_tag_playlist;
            $title_html_tag_soundwave = ($title_html_tag_soundwave == '') ? 'div' : $title_html_tag_soundwave;
            $hide_album_title = (bool)( isset( $instance['hide_album_title'] ) )? $instance['hide_album_title']: false;
            $hide_album_subtitle = (bool)( isset( $instance['hide_album_subtitle'] ) )? $instance['hide_album_subtitle']: false;
            $playlist_title = ( isset( $instance['playlist_title'] ) )? $instance['playlist_title']: false;           
        
            //Field validation
            $sr_html_allowed_tags = array('h1', 'h2', 'h3', 'h4','h5','h6','div','span', 'p');
            if (!in_array($title_html_tag_playlist, $sr_html_allowed_tags, true)) {
                $title_html_tag_playlist = 'h3';
            }
            if (!in_array($title_html_tag_soundwave, $sr_html_allowed_tags, true)) {
                $title_html_tag_soundwave = 'div';
            }
      
            if($sticky_player){
                if ( function_exists( 'run_sonaar_music_pro' )){
                    $sticky_player = ($instance['sticky_player']=="true" || $instance['sticky_player']==1) ? : false;
                }else{
                    $sticky_player = false;
                }
            }
            if($show_playlist){
                $show_playlist = ($instance['show_playlist']=="true" || $instance['show_playlist']==1) ? : false;      
            }
        
            if($show_track_market){
                $show_track_market = ($instance['show_track_market']=="true" || $instance['show_track_market']==1) ? : false;      
            }
            if($show_album_market){
                $show_album_market = ($instance['show_album_market']=="true" || $instance['show_album_market']==1) ? : false;      
            }
            if($hide_artwork){
                $hide_artwork = ($instance['hide_artwork']=="true" || $instance['hide_artwork']==1) ? : false;      
            }
            if($track_artwork){
                if ( function_exists( 'run_sonaar_music_pro' )){
                    $track_artwork = ($instance['track_artwork']=="true" || $instance['track_artwork']==1) ? : false;      
                }else{
                    $track_artwork = false;
                }
            }
            if($displayControlArtwork){
                $displayControlArtwork = ($instance['display_control_artwork']=="true" || $instance['display_control_artwork']==1) ? : false;      
            }
            if($hide_control_under){
                $hide_control_under = ($instance['hide_control_under']=="true") ? true : false;      
            }
            if($hide_album_title){
                $hide_album_title = ($instance['hide_album_title']=="true") ? true : false;      
            }
            if($hide_album_subtitle){
                $hide_album_subtitle = ($instance['hide_album_subtitle']=="true") ? true : false;      
            }
            if($hide_progressbar){
                $hide_progressbar = ($instance['hide_progressbar']=="true" || $instance['hide_progressbar']==1) ? true : false;      
            }
            if($progressbar_inline){
                $progressbar_inline = ($instance['progressbar_inline']=="true" || $instance['progressbar_inline']==1) ? true : false;      
            }
            if($hide_times){
                $hide_times = ($instance['hide_times']=="true" || $instance['hide_times']==1) ? true : false;      
            }
            if($notrackskip){
                $notrackskip = ($instance['notrackskip']=="true" || $instance['notrackskip']==1) ? 'on' : false;      
            }
            if($remove_player){
                $remove_player = ($instance['remove_player']=="true" || $instance['remove_player']==1) ? true : false;      
            }

            if($hide_timeline){
                $hide_timeline = ($instance['hide_timeline']=="true" || $instance['hide_timeline']==1) ? true : false;      
            }
            $hide_timeline_style = ( $remove_player || $hide_timeline )? 'style="display: none;"': '' ;

            $store_buttons = array();

            $playlist = $this->get_playlist($albums, $title, $feed_title, $feed, $feed_img, $el_widget_id, $artwork);

            if (isset($playlist['tracks'][0]['poster']) =="" || !$playlist['tracks'][0]['poster'] && !$artwork ){
                $hide_artwork = true;
            }

            if ( isset($playlist['tracks']) && ! empty($playlist['tracks']) )
                $player_message = esc_html_x('Loading tracks...', 'Widget', 'sonaar-music');
            else
                $player_message = esc_html_x('No tracks founds...', 'Widget', 'sonaar-music');
            
            /***/
            
            if ( ! $playlist )
                return;
            
            if($show_playlist) {
                $args['before_widget'] = str_replace("iron_widget_radio", "iron_widget_radio playlist_enabled", $args['before_widget']);
            }
        
		/* Enqueue Sonaar Music related CSS and Js file */
		wp_enqueue_style( 'sonaar-music' );
		wp_enqueue_style( 'sonaar-music-pro' );
		wp_enqueue_script( 'sonaar-music-mp3player' );
		wp_enqueue_script( 'sonaar-music-pro-mp3player' );
		wp_enqueue_script( 'sonaar_player' );
		if ( function_exists('sonaar_player') ) {
			add_action('wp_footer','sonaar_player', 12);
		}

        echo $args['before_widget'];
        
        if ( ! empty( $title ) )
            echo $args['before_title'] . esc_html($title) . $args['after_title'];
		
		if ( is_array($albums)) {
			$albums = implode(',', $albums);
		}
        if ( FALSE === get_post_status( $albums ) || get_post_status ( $albums ) == 'trash') {
            echo esc_html__('No playlist selected. Please select a playlist', 'sonaar-music');
            return;
        }
    
        $firstAlbum = explode(',', $albums);
        $firstAlbum = $firstAlbum[0];

        $playlist_title = ( isset( $instance['playlist_title'] ) )? $instance['playlist_title']: false;
        
        $ironAudioClass = '';
        //$ironAudioClass .= ( Sonaar_Music::get_option('waveformType') === 'simplebar' ) ? ' sr_simple_bar' :'';
        $ironAudioClass .= ( $show_playlist ) ? ' show-playlist' :'';
        $ironAudioClass .= ( $hide_artwork == "true" ) ? ' sonaar-no-artwork' :'';
        $ironAudioClass .= ' sr_waveform_' . Sonaar_Music::get_option('waveformType');
        
        $album_ids_with_show_market = ( $show_album_market )? $albums : 0 ;
        
        $format_playlist ='';

        if(Sonaar_Music::get_option('show_artist_name') ){
            $artistSeparator = (Sonaar_Music::get_option('artist_separator') && Sonaar_Music::get_option('artist_separator') != '' && Sonaar_Music::get_option('artist_separator') != 'by')?Sonaar_Music::get_option('artist_separator'): __('by', 'sonaar-music');
            $artistSeparator = ' ' . $artistSeparator . ' ';
        }else{
            $artistSeparator = '';
        }
        $storeButtonPosition = [];//$storeButtonPosition[ {track index} , {store index} ] , so $storeButtonPosition[ 0, 1 ] refers to the second(1) store button from the first(0) track
        $trackIndexRelatedToItsPost = 0; //variable required to set the data-store-id. Data-store-id is used to popup the right content.
        $currentTrackId = ''; //Used to set the $trackIndexRelatedToItsPost
        foreach( $playlist['tracks'] as $key1 => $track){
            $storeButtonPosition[$key1] = [];
            $trackUrl = $track['mp3'] ;
            $showLoading = $track['loading'] ;
            $song_store_list = '<span class="store-list">';
            if($currentTrackId != $track['sourcePostID']){ //Reset $trackIndexRelatedToItsPost counting. It is incremented at the end of the foreach.
                $currentTrackId = $track['sourcePostID'];
                $trackIndexRelatedToItsPost = 0; 
            }
            if(isset($track['album_store_list'][0])){
                $track['song_store_list'] = ( isset($track['song_store_list'][0]) ) ? array_merge($track['song_store_list'], $track['album_store_list']) : $track['album_store_list'];
                $track['has_song_store'] = true;
            }
            if ( $show_track_market && is_array($track['song_store_list']) ){
 
                if ($track['has_song_store']){
                    $song_store_list .= '<div class="song-store-list-menu"><i class="fas fa-ellipsis-v"></i><div class="song-store-list-container">';
                    
                    foreach( $track['song_store_list'] as $key2 => $store ){
                        $storeButtonPosition[$key1][$key2]=[];
                        if(isset($store['link-option']) && $store['link-option'] == 'popup'){
                            if( array_key_exists('store-content', $store) ){
                                array_push ($storeButtonPosition[$key1][$key2], $store['store-content']);
                            }
                        }
                        if(isset($store['store-icon'])){
                            $classes = 'song-store';
                            if(!isset($store['store-name'])){
                                $store['store-name']='';
                            }
                            
                            if(!isset($store['store-link'])){
                                $store['store-link']='#';
                            }
                            $href = 'href="' . esc_html($store['store-link']) . '"';
                            $download="";
                            $label = '';
                            if($store['store-icon'] == "fas fa-download"){
                                $download = ' download';
                            }
                            if(!isset($store['store-icon'])){
                                $store['store-icon']='';
                            }

                            if(!isset($store['store-target'])){
                                $store['store-target']='_blank';
                            }
                            

                            if(isset($store['link-option']) && $store['link-option'] == 'popup'){ //if Popup content
                               $classes .= ' sr-store-popup';
                               $store['store-target'] = '_self';
                               $href = '';
                            }
                            if( isset($store['show-label']) || Sonaar_Music::get_option('show_label') == 'true' ){
                                if ( (isset($store['show-label']) && $store['show-label'] == 'true' || (isset($store['show-label'])) && $store['show-label'] == '' && Sonaar_Music::get_option('show_label') == 'true') || ( !isset($store['show-label']) ) && Sonaar_Music::get_option('show_label') == 'true'){
                                    $classes .= ' sr_store_wc_round_bt';
                                    $label = $store['store-name'];
                                }
                            }
                            
                            $song_store_list .= '<a ' . $href .  $download . ' class="' . $classes . '" target="' .  esc_attr($store['store-target']) . '" title="' . esc_attr($store['store-name']) . '" data-source-post-id="' . $track['sourcePostID'] . '" data-store-id="' . $trackIndexRelatedToItsPost . '-' . $key2 . '" tabindex="1"><i class="' . $store['store-icon'] . '"></i>' . esc_attr($label) . '</a>';
                        }
                    }
                    $song_store_list .= '</div></div>';
                }
            }
            $song_store_list .= '</span>';
            $store_buttons = ( !empty($track["track_store"]) )? '<a class="button" target="_blank" href="'. esc_url( $track['track_store'] ) .'">'. esc_textarea( $track['track_buy_label'] ).'</a>' : '' ;
            $artistSeparator_string = ($track['track_artist'])?$artistSeparator:'';//remove separator if no track doesnt have artist

            $track_image_url = (($track_artwork && isset($track['track_image_id'])) && ($track['track_image_id'] != 0))? wp_get_attachment_image_src($track['track_image_id'], 'thumbnail', true)[0] : $track['poster'] ;
            $track_artwork_value = ($track_artwork && $track_image_url) ? '<img src=' . esc_url( $track_image_url ) . ' class="sr_track_cover" />': '';
            
            $format_playlist .= '<li 
            class="sr-playlist-item" 
            data-audiopath="' . esc_url( $trackUrl ) . '"
            data-showloading="' . $showLoading .'"
            data-albumTitle="' . esc_attr( $track['album_title'] ) . '"
            data-albumArt="' . esc_url( $track['poster'] ) . '"
            data-releasedate="' . esc_attr( $track['release_date'] ) . '"
            data-trackTitle="' . $track['track_title'] . $artistSeparator_string . $track['track_artist'] . '"
            data-trackID="' . $track['id'] . '"
            data-trackTime="' . $track['lenght'] . '"
            >'.
            $track_artwork_value .
            $song_store_list
            
            . '</li>';

            $trackIndexRelatedToItsPost++;//$trackIndexRelatedToItsPost is required to set the data-store-id. Data-store-id is used to popup the right content.
        }
       
        if( Sonaar_Music::get_option('waveformType') === 'wavesurfer' ) {
            $fakeWave = '';
        }else{
            $barHeight =(Sonaar_Music::get_option('sr_soundwave_height')) ? Sonaar_Music::get_option('sr_soundwave_height') : 70;
            $mediaElementStyle = (Sonaar_Music::get_option('waveformType') === 'mediaElement') ? 'style="height:'.$barHeight.'px"' : '';
            $fakeWave = '
            <div class="sonaar_fake_wave" '.$mediaElementStyle.'>
                <audio src="" class="sonaar_media_element"></audio>
                <div class="sonaar_wave_base">
                    <canvas id=' . $args["widget_id"] . '-container' . ' class="" height="'.$barHeight.'" width="2540"></canvas>
                    <svg></svg>
                </div>
                <div class="sonaar_wave_cut">
                    <canvas id=' . $args["widget_id"] . '-progress' . ' class="" height="'.$barHeight.'" width="2540"></canvas>
                    <svg></svg>
                </div>
            </div>';
        }
        $feedurl = ($feed) ? '1' : '0';

        $hide_times_current = (!$hide_times) ? '
            <div class="currentTime"></div>
        ' : '' ;
        $hide_times_total = (!$hide_times) ? '
            <div class="totalTime"></div>
        ' : '' ;

        $wave_margin = ($hide_times) ? 'style="margin-left:0px;margin-right:0px;"': ''; // remove margin needed for the current/total time

        $progressbar = '';
        $player_style = ($hide_progressbar) ? 'style="height:33px;"': '';
        if (!$hide_progressbar){
            $progressbar = '
                ' . $hide_times_current . ' 
                <div id="'.esc_attr($args["widget_id"]). '-' . bin2hex(random_bytes(5)) . '-wave" class="wave" ' . $wave_margin . '>
                ' . $fakeWave . ' 
                </div>
                ' . $hide_times_total . ' 
            ';
         }else{
             // hide the progress bar
             $progressbar = '
                <div id="'.esc_attr($args["widget_id"]). '-' . bin2hex(random_bytes(5)) . '-wave" class="wave" style="display:none;">
                ' . $fakeWave . '
                </div>
                
            ';
         }
        $control = '<div class="control">';
            if(count($playlist['tracks']) > 1 ){
                $control .=
                    '<div class="previous" style="opacity:0;">
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="18.33" x="0px" y="0px" viewBox="0 0 10.2 11.7" style="enable-background:new 0 0 10.2 11.7;" xml:space="preserve">
                            <polygon points="10.2,0 1.4,5.3 1.4,0 0,0 0,11.7 1.4,11.7 1.4,6.2 10.2,11.7"/>
                        </svg>
                    </div>';
            };
            $control .=
                '<div class="play" style="opacity:0;">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="26" height="31.47" x="0px" y="0px" viewBox="0 0 17.5 21.2" style="enable-background:new 0 0 17.5 21.2;" xml:space="preserve">
                        <path d="M0,0l17.5,10.9L0,21.2V0z"/>
                        <rect width="6" height="21.2"/>
                        <rect x="11.5" width="6" height="21.2"/>
                    </svg>
                </div>';
            if(count($playlist['tracks']) > 1 ){
                $control .=
                '<div class="next" style="opacity:0;">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="18.33" x="0px" y="0px" viewBox="0 0 10.2 11.7" style="enable-background:new 0 0 10.2 11.7;" xml:space="preserve">
                        <polygon points="0,11.7 8.8,6.4 8.8,11.7 10.2,11.7 10.2,0 8.8,0 8.8,5.6 0,0"/>
                    </svg>
                </div>';
            };
        $control .= '</div>';

        $tracktitle = (!$hide_track_title) ? '
            <'.$title_html_tag_soundwave.' class="track-title"></'.$title_html_tag_soundwave.'>
        ' : '';
        $class_player ='player ';
        $class_player .=($progressbar_inline) ? 'sr_player__inline ' : '';
        $album_title_html =  '<'.$title_html_tag_playlist.' class="sr_it-playlist-title">'. esc_attr($playlist_title) .'</'.$title_html_tag_playlist.'>';
        $album_subtitle_html =  '<div class="sr_it-playlist-release-date"><span class="sr_it-date-value">'. ( ( get_post_meta( $firstAlbum, 'alb_release_date', true ) )? get_post_meta($firstAlbum, 'alb_release_date', true ): '' ) . '</span></div>';
        $classControlPlayer = ($displayControlArtwork) ? ' sr_player_on_artwork' : '';
        $controlArtwork = ($displayControlArtwork) ? $control : '';
        $displayControlUnder = ($hide_control_under) ? '' : $control;
        $displayAlbumTitle = ($hide_album_title) ? '' : $album_title_html;
        $displayAlbumSubTitle = ($hide_album_subtitle) ? '' : $album_subtitle_html;

        $notrackskip = ($notrackskip == false) ? get_post_meta($albums, 'no_track_skip', true) : $notrackskip;
        $output = '
        <div class="iron-audioplayer ' . $ironAudioClass . '" id="'. esc_attr( $args["widget_id"] ) .'-' . bin2hex(random_bytes(5)) . '" data-id="' . $args["widget_id"] .'" data-albums="'. $albums .'"data-url-playlist="' . esc_url(home_url('?load=playlist.json&amp;title='.$title.'&amp;albums='.$albums.'&amp;feed_title='.$feed_title.'&amp;feed='.$feed.'&amp;feed_img='.$feed_img.'&amp;el_widget_id='.$el_widget_id.'&amp;artwork='.$artwork.'')) . '" data-sticky-player="'. esc_attr($sticky_player) . '" data-shuffle="'. esc_attr($shuffle) . '" data-playlist_title="'. esc_html($playlist_title) . '" data-scrollbar="'. esc_attr($scrollbar) . '" data-wave-color="'. esc_attr($wave_color) .'" data-wave-progress-color="'. esc_attr($wave_progress_color) . '" data-no-wave="'. esc_attr($hide_timeline) . '" data-hide-progressbar="'. esc_attr($hide_progressbar) . '" data-feedurl="'. esc_attr($feedurl) .'" data-notrackskip="'. esc_attr($notrackskip) .'" style="opacity:0;">
            
            <div class="sonaar-grid sonaar-grid-2 sonaar-grid-fullwidth-mobile">
                '.(!$hide_artwork || $hide_artwork!="true" ?
                    '<div class="sonaar-Artwort-box' . $classControlPlayer . '">
                    ' . $controlArtwork . '
                        <div class="album">
                            <div class="album-art">
                                <img alt="album-art">
                            </div>
                        </div>
                    </div>'
                : '').'
                <div class="playlist">
                 ' . $displayAlbumTitle . '
                 ' . $displayAlbumSubTitle . '
                    <ul class"">' . $format_playlist . '</ul>
                </div>
            </div>
            <div class="album-player" ' . $hide_timeline_style .'>
                
                <div class="album-title"></div>
                ' . $tracktitle . '
                <div class="' . $class_player . '" ' . $player_style . '>
                    <div class="sr_progressbar">' . $progressbar . ' </div>
                    ' . $displayControlUnder . '
                </div>
            </div>
            <div class="album-store">' . $this->get_market( $store_title_text, $album_ids_with_show_market, $feedurl, $el_widget_id ) . '</div>
        </div>'; 

        echo $output;
        
        //Temp. removed: Not required
        // echo $action;
        echo $args['after_widget'];
    }
    
    private function wc_add_to_cart($id = null){
       
        if ( $id == null || ( !defined( 'WC_VERSION' ) && get_site_option('SRMP3_ecommerce') != '1' ) ){
            return false;
        }

        return get_post_meta($id, 'wc_add_to_cart', true);
    }
    private function wc_buynow_bt($id = null){
        if ($id == null || ( !defined( 'WC_VERSION' ) && get_site_option('SRMP3_ecommerce') != '1' )){
            return false;
        }

        return get_post_meta($id, 'wc_buynow_bt', true);
    }
    private function get_market($store_title_text, $album_id = 0, $feedurl = 0, $el_widget_id = null){
        
        if( $album_id == 0 && !$feedurl)
        return;

        if (!$feedurl){ // source if from albumid
            $firstAlbum = explode(',', $album_id);
            $firstAlbum = $firstAlbum[0];
            $storeList = get_post_meta($firstAlbum, 'alb_store_list', true);

            $wc_add_to_cart =  $this->wc_add_to_cart($firstAlbum);
            $wc_buynow_bt =  $this->wc_buynow_bt($firstAlbum);
            $is_variable_product = ($wc_add_to_cart == 'true' || $wc_buynow_bt == 'true' ) ? $this->is_variable_product($firstAlbum) : '';
            $album_store_list = ($wc_add_to_cart == 'true' || $wc_buynow_bt == 'true') ? $this->push_woocart_in_storelist(get_post($firstAlbum), $is_variable_product, $wc_add_to_cart, $wc_buynow_bt) : false;
        } else if($feedurl = 1) {
             // source if from elementor widget
            if (!$el_widget_id)
            return;

            if ( \Elementor\Plugin::$instance->editor->is_edit_mode() ) {
                //__A. WE ARE IN EDITOR SO USE CURRENT POST META SOURCE TO UPDATE THE WIDGET LIVE OTHERWISE IT WONT UPDATE WITH LIVE DATA
                $storeList =  get_post_meta( $album_id, 'alb_store_list', true);
                if($storeList == ''){
                    return;
                }   
            }else{
                //__B. WE ARE IN FRONT-END SO USE SAVED POST META SOURCE
                $elementorData = get_post_meta( $album_id, '_elementor_data', true);
                $elementorData = json_decode($elementorData, true);
                $id = $el_widget_id;
                $results=[];

                if($elementorData){
                   $this->findData( $elementorData, $id, $results );
                   $storeList = (!empty($results['settings']['storelist_repeater'])) ? $results['settings']['storelist_repeater'] : '';
                }else{
                    return;
                } 
            }
        }
        if(isset($album_store_list) && is_array($album_store_list) && count($album_store_list) > 0){

            $storeList = (is_array($storeList)) ? array_merge($storeList,$album_store_list ): $album_store_list;
        }
            if ( is_array($storeList) && $storeList ){
                $output = '
                <div class="buttons-block">
                    <div class="ctnButton-block">
                        <div class="available-now">';
                            $output .= ( $store_title_text == NULL ) ? esc_html__("Available now on:", 'sonaar-music') : esc_html__($store_title_text);
                            $output .=  '
                        </div>
                        <ul class="store-list">';
                        if ($feedurl){
                            foreach ($storeList as $store ) {
                                if(!isset($store['store_name'])){
                                    $store['store_name']="";
                                }
                                if(!isset($store['store_link'])){
                                    $store['store_link']="";
                                }

                                if(array_key_exists ( 'store_icon' , $store )){
                                    $icon = ( $store['store_icon']['value'] )? '<i class="' . $store['store_icon']['value'] . '"></i>': '';
                                }else{
                                    $icon ='';
                                }
                                $output .= '<li><a class="button" href="' . esc_url( $store['store_link'] ) . '" target="_blank">'. $icon . $store['store_name'] . '</a></li>';
                            }
                        }else{
                            foreach ($storeList as $key => $store ) {
                                if(!isset($store['store-name'])){
                                    $store['store-name']="";
                                }
                                if(!isset($store['store-link'])){
                                    $store['store-link']="";
                                }
                                if(!isset($store['store-target'])){
                                    $store['store-target']='_blank';
                                }

                                if(array_key_exists ( 'store-icon' , $store )){
                                    $icon = ( $store['store-icon'] )? '<i class="' . $store['store-icon'] . '"></i>': '';
                                }else{
                                    $icon ='';
                                }
                                $classes = 'button';

                                $href = 'href="' . $store['store-link'] . '"';
                                if(isset($store['link-option']) && $store['link-option'] == 'popup'){ 
                                    $classes .= ' sr-store-popup';
                                    $store['store-target'] = '_self';
                                    $href = '';
                                }
                                $output .= '<li><a class="'. $classes .'" data-source-post-id="' . $firstAlbum .'" data-store-id="a-'. $key .'" '. $href .' target="' . $store['store-target'] . '">'. $icon . $store['store-name'] . '</a></li>';
                            }
                        }

                        $output .= '
                        </ul>
                    </div>
                </div>';
                
                return $output;
            }        
    }

    /**
    * Back-end widget form.
    */
    
    public function form ( $instance ){
        $instance = wp_parse_args( (array) $instance, self::$widget_defaults );
            
            $title = esc_attr( $instance['title'] );
            $albums = $instance['albums'];
            $show_playlist = (bool)$instance['show_playlist'];
            $sticky_player = (bool)$instance['sticky_player'];
            $hide_artwork = (bool)$instance['hide_artwork'];
            $show_album_market = (bool)$instance['show_album_market'];
            $show_track_market = (bool)$instance['show_track_market'];
            //$remove_player = (bool)$instance['remove_player']; // deprecated and replaced by hide_timeline
            $hide_timeline = (bool)$instance['hide_timeline'];
            
            $all_albums = get_posts(array(
            'post_type' => SR_PLAYLIST_CPT
            , 'posts_per_page' => -1
            , 'no_found_rows'  => true
            ));
            
            if ( !empty( $all_albums ) ) :?>

  <p>
    <label for="<?php echo $this->get_field_id('title'); ?>">
      <?php _ex('Title:', 'Widget', 'sonaar-music'); ?>
    </label>
    <input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>" placeholder="<?php _e('Popular Songs', 'sonaar-music'); ?>" />
  </p>
  <p>
    <label for="<?php echo $this->get_field_id('albums'); ?>">
      <?php _ex('Album:', 'Widget', 'sonaar-music'); ?>
    </label>
    <select class="widefat" id="<?php echo $this->get_field_id('albums'); ?>" name="<?php echo $this->get_field_name('albums'); ?>[]" multiple="multiple">
      <?php foreach($all_albums as $a): ?>

        <option value="<?php echo $a->ID; ?>" <?php echo ( is_array($albums) && in_array($a->ID, $albums) ? ' selected="selected"' : ''); ?>>
          <?php echo esc_attr($a->post_title); ?>
        </option>

        <?php endforeach; ?>
    </select>
  </p>
<?php if ( function_exists( 'run_sonaar_music_pro' ) ): ?>
  <p>
    <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('sticky_player'); ?>" name="<?php echo $this->get_field_name('sticky_player'); ?>" <?php checked( $sticky_player ); ?> />
    <label for="<?php echo $this->get_field_id('sticky_player'); ?>">
      <?php _e( 'Enable Sticky Audio Player', 'sonaar-music'); ?>
    </label>
    <br />
  </p>
<?php endif ?>
  <p>
    <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_playlist'); ?>" name="<?php echo $this->get_field_name('show_playlist'); ?>" <?php checked( $show_playlist ); ?> />
    <label for="<?php echo $this->get_field_id('show_playlist'); ?>">
      <?php _e( 'Show Playlist', 'sonaar-music'); ?>
    </label>
    <br />
  </p>

  <p>
    <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_album_market'); ?>" name="<?php echo $this->get_field_name('show_album_market'); ?>" <?php checked( $show_album_market ); ?> />
    <label for="<?php echo $this->get_field_id('show_album_market'); ?>">
      <?php _e( 'Show Album store', 'sonaar-music'); ?>
    </label>
    <br />
  </p>
  <p>
    <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hide_artwork'); ?>" name="<?php echo $this->get_field_name('hide_artwork'); ?>" <?php checked( $hide_artwork ); ?> />
    <label for="<?php echo $this->get_field_id('hide_artwork'); ?>">
      <?php _e( 'Hide Album Cover', 'sonaar-music'); ?>
    </label>
    <br />
  </p>
  <p>
    <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('show_track_market'); ?>" name="<?php echo $this->get_field_name('show_track_market'); ?>" <?php checked( $show_track_market ); ?> />
    <label for="<?php echo $this->get_field_id('show_track_market'); ?>">
      <?php _e( 'Show Track store', 'sonaar-music'); ?>
    </label>
    <br />
  </p>
  </p>
  <p>
    <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id('hide_timeline'); ?>" name="<?php echo $this->get_field_name('hide_timeline'); ?>" <?php checked( $hide_timeline ); ?> />
    <label for="<?php echo $this->get_field_id('hide_timeline'); ?>">
      <?php _e( 'Remove Visual Timeline', 'sonaar-music'); ?>
    </label>
    <br />
  </p>

  <?php
            else:
                
            echo wp_kses_post( '<p>'. sprintf( _x('No albums have been created yet. <a href="%s">Create some</a>.', 'Widget', 'sonaar-music'), esc_url(admin_url('edit.php?post_type=' . SR_PLAYLIST_CPT)) ) .'</p>' );
            
            endif;
    }
    
    
    
    
    
    
    /**
    * Sanitize widget form values as they are saved.
    */
    
    public function update ( $new_instance, $old_instance )
    {
        $instance = wp_parse_args( $old_instance, self::$widget_defaults );
            
            $instance['title'] = strip_tags( stripslashes($new_instance['title']) );
            $instance['albums'] = $new_instance['albums'];
            $instance['show_playlist']  = (bool)$new_instance['show_playlist'];
            $instance['hide_artwork']  = (bool)$new_instance['hide_artwork'];
            $instance['sticky_player']  = (bool)$new_instance['sticky_player'];
            $instance['show_album_market']  = (bool)$new_instance['show_album_market'];
            $instance['show_track_market']  = (bool)$new_instance['show_track_market'];
            //$instance['remove_player']  = (bool)$new_instance['remove_player']; deprecated and replaced by hide_timeline
            $instance['hide_timeline']  = (bool)$new_instance['hide_timeline'];
            
            return $instance;
    }
    
    
    private function print_playlist_json() {
        $jsonData = array();
        
        $title = !empty($_GET["title"]) ? $_GET["title"] : null;
        $albums = !empty($_GET["albums"]) ? $_GET["albums"] : array();
        $feed_title = !empty($_GET["feed_title"]) ? $_GET["feed_title"] : null;
        $feed = !empty($_GET["feed"]) ? $_GET["feed"] : null;
        $feed_img = !empty($_GET["feed_img"]) ? $_GET["feed_img"] : null;
        $el_widget_id = !empty($_GET["el_widget_id"]) ? $_GET["el_widget_id"] : null;
        $artwork =  !empty($_GET["artwork"]) ? $_GET["artwork"] : null;
        $playlist = $this->get_playlist($albums, $title, $feed_title, $feed, $feed_img, $el_widget_id, $artwork);
        
        if(!is_array($playlist) || empty($playlist['tracks']))
        return;
        
        wp_send_json($playlist);
        
    }
    private function findData($arr, $id, &$results = []){
        foreach ($arr as $data) {           
            if ( is_array($data) ){
                if (array_key_exists('id', $data)) {
                    if($data['id'] == $id){
                        $results = $data;
                    }
                }
                $this->findData( $data, $id, $results);     
            }
        }
        return false ;
    }
    private function get_wc_price($id){
        if ( !defined( 'WC_VERSION' ) ){
            return;
        }
       
        $currency = get_woocommerce_currency_symbol();
        $currency_pos = get_option('woocommerce_currency_pos');
       
        $product_price = get_post_meta( $id, '_price', true );
        
        if ($product_price == ''){
            $product_price = esc_html__("Free", 'sonaar-music');
            return $product_price;
        }
        
        if ($product_price != ''){
            if ( $currency_pos == 'left' ){
                return html_entity_decode($currency . $product_price);
            } else if ( $currency_pos == 'left_space' ) {
                return html_entity_decode($currency . ' ' . $product_price);
            }else if( $currency_pos == 'right' ){
                return html_entity_decode($product_price . $currency);
            }else{
                return html_entity_decode($product_price . ' ' . $currency);
            }
        }
    }
    private function is_variable_product($id){
        $product_attributes = get_post_meta( $id, '_product_attributes', false );

        if (!is_array($product_attributes) || count($product_attributes) ==0 ){
            return false;
        }
        $prod_has_attributes = array_column($product_attributes[0], 'is_variation');
        foreach($prod_has_attributes as $a){
            if ($a == 1){
                return true;
            }
        }
        return false;
    }

    private function push_woocart_in_storelist($post, $is_variable_product = null, $wc_add_to_cart = false, $wc_buynow_bt = false){
        if (  !defined( 'WC_VERSION' ) || ( defined( 'WC_VERSION' ) && !function_exists( 'run_sonaar_music_pro' ) && get_site_option('SRMP3_ecommerce') != '1' ) ){
            return false;
		}

        $wc_bt_type = Sonaar_Music::get_option('wc_bt_type');
        $store_list =  array();
        
        if ( $wc_bt_type == 'wc_bt_type_inactive' ){
            return $store_list;
        }
        
        $post_id = $post->ID;
        $slug = $post->post_name;
        
    
        $homeurl = esc_url( home_url() );
        $product_permalink = get_option('woocommerce_permalinks')['product_base'];
        $product_slug = $slug;
        $checkout_url = ( defined( 'WC_VERSION' ) ) ? wc_get_checkout_url() : '';
        $product_price = ( $wc_bt_type !='wc_bt_type_label' ) ? $this->get_wc_price($post_id) : '';
    
        if( $wc_add_to_cart == 'true' ){
            $label = (Sonaar_Music::get_option('wc_add_to_cart_text') && Sonaar_Music::get_option('wc_add_to_cart_text') != '' && Sonaar_Music::get_option('wc_add_to_cart_text') != 'Add to Cart') ? Sonaar_Music::get_option('wc_add_to_cart_text') : __('Add to Cart', 'sonaar-music');
            $label = ($wc_bt_type == 'wc_bt_type_price') ? '' : $label . ' '; 
            $url_if_variation = $homeurl . $product_permalink . '/' . $product_slug; //no add to cart since its a variation and user must choose variation from the single page
            $url_if_no_variation = get_permalink(get_the_ID()) . '?add-to-cart=' . $post_id;
            $storeicon = ( Sonaar_Music::get_option('wc_bt_show_icon') =='true' ) ? 'fas fa-cart-plus' : '';
            $pageUrl = ($is_variable_product == 1) ? $url_if_variation : $url_if_no_variation ;

            array_push($store_list, [
                'store-icon'    => $storeicon,
                'store-link'    => $pageUrl,
                'store-name'    => $label . $product_price,
                'store-target'  => '_self',
                'show-label'    => true
            ]);
        }
        if( $wc_buynow_bt == 'true' ){
            $label = (Sonaar_Music::get_option('wc_buynow_text') && Sonaar_Music::get_option('wc_buynow_text') != '' && Sonaar_Music::get_option('wc_buynow_text') != 'Buy Now' ) ? Sonaar_Music::get_option('wc_buynow_text') : __('Buy Now', 'sonaar-music');
            $label = ($wc_bt_type == 'wc_bt_type_price') ? '' : $label . ' '; 
            $url_if_variation = $homeurl . $product_permalink . '/' . $product_slug; //no add to cart since its a variation and user must choose variation from the single page;
            $url_if_no_variation = $checkout_url . '?add-to-cart=' . $post_id;
            $storeicon = ( Sonaar_Music::get_option('wc_bt_show_icon') == 'true' ) ? 'fas fa-shopping-cart' : '';
            $pageUrl = ($is_variable_product == 1) ? $url_if_variation : $url_if_no_variation ;

            array_push($store_list, [
                'store-icon'    => $storeicon,
                'store-link'    => $pageUrl,
                'store-name'    =>  $label . $product_price,
                'store-target'  => '_self',
                'show-label'    => true
            ]);
        }
        return $store_list;
    }
    private function checkACF($field, $ids, $loop = true){
        if (substr( $field, 0, 3 ) === "acf") { 
            if (!function_exists('get_field')) return $field;
            if (empty($ids[0])){
                // make sure to get current post id if no album id has been specified so we can run the checkACF function.
                $ids[0] = get_post(get_the_ID());
            }
            $strings = '';
            foreach ( $ids as $a ) {
                if (!$loop){
                    $strings .= get_field( $field,  $a->ID );
                    return $strings;
                }
                $separator = ($a != end($ids)) ? " || " : '';
                $strings .= get_field( $field,  $a->ID ) . $separator;
            }
            return $strings;
        }
        return $field;
    }

    private function get_playlist($album_ids = array(), $title = null, $feed_title = null, $feed = null, $feed_img = null, $el_widget_id = null, $artwork = null) {
        global $post;
        $playlist = array();
        $tracks = array();
        $albums = '';

        if(!is_array($album_ids)) {
            $album_ids = explode(",", $album_ids);
        }
        $albums = get_posts(array(
            'numberposts' => -1,
            'post_type' => ( Sonaar_Music::get_option('srmp3_posttypes') != null ) ? Sonaar_Music::get_option('srmp3_posttypes') : SR_PLAYLIST_CPT,//array(SR_PLAYLIST_CPT, 'post', 'product'),
            'post__in' => $album_ids,
            'lang' => ''
        ));

        if(Sonaar_Music::get_option('show_artist_name') ){
            $artistSeparator = (Sonaar_Music::get_option('artist_separator') && Sonaar_Music::get_option('artist_separator') != '' && Sonaar_Music::get_option('artist_separator') != 'by' )?Sonaar_Music::get_option('artist_separator'): __('by', 'sonaar-music');
            $artistSeparator = ' ' . $artistSeparator . ' ';
        }else{
            $artistSeparator = '';
        }

        if( $feed == '1' ){
            //001. FEED = 1 MEANS ITS A FEED BUILT WITH ELEMENTOR AND USE TRACKS UPLOAD. IF A PREDEFINED PLAYLIST IS SET, GO TO 003. FEED = 1 VALUE IS SET IN THE SR-MUSIC-PLAYER.PHP

            if ( \Elementor\Plugin::$instance->editor->is_edit_mode() ) {
                //__A. WE ARE IN EDITOR SO USE CURRENT POST META SOURCE TO UPDATE THE WIDGET LIVE OTHERWISE IT WONT UPDATE WITH LIVE DATA
                $album_tracks =  get_post_meta( $album_ids[0], 'srmp3_elementor_tracks', true);
                if($album_tracks == ''){
                    return;
                }   
            }else{
                //__B. WE ARE IN FRONT-END SO USE SAVED POST META SOURCE
                $elementorData = get_post_meta( $album_ids[0], '_elementor_data', true);
                $elementorData = (is_string($elementorData)) ? json_decode($elementorData, true) : ''; // make sure json_decode is parsing a string
                if(empty($elementorData)){
                    return;
                }
                
                $id = $el_widget_id;
                $results=[];

                $this->findData( $elementorData, $id, $results );

                $album_tracks = $results['settings']['feed_repeater'];

                $artwork = ( isset($results['settings']['album_img']['id'] )) ? wp_get_attachment_image_src( $results['settings']['album_img']['id'], 'large' )[0] : '';
            }
        
            $num = 1;
            for($i = 0 ; $i < count($album_tracks) ; $i++) {

                $track_title = ( isset($album_tracks[$i]['feed_track_title'] )) ? $album_tracks[$i]['feed_track_title'] : false;
                $track_length = false;
                $album_title = false;

                if ( isset( $album_tracks[$i]['feed_track_img']['id'] ) && $album_tracks[$i]['feed_track_img']['id'] != ''){
                    $thumb_url = wp_get_attachment_image_src( $album_tracks[$i]['feed_track_img']['id'], 'large' )[0];
                    $thumb_id = $album_tracks[$i]['feed_track_img']['id'];
                }else{
                   $thumb_url = $artwork;
                }
                

                if( isset( $album_tracks[$i]['feed_source_file']['url'] ) ){
                    // TRACK SOURCE IS FROM MEDIA LIBRARY
                    $audioSrc = $album_tracks[$i]['feed_source_file']['url'];
                    $mp3_id = $album_tracks[$i]['feed_source_file']['id'];
                    $mp3_metadata = wp_get_attachment_metadata( $mp3_id );
                    $track_length = ( isset( $mp3_metadata['length_formatted'] ) && $mp3_metadata['length_formatted'] !== '' )? $mp3_metadata['length_formatted'] : false;
                    $album_title = ( isset( $mp3_metadata['album'] ) && $mp3_metadata['album'] !== '' )? $mp3_metadata['album'] : false;
                    $track_artist = ( isset( $mp3_metadata['artist'] ) && $mp3_metadata['artist'] !== '' && Sonaar_Music::get_option('show_artist_name') )? $mp3_metadata['artist'] : false;
                    $track_title = ( isset( $mp3_metadata["title"] ) && $mp3_metadata["title"] !== '' )? $mp3_metadata["title"] : false ;
                    $track_title = ( get_the_title( $mp3_id ) !== '' && $track_title !== get_the_title( $mp3_id ) ) ? get_the_title( $mp3_id ) : $track_title;
                    $track_title = html_entity_decode( $track_title, ENT_COMPAT, 'UTF-8' );


                }else if( isset( $album_tracks[$i]['feed_source_external_url']['url'] ) ){
                     // TRACK SOURCE IS AN EXTERNAL LINK
                    $audioSrc = $album_tracks[$i]['feed_source_external_url']['url'];
                }else{
                    $audioSrc = '';
                }
                $showLoading = true;

                ////////
                
                $album_tracks[$i] = array();
                $album_tracks[$i]["id"] = '';
                $album_tracks[$i]["mp3"] = $audioSrc;
                $album_tracks[$i]["loading"] = $showLoading;
                $album_tracks[$i]["track_title"] = ( $track_title )? $track_title : "Track ". $num;
                $album_tracks[$i]["track_artist"] = ( isset( $track_artist ) && $track_artist != '' )? $track_artist : '';
                $album_tracks[$i]["lenght"] = $track_length;
                $album_tracks[$i]["album_title"] = ( $album_title )? $album_title : '';
                $album_tracks[$i]["poster"] = ( $thumb_url )? urldecode($thumb_url) : null;
                if(isset($thumb_id)){
                    $album_tracks[$i]["track_image_id"] = $thumb_id;    
                }
                
                $album_tracks[$i]["release_date"] = false;
                $album_tracks[$i]["song_store_list"] ='';
                $album_tracks[$i]["has_song_store"] = false;
                $album_tracks[$i]['sourcePostID'] = null;
                $thumb_id = null;
                $num++;
            }
                $tracks = array_merge($tracks, $album_tracks);

        }else if ( $feed && $feed != '1'){
            // 002. FEED MEANS SOURCE IS USED DIRECTLY IN THE SHORTCODE ATTRIBUTE
            $feed = $this->checkACF($feed, $albums);
            $feed_title = $this->checkACF($feed_title, $albums);
            $feed_img = $this->checkACF($feed_img, $albums);
            $artwork = $this->checkACF($artwork, $albums, false); 

            $thealbum = array();

            $feed_ar = explode('||', $feed);
            $feed_title_ar = explode('||', $feed_title);
            $feed_img_ar = explode('||', $feed_img);

            $thealbum = [$feed_ar];
            
            foreach($thealbum as $a) {
                $album_tracks = $feed_ar;
                $num = 1;
                for($i = 0 ; $i < count($feed_ar) ; $i++) {
                    $track_title = ( isset( $feed_title_ar[$i] )) ? $feed_title_ar[$i] : false;

                    if ( isset($feed_img_ar[$i]) ){
                        $thumb_url = $feed_img_ar[$i];
                    }else{
                       $thumb_url = $artwork;
                    }
                    
                    ////////
                    $audioSrc = $feed_ar[$i];
                    $showLoading = true;
                    ////////
                    $album_tracks[$i] = array();
                    $album_tracks[$i]["id"] = '';
                    $album_tracks[$i]["mp3"] = $audioSrc;
                    $album_tracks[$i]["loading"] = $showLoading;
                    $album_tracks[$i]["track_title"] = ( $track_title )? $track_title : "Track ". $num;
                    $album_tracks[$i]["track_artist"] = ( isset( $track_artist ) && $track_artist != '' )? $track_artist : '';
                    $album_tracks[$i]["lenght"] = false;
                    $album_tracks[$i]["album_title"] = '';
                    $album_tracks[$i]["poster"] = ( $thumb_url )? urldecode($thumb_url) : $artwork;
                    $album_tracks[$i]["release_date"] = false;
                    $album_tracks[$i]["song_store_list"] ='';
                    $album_tracks[$i]["has_song_store"] = false;
                    $album_tracks[$i]['sourcePostID'] = null;
                    $num++;
                }

                $tracks = array_merge($tracks, $album_tracks);
            }     
        } else {
            // 003. FEED SOURCE IS A POSTID -> ALB_TRACKLIST POST META

            foreach ( $albums as $a ) {
               
                $album_tracks =  get_post_meta( $a->ID, 'alb_tracklist', true);
                $wc_add_to_cart = $this->wc_add_to_cart($a->ID);
                $wc_buynow_bt =  $this->wc_buynow_bt($a->ID);
                $is_variable_product = ($wc_add_to_cart == 'true' || $wc_buynow_bt == 'true' ) ? $this->is_variable_product($a->ID) : '';
              
                if ( get_post_meta( $a->ID, 'reverse_tracklist', true) ){
                    $album_tracks = array_reverse($album_tracks); //reverse tracklist order option
                }
                
                if ($album_tracks!=''){ 
                    for($i = 0 ; $i < count($album_tracks) ; $i++) {

                       
                        $fileOrStream =  $album_tracks[$i]['FileOrStream'];
                        $thumb_id = get_post_thumbnail_id($a->ID);
                        if(isset($album_tracks[$i]["track_image_id"]) && $album_tracks[$i]["track_image_id"] != ''){
                            $thumb_id = $album_tracks[$i]["track_image_id"];
                        }
                        
                        $thumb_url = ( $thumb_id )? wp_get_attachment_image_src($thumb_id, Sonaar_Music::get_option('music_player_coverSize'), true)[0] : false ;
                        if ($artwork){ //means artwork is set in the shortcode so prioritize this image instead of the the post featured image.
                            $thumb_url = $artwork;
                        }
                        //$store_array = array();
                        $track_title = false;
                        $album_title = false;
                        $mp3_id = false;
                        $audioSrc = '';
                        $song_store_list = isset($album_tracks[$i]["song_store_list"]) ? $album_tracks[$i]["song_store_list"] : '' ;
                        $album_store_list = ($wc_add_to_cart == 'true' || $wc_buynow_bt == 'true') ? $this->push_woocart_in_storelist($a, $is_variable_product, $wc_add_to_cart, $wc_buynow_bt) : false;
                        
                        $has_song_store =false;
                        if (isset($song_store_list[0])){
                            $has_song_store = true; 
                        }
                        
                        $showLoading = false;
                        $track_length = false;

                        switch ($fileOrStream) {
                            case 'mp3':
                                if ( isset( $album_tracks[$i]["track_mp3"] ) ) {
                                    $mp3_id = $album_tracks[$i]["track_mp3_id"];
                                    $mp3_metadata = wp_get_attachment_metadata( $mp3_id );
                                    $track_title = ( isset( $mp3_metadata["title"] ) && $mp3_metadata["title"] !== '' )? $mp3_metadata["title"] : false ;
                                    $track_title = ( get_the_title($mp3_id) !== '' && $track_title !== get_the_title($mp3_id))? get_the_title($mp3_id): $track_title;
                                    $track_title = html_entity_decode($track_title, ENT_COMPAT, 'UTF-8');
                                    $track_artist = ( isset( $mp3_metadata['artist'] ) && $mp3_metadata['artist'] !== '' && Sonaar_Music::get_option('show_artist_name') )? $mp3_metadata['artist'] : false;
                                    $album_title = ( isset( $mp3_metadata['album'] ) && $mp3_metadata['album'] !== '' )? $mp3_metadata['album'] : false;
                                    $track_length = ( isset( $mp3_metadata['length_formatted'] ) && $mp3_metadata['length_formatted'] !== '' )? $mp3_metadata['length_formatted'] : false;
                                    $audioSrc = wp_get_attachment_url($mp3_id);
                                    $showLoading = true;
                                }
                                break;

                            case 'stream':
                                
                                $audioSrc = ( array_key_exists ( "stream_link" , $album_tracks[$i] ) && $album_tracks[$i]["stream_link"] !== '' )? $album_tracks[$i]["stream_link"] : false;
                                $track_title = (  array_key_exists ( 'stream_title' , $album_tracks[$i] ) && $album_tracks[$i]["stream_title"] !== '' )? $album_tracks[$i]["stream_title"] : false;
                                $album_title = ( isset ($album_tracks[$i]["stream_album"]) && $album_tracks[$i]["stream_album"] !== '' )? $album_tracks[$i]["stream_album"] : false;
                                $showLoading = true;
                                break;
                            
                            default:
                                $album_tracks[$i] = array();
                                break;
                        }
                
                        $num = 1;
                       
                        $album_tracks[$i] = array();
                        $album_tracks[$i]["id"] = ( $mp3_id )? $mp3_id : '' ;
                        $album_tracks[$i]["mp3"] = $audioSrc;
                        $album_tracks[$i]["loading"] = $showLoading;
                        $album_tracks[$i]["track_title"] = ( $track_title )? $track_title : "Track ". $num++;
                        $album_tracks[$i]["track_artist"] = ( isset( $track_artist ) && $track_artist != '' )? $track_artist : '';
                        $album_tracks[$i]["lenght"] = $track_length;
                        $album_tracks[$i]["album_title"] = ( $album_title )? $album_title : $a->post_title;
                        $album_tracks[$i]["poster"] = urldecode($thumb_url);
                        if(isset($thumb_id)){
                            $album_tracks[$i]["track_image_id"] = $thumb_id;    
                        }
                        $album_tracks[$i]["release_date"] = get_post_meta($a->ID, 'alb_release_date', true);
                        $album_tracks[$i]["song_store_list"] = $song_store_list;
                        $album_tracks[$i]["has_song_store"] = $has_song_store;
                        $album_tracks[$i]["album_store_list"] = $album_store_list;
                        $album_tracks[$i]['sourcePostID'] = $a->ID;
                        $thumb_id = null;
                    
                    }
            
                    $tracks = array_merge($tracks, $album_tracks);

                }

            }
        }
        $playlist['playlist_name'] = $title;
        if ( empty($playlist['playlist_name']) ) $playlist['playlist_name'] = "";

        $playlist['artist_separator'] = $artistSeparator;

        $playlist['tracks'] = $tracks;
        if ( empty($playlist['tracks']) ) $playlist['tracks'] = array();
        return $playlist;

    }

}
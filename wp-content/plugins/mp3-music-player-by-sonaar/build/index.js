/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayLikeToArray.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) {
    arr2[i] = arr[i];
  }

  return arr2;
}

module.exports = _arrayLikeToArray;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js":
/*!***************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/arrayWithHoles.js ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _arrayWithHoles(arr) {
  if (Array.isArray(arr)) return arr;
}

module.exports = _arrayWithHoles;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js":
/*!*********************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _iterableToArrayLimit(arr, i) {
  var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"];

  if (_i == null) return;
  var _arr = [];
  var _n = true;
  var _d = false;

  var _s, _e;

  try {
    for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) {
      _arr.push(_s.value);

      if (i && _arr.length === i) break;
    }
  } catch (err) {
    _d = true;
    _e = err;
  } finally {
    try {
      if (!_n && _i["return"] != null) _i["return"]();
    } finally {
      if (_d) throw _e;
    }
  }

  return _arr;
}

module.exports = _iterableToArrayLimit;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/nonIterableRest.js":
/*!****************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/nonIterableRest.js ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _nonIterableRest() {
  throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

module.exports = _nonIterableRest;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/slicedToArray.js":
/*!**************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/slicedToArray.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayWithHoles = __webpack_require__(/*! ./arrayWithHoles.js */ "./node_modules/@babel/runtime/helpers/arrayWithHoles.js");

var iterableToArrayLimit = __webpack_require__(/*! ./iterableToArrayLimit.js */ "./node_modules/@babel/runtime/helpers/iterableToArrayLimit.js");

var unsupportedIterableToArray = __webpack_require__(/*! ./unsupportedIterableToArray.js */ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js");

var nonIterableRest = __webpack_require__(/*! ./nonIterableRest.js */ "./node_modules/@babel/runtime/helpers/nonIterableRest.js");

function _slicedToArray(arr, i) {
  return arrayWithHoles(arr) || iterableToArrayLimit(arr, i) || unsupportedIterableToArray(arr, i) || nonIterableRest();
}

module.exports = _slicedToArray;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js":
/*!***************************************************************************!*\
  !*** ./node_modules/@babel/runtime/helpers/unsupportedIterableToArray.js ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var arrayLikeToArray = __webpack_require__(/*! ./arrayLikeToArray.js */ "./node_modules/@babel/runtime/helpers/arrayLikeToArray.js");

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return arrayLikeToArray(o, minLen);
}

module.exports = _unsupportedIterableToArray;
module.exports["default"] = module.exports, module.exports.__esModule = true;

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/helpers/slicedToArray */ "./node_modules/@babel/runtime/helpers/slicedToArray.js");
/* harmony import */ var _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/element */ "@wordpress/element");
/* harmony import */ var _wordpress_element__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__);



(function (wp) {
  var registerBlockType = wp.blocks.registerBlockType; //Blocks API

  var _wp$element = wp.element,
      createElement = _wp$element.createElement,
      useState = _wp$element.useState,
      render = _wp$element.render; //React.createElement

  var __ = wp.i18n.__; //translation functions

  var _wp$components = wp.components,
      PanelBody = _wp$components.PanelBody,
      SelectControl = _wp$components.SelectControl,
      TextControl = _wp$components.TextControl,
      ToggleControl = _wp$components.ToggleControl,
      RangeControl = _wp$components.RangeControl,
      ColorPalette = _wp$components.ColorPalette,
      Button = _wp$components.Button,
      Modal = _wp$components.Modal; //WordPress form inputs and server-side renderer

  var _wp$blockEditor = wp.blockEditor,
      InspectorControls = _wp$blockEditor.InspectorControls,
      RichText = _wp$blockEditor.RichText;
  var serverSideRender = wp.serverSideRender;
  var player_load = '';
  var sonaarIcon = wp.element.createElement('svg', {
    width: 20,
    height: 20,
    viewBox: '0 0 512 512'
  }, wp.element.createElement('path', {
    d: "M250.5,226.77V3.92C114.56,3.92,4.36,114.12,4.36,250.06s110.2,246.13,246.13,246.13V273.34 c11.73,125.01,116.95,222.85,245.03,222.85V3.92C367.44,3.92,262.23,101.76,250.5,226.77z"
  }));
  registerBlockType('sonaar/sonaar-block', {
    // Built-in attributes
    title: 'Sonaar MP3 Audio Player',
    description: __("A stunning audio player.", "sonaar-music"),
    icon: sonaarIcon,
    category: 'embed',
    keywords: ['mp3', 'player', 'audio', 'sonaar', 'podcast', 'music', 'beat', 'sermon', 'episode', 'radio', 'stream', 'sonar', 'sonaar', 'sonnaar', 'track'],
    // Built-in functions
    edit: function edit(props) {
      if (player_load === '' || player_load === true) {
        player_load = false;
      }

      var clientId = props.clientId;
      var attributes = props.attributes;
      var setAttributes = props.setAttributes;
      var run_pro = attributes.run_pro;
      var show_pro_badge = run_pro ? '' : 'sonaar-music__pro-badge';
      var wc_enable = attributes.wc_enable;
      var album_id = attributes.album_id;
      var playlist_list = attributes.playlist_list;
      var playlist_show_playlist = attributes.playlist_show_playlist;
      var playlist_show_album_market = attributes.playlist_show_album_market;
      var sr_player_on_artwork = attributes.sr_player_on_artwork;
      var playlist_hide_artwork = attributes.playlist_hide_artwork;
      var playlist_show_soundwave = attributes.playlist_show_soundwave;
      var play_current_id = attributes.play_current_id;
      var enable_sticky_player = false;
      var enable_shuffle = false;
      var enable_scrollbar = false;
      var scrollbar_height = 200;
      var move_playlist_below_artwork = false;
      var track_artwork_show = false;
      var track_artwork_size = 45;
      var title_html_tag_playlist = 'h3';
      var title_color = '';
      var subtitle_color = '';
      var track_title_color = '';
      var tracklist_hover_color = '';
      var tracklist_active_color = '';
      var track_separator_color = '';
      var tracklist_spacing = 8;
      var duration_color = '';
      var title_align = 'left';
      var title_indent = 0;
      var title_fontsize = 0;
      var subtitle_fontsize = 0;
      var track_title_fontsize = 0;
      var duration_fontsize = 0;
      var store_title_fontsize = 0;
      var store_button_fontsize = 0;
      var duration_soundwave_fontsize = 0;
      var title_soundwave_fontsize = 0;
      var html_tags = [];
      var sr_alignments = [];
      var sr_text_alignments = [];
      var colors = [];
      var border_types = [];
      var title_btshow = false;
      var subtitle_btshow = false;
      var hide_number_btshow = false;
      var hide_time_duration = false;
      var play_pause_bt_show = false;
      var tracklist_controls_color = '';
      var tracklist_controls_size = 12;
      var hide_track_market = false;
      var wc_bt_show = true;
      var wc_icons_color = '';
      var wc_icons_bg_color = '';
      var view_icons_alltime = true;
      var popover_icons_store = '';
      var tracklist_icons_color = '';
      var tracklist_icons_spacing = 0;
      var tracklist_icons_size = 0;
      var title_soundwave_show = false;
      var title_html_tag_soundwave = 'div';
      var title_soundwave_color = '';
      var soundwave_show = false;
      var soundWave_progress_bar_color = '';
      var soundWave_bg_bar_color = '';
      var progressbar_inline = false;
      var duration_soundwave_show = false;
      var duration_soundwave_color = '';
      var audio_player_controls_spacebefore = 0;
      var artwork_width = 300;
      var artwork_radius = 0;
      var audio_player_artwork_controls_color = '';
      var audio_player_artwork_controls_scale = 1;
      var audio_player_controls_color = '';
      var artwork_padding = 0;
      var playlist_justify = 'center';
      var artwork_align = 'center';
      var playlist_width = 100;
      var playlist_margin = 0;
      var tracklist_margin = 0;
      var store_title_btshow = false;

      var store_title_text = __('Available now on:', 'sonaar-music');

      var store_title_color = '';
      var store_title_align = 'center';
      var album_stores_align = 'center';
      var button_text_color = '';
      var background_color = '';
      var button_hover_color = '';
      var button_background_hover_color = '';
      var button_hover_border_color = '';
      var button_border_style = 'none';
      var button_border_width = 3;
      var button_border_color = 'black';
      var button_border_radius = 0;
      var store_icon_show = false;
      var icon_font_size = 0;
      var icon_indent = 10;
      var album_stores_padding = 22;

      if (run_pro) {
        enable_sticky_player = attributes.enable_sticky_player;
        enable_shuffle = attributes.enable_shuffle;
        enable_scrollbar = attributes.enable_scrollbar;
        scrollbar_height = attributes.scrollbar_height;
        move_playlist_below_artwork = attributes.move_playlist_below_artwork;
        track_artwork_show = attributes.track_artwork_show;
        track_artwork_size = attributes.track_artwork_size;
        html_tags = attributes.html_tags;
        sr_alignments = attributes.sr_alignments;
        sr_text_alignments = attributes.sr_text_alignments;
        colors = attributes.colors;
        border_types = attributes.border_types;
        title_html_tag_playlist = attributes.title_html_tag_playlist;
        title_color = attributes.title_color;
        subtitle_color = attributes.subtitle_color;
        track_title_color = attributes.track_title_color;
        tracklist_hover_color = attributes.tracklist_hover_color;
        tracklist_active_color = attributes.tracklist_active_color;
        track_separator_color = attributes.track_separator_color;
        tracklist_spacing = attributes.tracklist_spacing;
        duration_color = attributes.duration_color;
        title_align = attributes.title_align;
        title_indent = attributes.title_indent;
        title_fontsize = attributes.title_fontsize;
        subtitle_fontsize = attributes.subtitle_fontsize;
        track_title_fontsize = attributes.track_title_fontsize;
        duration_fontsize = attributes.duration_fontsize;
        store_title_fontsize = attributes.store_title_fontsize;
        store_button_fontsize = attributes.store_button_fontsize;
        duration_soundwave_fontsize = attributes.duration_soundwave_fontsize;
        title_soundwave_fontsize = attributes.title_soundwave_fontsize;
        title_btshow = attributes.title_btshow;
        subtitle_btshow = attributes.subtitle_btshow;
        hide_number_btshow = attributes.hide_number_btshow;
        hide_time_duration = attributes.hide_time_duration;
        play_pause_bt_show = attributes.play_pause_bt_show;
        tracklist_controls_color = attributes.tracklist_controls_color;
        tracklist_controls_size = attributes.tracklist_controls_size;
        hide_track_market = attributes.hide_track_market;
        wc_bt_show = attributes.wc_bt_show;
        wc_icons_color = attributes.wc_icons_color;
        wc_icons_bg_color = attributes.wc_icons_bg_color;
        view_icons_alltime = attributes.view_icons_alltime;
        popover_icons_store = attributes.popover_icons_store;
        tracklist_icons_color = attributes.tracklist_icons_color;
        tracklist_icons_spacing = attributes.tracklist_icons_spacing;
        tracklist_icons_size = attributes.tracklist_icons_size;
        title_soundwave_show = attributes.title_soundwave_show;
        title_html_tag_soundwave = attributes.title_html_tag_soundwave;
        title_soundwave_color = attributes.title_soundwave_color;
        soundwave_show = attributes.soundwave_show;
        soundWave_progress_bar_color = attributes.soundWave_progress_bar_color;
        soundWave_bg_bar_color = attributes.soundWave_bg_bar_color;
        progressbar_inline = attributes.progressbar_inline;
        duration_soundwave_show = attributes.duration_soundwave_show;
        duration_soundwave_color = attributes.duration_soundwave_color;
        audio_player_controls_spacebefore = attributes.audio_player_controls_spacebefore;
        artwork_width = attributes.artwork_width;
        audio_player_artwork_controls_color = attributes.audio_player_artwork_controls_color;
        audio_player_artwork_controls_scale = attributes.audio_player_artwork_controls_scale;
        audio_player_controls_color = attributes.audio_player_controls_color;
        artwork_radius = attributes.artwork_radius;
        artwork_padding = attributes.artwork_padding;
        playlist_justify = attributes.playlist_justify;
        artwork_align = attributes.artwork_align;
        playlist_width = attributes.playlist_width;
        playlist_margin = attributes.playlist_margin;
        tracklist_margin = attributes.tracklist_margin;
        store_title_btshow = attributes.store_title_btshow;
        store_title_text = attributes.store_title_text;
        store_title_color = attributes.store_title_color;
        store_title_align = attributes.store_title_align;
        album_stores_align = attributes.album_stores_align;
        button_text_color = attributes.button_text_color;
        background_color = attributes.background_color;
        button_hover_color = attributes.button_hover_color;
        button_background_hover_color = attributes.button_background_hover_color;
        button_hover_border_color = attributes.button_hover_border_color;
        button_border_style = attributes.button_border_style;
        button_border_width = attributes.button_border_width;
        button_border_color = attributes.button_border_color;
        button_border_radius = attributes.button_border_radius;
        store_icon_show = attributes.store_icon_show;
        icon_font_size = attributes.icon_font_size;
        icon_indent = attributes.icon_indent;
        album_stores_padding = attributes.album_stores_padding;
      }

      var _useState = useState(false),
          _useState2 = _babel_runtime_helpers_slicedToArray__WEBPACK_IMPORTED_MODULE_0___default()(_useState, 2),
          isOpen = _useState2[0],
          setOpen = _useState2[1];

      var openGoProModal = function openGoProModal() {
        return setOpen(true);
      };

      var closeGoProModal = function closeGoProModal() {
        return setOpen(false);
      };

      var SrpModalGoPro = function SrpModalGoPro() {
        return createElement(_wordpress_element__WEBPACK_IMPORTED_MODULE_1__["Fragment"], null, isOpen && createElement(Modal, {
          title: "Pro Feature",
          onRequestClose: closeGoProModal
        }, createElement("h2", null, "Unlock MP3 Audio Player PRO"), createElement("p", null, "Get this feature and more with the Pro version of MP3 Audio Player Pro by Sonaar!"), createElement(Button, {
          href: "https://sonaar.io/free-mp3-music-player-plugin-for-wordpress/?utm_source=Sonaar+Music+Free+Plugin&utm_medium=plugin#pricing",
          target: "_blank",
          isPrimary: true
        }, "Learn More")));
      };

      if (!run_pro) {
        jQuery("body").append('<div id="sonaar-music-plugin-app"></div>');
        render(createElement(SrpModalGoPro, null), document.getElementById("sonaar-music-plugin-app"));
      }

      var ironAudioplayersLoaded = false;
      var setIronAudioplayers = setTimeout(function () {
        if (jQuery('.iron-audioplayer').length > 0) {
          ironAudioplayersLoaded = true;
        } else {
          var setIronAudioplayerInterval = setInterval(function () {
            if (jQuery('#block-' + clientId + ' .iron-audioplayer').length > 0) {
              IRON.players = [];
              jQuery('.iron-audioplayer').each(function () {
                var player = Object.create(IRON.audioPlayer);
                player.init(jQuery(this));
                IRON.players.push(player);
              });
              clearInterval(setIronAudioplayerInterval);
            }
          }, 500);
        }

        IRON.players = [];
        jQuery('#block-' + clientId + ' .iron-audioplayer').each(function () {
          var player = Object.create(IRON.audioPlayer);
          player.init(jQuery(this));
          IRON.players.push(player);
        });
      }, 2000); //Display block preview and UI

      return [createElement(InspectorControls, {
        key: 'inspector'
      }, createElement(PanelBody, {
        title: __('Player Settings', 'sonaar-music'),
        initialOpen: true
      }, createElement(SelectControl, {
        label: __('Select Playlist(s)', 'sonaar-music'),
        multiple: true,
        id: "playlist-list-id" + clientId,
        options: playlist_list,
        value: album_id,
        onChange: function onChange(value) {
          setAttributes({
            album_id: value
          });
          setIronAudioplayers;
        }
      }), createElement(ToggleControl, {
        label: __('Show Tracklist', 'sonaar-music'),
        checked: playlist_show_playlist,
        onChange: function onChange(show_playlist) {
          return setAttributes({
            playlist_show_playlist: show_playlist
          });
        }
      }), createElement(ToggleControl, {
        label: __('External Links', 'sonaar-music'),
        checked: playlist_show_album_market,
        onChange: function onChange(show_album_market) {
          return setAttributes({
            playlist_show_album_market: show_album_market
          });
        }
      }), createElement(ToggleControl, {
        label: __('Show Controls over Image Cover', 'sonaar-music'),
        checked: sr_player_on_artwork,
        onChange: function onChange(player_on_artwork) {
          return setAttributes({
            sr_player_on_artwork: player_on_artwork
          });
        }
      }), createElement(ToggleControl, {
        label: __('Hide Image Cover', 'sonaar-music'),
        checked: playlist_hide_artwork,
        onChange: function onChange(hide_artwork) {
          return setAttributes({
            playlist_hide_artwork: hide_artwork
          });
        }
      }), createElement(ToggleControl, {
        label: __('Hide Mini Player/Soundwave', 'sonaar-music'),
        checked: playlist_show_soundwave,
        onChange: function onChange(show_soundwave) {
          return setAttributes({
            playlist_show_soundwave: show_soundwave
          });
        }
      }), createElement(ToggleControl, {
        label: __('Play its own Post ID track', 'sonaar-music'),
        checked: play_current_id,
        onChange: function onChange(play_id) {
          return setAttributes({
            play_current_id: play_id
          });
        }
      }), createElement(ToggleControl, {
        label: __('Enable Sticky Audio Player', 'sonaar-music'),
        className: show_pro_badge,
        checked: enable_sticky_player,
        onChange: function onChange(sticky_player) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            enable_sticky_player: sticky_player
          });
          setIronAudioplayers;
        }
      }), createElement(ToggleControl, {
        label: __('Enable Shuffle', 'sonaar-music'),
        checked: enable_shuffle,
        className: show_pro_badge,
        onChange: function onChange(shuffle) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            enable_shuffle: shuffle
          });
          setIronAudioplayers;
        }
      }), createElement(ToggleControl, {
        label: __('Enable Scrollbar', 'sonaar-music'),
        checked: enable_scrollbar,
        className: show_pro_badge,
        onChange: function onChange(scrollbar) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            enable_scrollbar: scrollbar
          });
          setIronAudioplayers;
        }
      }), run_pro && enable_scrollbar ? createElement(RangeControl, {
        label: __('Scrollbar Height (px)', 'sonaar-music'),
        value: scrollbar_height,
        min: 0,
        max: 2000,
        onChange: function onChange(value) {
          setAttributes({
            scrollbar_height: value
          });
          setIronAudioplayers;
        }
      }) : null),
      /*( ! run_pro ) && createElement(PanelBody,{
          title: __('Go Pro','sonaar-music'),
          initialOpen: false
          },
          
          createElement( 'div', {
                  className: 'sr_gopro gb_sr_gopro elementor-nerd-box sonaar-gopro',
              },
              createElement( 'i', {
                      className: 'elementor-nerd-box-icon fa fas fa-bolt',
              }),
              createElement( RichText.Content, {
                  tagName: 'div',
                  className: 'elementor-nerd-box-title',
                  value: __( 'Meet the MP3 Player PRO', 'sonaar-music' ),
              }),
              createElement( RichText.Content, {
                  tagName: 'div',
                  className: 'elementor-nerd-box-message',
                  value: __( 'Our PRO version lets you use Style options to customize the look and feel of the player in real-time! Over 70+ options available!', 'sonaar-music' ),
              }),                            
              createElement( 'ul', {},
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Sticky Player with Soundwave', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Real-Time Style Options', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Display thumbnail beside each tracks', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Input feed URL directly in the widget', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Volume Control', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Shuffle Tracks', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Build dynamic playlist', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Tool to import/bulk create playlists', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Tracklist View', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( 'Statistic Reports', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( '1 year of support via live chat', 'sonaar-music' ),
                      }),
                  ),
                  createElement( 'li', {},
                      createElement( 'i', {
                              className: 'eicon-check',
                      }),
                      createElement( RichText.Content, {
                          value: __( '1 year of plugin updates', 'sonaar-music' ),
                      }),
                  ),
              ),
              
              createElement( RichText.Content, {
                  tagName: 'div',
                  className: 'elementor-nerd-box-message',
                  value: __( 'All those features are available with the MP3 Player\'s Pro Version.', 'sonaar-music' ),
              }),
              createElement( RichText.Content, {
                  tagName: 'a',
                  className: 'elementor-nerd-box-link elementor-go-pro button button-primary button-large',
                  href: 'https://sonaar.io/free-mp3-music-player-plugin-for-wordpress/?utm_source=Sonaar+Music+Free+Plugin&utm_medium=plugin',
                  target: '_blank',
                  value: __( 'Go Pro', 'sonaar-music' ),
              }),
          ),
      ),
      */
      !playlist_hide_artwork && createElement(PanelBody, {
        title: __('Image Cover', 'sonaar-music'),
        initialOpen: false,
        className: show_pro_badge
      }, createElement(RangeControl, {
        label: __('Image Width (px)', 'sonaar-music'),
        value: artwork_width,
        min: 1,
        max: 450,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            artwork_width: value
          });
        }
      }), createElement(RangeControl, {
        label: __('Image Padding', 'sonaar-music'),
        value: artwork_padding,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            artwork_padding: value
          });
        }
      }), createElement(RangeControl, {
        label: __('Image Radius', 'sonaar-music'),
        value: artwork_radius,
        min: 0,
        max: 300,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            artwork_radius: value
          });
        }
      }), sr_player_on_artwork && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Audio Player Controls over Image', 'sonaar-music')
      }), sr_player_on_artwork && createElement(ColorPalette, {
        label: __('Audio Player Controls over Image', 'sonaar-music'),
        colors: colors,
        value: audio_player_artwork_controls_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            audio_player_artwork_controls_color: value
          });
        }
      }), sr_player_on_artwork && createElement(RangeControl, {
        label: __('Control Size Scale', 'sonaar-music'),
        value: audio_player_artwork_controls_scale,
        min: 0,
        max: 10,
        step: 0.1,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            audio_player_artwork_controls_scale: value
          });
        }
      })), playlist_show_playlist && createElement(PanelBody, {
        title: __('Tracklist', 'sonaar-music'),
        initialOpen: false,
        className: show_pro_badge
      }, !playlist_hide_artwork && createElement(ToggleControl, {
        label: __('Move Playlist Below Artwork', 'sonaar-music'),
        checked: move_playlist_below_artwork,
        onChange: function onChange(move_playlist_artwork) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            move_playlist_below_artwork: move_playlist_artwork
          });
        }
      }), !playlist_hide_artwork && createElement('hr', {}), createElement(ToggleControl, {
        label: __('Show Thumbnail for Each Track', 'sonaar-music'),
        checked: track_artwork_show,
        onChange: function onChange(artwork_show) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            track_artwork_show: artwork_show
          });
        }
      }), sr_player_on_artwork && !playlist_hide_artwork && playlist_show_playlist && move_playlist_below_artwork && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Image Alignment', 'sonaar-music')
      }), sr_player_on_artwork && !playlist_hide_artwork && playlist_show_playlist && move_playlist_below_artwork && createElement(SelectControl, {
        options: sr_alignments,
        value: artwork_align,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            artwork_align: value
          });
        }
      }), track_artwork_show && createElement(RangeControl, {
        label: __('Thumbnail Width (px)', 'sonaar-music'),
        value: track_artwork_size,
        min: 0,
        max: 500,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            track_artwork_size: value
          });
        }
      }), createElement('hr', {}), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Playlist Container', 'sonaar-music')
      }), createElement(RangeControl, {
        label: __('Playlist Width (%)', 'sonaar-music'),
        value: playlist_width,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            playlist_width: value
          });
        }
      }), playlist_width < 91 && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Playlist Alignments', 'sonaar-music')
      }), playlist_width < 91 && createElement(SelectControl, {
        options: sr_alignments,
        value: playlist_justify,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            playlist_justify: value
          });
        }
      }), createElement(RangeControl, {
        label: __('Playlist Margin (px)', 'sonaar-music'),
        value: playlist_margin,
        min: 0,
        max: 200,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            playlist_margin: value
          });
        }
      }), createElement(RangeControl, {
        label: __('Tracklist Margin (px)', 'sonaar-music'),
        value: tracklist_margin,
        min: 0,
        max: 200,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_margin: value
          });
        }
      }), createElement('hr', {}), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Heading Settings', 'sonaar-music')
      }), createElement(ToggleControl, {
        label: __('Hide Heading', 'sonaar-music'),
        checked: title_btshow,
        onChange: function onChange(hide_title) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_btshow: hide_title
          });
        }
      }), run_pro && !title_btshow && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('HTML Heading Tag', 'sonaar-music')
      }), run_pro && !title_btshow && createElement(SelectControl, {
        options: html_tags,
        value: title_html_tag_playlist,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_html_tag_playlist: value
          });
        }
      }), !title_btshow && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Heading Color', 'sonaar-music')
      }), !title_btshow && createElement(ColorPalette, {
        label: __('Heading Color', 'sonaar-music'),
        colors: colors,
        value: title_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_color: value
          });
        }
      }), !title_btshow && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Heading Alignment', 'sonaar-music')
      }), !title_btshow && createElement(SelectControl, {
        options: sr_text_alignments,
        value: title_align,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_align: value
          });
        }
      }), !title_btshow && createElement(RangeControl, {
        label: __('Heading Fontsize (px)', 'sonaar-music'),
        value: title_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_fontsize: value
          });
        }
      }), !title_btshow && createElement(RangeControl, {
        label: __('Heading Indent (px)', 'sonaar-music'),
        value: title_indent,
        min: -500,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_indent: value
          });
        }
      }), createElement('hr', {}), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Subheading Settings', 'sonaar-music')
      }), createElement(ToggleControl, {
        label: __('Hide Subheading', 'sonaar-music'),
        checked: subtitle_btshow,
        onChange: function onChange(hide_subtitle) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            subtitle_btshow: hide_subtitle
          });
        }
      }), !subtitle_btshow && createElement(RangeControl, {
        label: __('Subheading Fontsize (px)', 'sonaar-music'),
        value: subtitle_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            subtitle_fontsize: value
          });
        }
      }), !subtitle_btshow && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Subheading Color', 'sonaar-music')
      }), !subtitle_btshow && createElement(ColorPalette, {
        label: __('Subheading Color', 'sonaar-music'),
        colors: colors,
        value: subtitle_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            subtitle_color: value
          });
        }
      }), createElement('hr', {}), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Track Settings', 'sonaar-music')
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Track Title Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Track Title Color', 'sonaar-music'),
        colors: colors,
        value: track_title_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            track_title_color: value
          });
        }
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Track Title Hover Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Track Title Hover Color', 'sonaar-music'),
        colors: colors,
        value: tracklist_hover_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_hover_color: value
          });
        }
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Track Title Active Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Track Title Active Color', 'sonaar-music'),
        colors: colors,
        value: tracklist_active_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_active_color: value
          });
        }
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Track Separator Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Track Separator Color', 'sonaar-music'),
        colors: colors,
        value: track_separator_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            track_separator_color: value
          });
        }
      }), createElement('hr', {}), createElement(RangeControl, {
        label: __('Track Title Fontsize (px)', 'sonaar-music'),
        value: track_title_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            track_title_fontsize: value
          });
        }
      }), createElement(RangeControl, {
        label: __('Track Spacing (px)', 'sonaar-music'),
        value: tracklist_spacing,
        min: 0,
        max: 50,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_spacing: value
          });
        }
      }), createElement('hr', {}), createElement(ToggleControl, {
        label: __('Hide Track Number', 'sonaar-music'),
        checked: hide_number_btshow,
        onChange: function onChange(hide_track_num) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            hide_number_btshow: hide_track_num
          });
        }
      }), createElement('hr', {}), createElement(ToggleControl, {
        label: __('Hide Track Duration', 'sonaar-music'),
        checked: hide_time_duration,
        onChange: function onChange(hide_time) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            hide_time_duration: hide_time
          });
        }
      }), !hide_time_duration && createElement(RangeControl, {
        label: __('Duration Fontsize (px)', 'sonaar-music'),
        value: duration_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            duration_fontsize: value
          });
        }
      }), !hide_time_duration && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Time Duration Color', 'sonaar-music')
      }), !hide_time_duration && createElement(ColorPalette, {
        label: __('Time Duration Color', 'sonaar-music'),
        colors: colors,
        value: duration_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            duration_color: value
          });
        }
      }), createElement('hr', {}), createElement(ToggleControl, {
        label: __('Hide Play/Pause Button', 'sonaar-music'),
        checked: play_pause_bt_show,
        onChange: function onChange(hide_play_pause) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            play_pause_bt_show: hide_play_pause
          });
        }
      }), !play_pause_bt_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Play/Pause Button Color', 'sonaar-music')
      }), !play_pause_bt_show && createElement(ColorPalette, {
        label: __('Play/Pause Button Color', 'sonaar-music'),
        colors: colors,
        value: tracklist_controls_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_controls_color: value
          });
        }
      }), !play_pause_bt_show && createElement(RangeControl, {
        label: __('Play/Pause Button Size (px)', 'sonaar-music'),
        value: tracklist_controls_size,
        min: 0,
        max: 50,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_controls_size: value
          });
        }
      }), createElement('hr', {}), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Call-to-Action Icons', 'sonaar-music')
      }), createElement(ToggleControl, {
        label: __('Hide Track\'s Call-to-Action(s)', 'sonaar-music'),
        checked: hide_track_market,
        onChange: function onChange(track_market) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            hide_track_market: track_market
          });
        }
      }), !hide_track_market && createElement(ToggleControl, {
        label: __('Display Icons without the 3 dots hover', 'sonaar-music'),
        checked: view_icons_alltime,
        onChange: function onChange(icons_alltime) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            view_icons_alltime: icons_alltime
          });
        }
      }), !hide_track_market && !view_icons_alltime && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('3 Dots Color', 'sonaar-music')
      }), !hide_track_market && !view_icons_alltime && createElement(ColorPalette, {
        label: __('3 Dots Color', 'sonaar-music'),
        colors: colors,
        value: popover_icons_store,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            popover_icons_store: value
          });
        }
      }), !hide_track_market && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Icons Color', 'sonaar-music')
      }), !hide_track_market && createElement(ColorPalette, {
        label: __('Icons Color', 'sonaar-music'),
        colors: colors,
        value: tracklist_icons_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_icons_color: value
          });
        }
      }), !hide_track_market && createElement(RangeControl, {
        label: __('Icon Spacing (px)', 'sonaar-music'),
        value: tracklist_icons_spacing,
        min: 0,
        max: 50,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_icons_spacing: value
          });
        }
      }), !hide_track_market && createElement(RangeControl, {
        label: __('Icon Size (px)', 'sonaar-music'),
        value: tracklist_icons_size,
        min: 0,
        max: 50,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            tracklist_icons_size: value
          });
        }
      }), wc_enable && !hide_track_market && createElement('hr', {}), wc_enable && !hide_track_market && createElement(ToggleControl, {
        label: __('WooCommerce Hide Icons', 'sonaar-music'),
        checked: wc_bt_show,
        onChange: function onChange(wc_btn_show) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            wc_bt_show: wc_btn_show
          });
        }
      }), wc_enable && wc_bt_show && !hide_track_market && createElement(ColorPalette, {
        label: __('WooCommerce Cart Icons Color', 'sonaar-music'),
        colors: colors,
        value: wc_icons_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            wc_icons_color: value
          });
        }
      }), wc_enable && wc_bt_show && !hide_track_market && createElement(ColorPalette, {
        label: __('WooCommerce Cart Icons Background', 'sonaar-music'),
        colors: colors,
        value: wc_icons_bg_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            wc_icons_bg_color: value
          });
        }
      })), createElement(PanelBody, {
        title: __('External Links', 'sonaar-music'),
        initialOpen: false,
        className: show_pro_badge
      }, createElement(ToggleControl, {
        label: __('Hide Heading', 'sonaar-music'),
        checked: store_title_btshow,
        onChange: function onChange(store_title_hide) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            store_title_btshow: store_title_hide
          });
        }
      }), !store_title_btshow && createElement(TextControl, {
        label: __('Heading text', 'sonaar-music'),
        value: store_title_text,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            store_title_text: value
          });
        }
      }), !store_title_btshow && createElement(RangeControl, {
        label: __('Heading Fontsize (px)', 'sonaar-music'),
        value: store_title_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            store_title_fontsize: value
          });
        }
      }), !store_title_btshow && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Heading Color', 'sonaar-music')
      }), !store_title_btshow && createElement(ColorPalette, {
        label: __('Heading Color', 'sonaar-music'),
        colors: colors,
        value: store_title_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            store_title_color: value
          });
        }
      }), !store_title_btshow && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Heading Alignment', 'sonaar-music')
      }), !store_title_btshow && createElement(SelectControl, {
        options: sr_alignments,
        value: store_title_align,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            store_title_align: value
          });
        }
      }), createElement('hr', {}), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Links Alignment', 'sonaar-music')
      }), createElement(SelectControl, {
        options: sr_alignments,
        value: album_stores_align,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            album_stores_align: value
          });
        }
      }), createElement(RangeControl, {
        label: __('Store Button Fontsize (px)', 'sonaar-music'),
        value: store_button_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            store_button_fontsize: value
          });
        }
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Text Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Text Color', 'sonaar-music'),
        colors: colors,
        value: button_text_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_text_color: value
          });
        }
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Button Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Button Color', 'sonaar-music'),
        colors: colors,
        value: background_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            background_color: value
          });
        }
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Text Hover Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Text Hover Color', 'sonaar-music'),
        colors: colors,
        value: button_hover_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_hover_color: value
          });
        }
      }), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Button Hover Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Button Hover Color', 'sonaar-music'),
        colors: colors,
        value: button_background_hover_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_background_hover_color: value
          });
        }
      }), createElement('hr', {}), createElement(SelectControl, {
        options: border_types,
        value: button_border_style,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_border_style: value
          });
        }
      }), button_border_style != 'none' && createElement(RangeControl, {
        label: __('Button Border Width (px)', 'sonaar-music'),
        value: button_border_width,
        min: 0,
        max: 50,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_border_width: value
          });
        }
      }), button_border_style != 'none' && createElement(ColorPalette, {
        label: __('Button Border Color', 'sonaar-music'),
        colors: colors,
        value: button_border_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_border_color: value
          });
        }
      }), button_border_style != 'none' && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Button Border Hover Color', 'sonaar-music')
      }), button_border_style != 'none' && createElement(ColorPalette, {
        label: __('Button Border Hover Color', 'sonaar-music'),
        colors: colors,
        value: button_hover_border_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_hover_border_color: value
          });
        }
      }), button_border_style != 'none' && createElement(RangeControl, {
        label: __('Button Radius (px)', 'sonaar-music'),
        value: button_border_radius,
        min: 0,
        max: 30,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            button_border_radius: value
          });
        }
      }), createElement('hr', {}), createElement(ToggleControl, {
        label: __('Hide Heading', 'sonaar-music'),
        checked: store_icon_show,
        onChange: function onChange(store_icon_hide) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            store_icon_show: store_icon_hide
          });
        }
      }), !store_icon_show && createElement(RangeControl, {
        label: __('Icon Font Size (px)', 'sonaar-music'),
        value: icon_font_size,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            icon_font_size: value
          });
        }
      }), !store_icon_show && createElement(RangeControl, {
        label: __('Icon Spacing (px)', 'sonaar-music'),
        value: icon_indent,
        min: 0,
        max: 50,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            icon_indent: value
          });
        }
      }), !store_icon_show && createElement(RangeControl, {
        label: __('Link Buttons Margin (px)', 'sonaar-music'),
        value: album_stores_padding,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            album_stores_padding: value
          });
        }
      })), !playlist_show_soundwave && createElement(PanelBody, {
        title: __('Mini Player & Soundwave', 'sonaar-music'),
        initialOpen: false,
        className: show_pro_badge
      }, !title_soundwave_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Soundwave Heading', 'sonaar-music')
      }), createElement(ToggleControl, {
        label: __('Hide Heading', 'sonaar-music'),
        checked: title_soundwave_show,
        onChange: function onChange(hide_title_soundwave) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_soundwave_show: hide_title_soundwave
          });
        }
      }), run_pro && !title_soundwave_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('HTML Heading Tag', 'sonaar-music')
      }), run_pro && !title_soundwave_show && createElement(SelectControl, {
        options: html_tags,
        value: title_html_tag_soundwave,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_html_tag_soundwave: value
          });
        }
      }), !title_soundwave_show && createElement(RangeControl, {
        label: __('Heading Fontsize (px)', 'sonaar-music'),
        value: title_soundwave_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_soundwave_fontsize: value
          });
        }
      }), !title_soundwave_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Heading Color', 'sonaar-music')
      }), !title_soundwave_show && createElement(ColorPalette, {
        label: __('Heading Color', 'sonaar-music'),
        colors: colors,
        value: title_soundwave_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            title_soundwave_color: value
          });
        }
      }), createElement('hr', {}), !title_soundwave_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Soundwave', 'sonaar-music')
      }), createElement(ToggleControl, {
        label: __('Hide SoundWave', 'sonaar-music'),
        checked: soundwave_show,
        onChange: function onChange(hide_soundwave) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            soundwave_show: hide_soundwave
          });
        }
      }), !soundwave_show && createElement(ToggleControl, {
        label: __('Inline Progress Bar', 'sonaar-music'),
        checked: progressbar_inline,
        onChange: function onChange(progressbar_inline_show) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            progressbar_inline: progressbar_inline_show
          });
        }
      }), !soundwave_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('SoundWave Progress Bar Color', 'sonaar-music')
      }), !soundwave_show && createElement(ColorPalette, {
        label: __('SoundWave Progress Bar Color', 'sonaar-music'),
        colors: colors,
        value: soundWave_progress_bar_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            soundWave_progress_bar_color: value
          });
        }
      }), !soundwave_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('SoundWave Background Color', 'sonaar-music')
      }), !soundwave_show && createElement(ColorPalette, {
        label: __('SoundWave Background Color', 'sonaar-music'),
        colors: colors,
        value: soundWave_bg_bar_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            soundWave_bg_bar_color: value
          });
        }
      }), !soundwave_show && createElement('hr', {}), !soundwave_show && createElement(ToggleControl, {
        label: __('Hide Time Durations', 'sonaar-music'),
        checked: duration_soundwave_show,
        onChange: function onChange(duration_soundwave_hide) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            duration_soundwave_show: duration_soundwave_hide
          });
        }
      }), !soundwave_show && !duration_soundwave_show && createElement(RangeControl, {
        label: __('Time Fontsize (px)', 'sonaar-music'),
        value: duration_soundwave_fontsize,
        min: 0,
        max: 100,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            duration_soundwave_fontsize: value
          });
        }
      }), !soundwave_show && !duration_soundwave_show && createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Time Color', 'sonaar-music')
      }), !soundwave_show && !duration_soundwave_show && createElement(ColorPalette, {
        label: __('Time Color', 'sonaar-music'),
        colors: colors,
        value: duration_soundwave_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            duration_soundwave_color: value
          });
        }
      }), createElement('hr', {}), createElement(RichText.Content, {
        tagName: 'label',
        className: 'components-base-control__label sonaar-block-label',
        value: __('Audio Player Controls Color', 'sonaar-music')
      }), createElement(ColorPalette, {
        label: __('Audio Player Controls Color', 'sonaar-music'),
        colors: colors,
        value: audio_player_controls_color,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            audio_player_controls_color: value
          });
        }
      }), !progressbar_inline && createElement(RangeControl, {
        label: __('Audio Player Controls Space Before (px)', 'sonaar-music'),
        value: audio_player_controls_spacebefore,
        min: -500,
        max: 200,
        onChange: function onChange(value) {
          if (!run_pro) {
            openGoProModal();
            return;
          }

          setAttributes({
            audio_player_controls_spacebefore: value
          });
          setIronAudioplayers;
        }
      }))), createElement(serverSideRender, {
        block: 'sonaar/sonaar-block',
        attributes: {
          album_id: album_id,
          playlist_show_playlist: playlist_show_playlist,
          playlist_show_album_market: playlist_show_album_market,
          sr_player_on_artwork: sr_player_on_artwork,
          playlist_hide_artwork: playlist_hide_artwork,
          playlist_show_soundwave: playlist_show_soundwave,
          play_current_id: play_current_id,
          move_playlist_below_artwork: move_playlist_below_artwork,
          track_artwork_show: track_artwork_show,
          track_artwork_size: track_artwork_size,
          title_btshow: title_btshow,
          title_html_tag_playlist: title_html_tag_playlist,
          title_color: title_color,
          subtitle_color: subtitle_color,
          track_title_color: track_title_color,
          tracklist_hover_color: tracklist_hover_color,
          tracklist_active_color: tracklist_active_color,
          track_separator_color: track_separator_color,
          tracklist_spacing: tracklist_spacing,
          duration_color: duration_color,
          title_align: title_align,
          title_fontsize: title_fontsize,
          subtitle_fontsize: subtitle_fontsize,
          track_title_fontsize: track_title_fontsize,
          duration_fontsize: duration_fontsize,
          store_title_fontsize: store_title_fontsize,
          store_button_fontsize: store_button_fontsize,
          duration_soundwave_fontsize: duration_soundwave_fontsize,
          title_soundwave_fontsize: title_soundwave_fontsize,
          title_indent: title_indent,
          subtitle_btshow: subtitle_btshow,
          hide_number_btshow: hide_number_btshow,
          hide_time_duration: hide_time_duration,
          play_pause_bt_show: play_pause_bt_show,
          tracklist_controls_color: tracklist_controls_color,
          tracklist_controls_size: tracklist_controls_size,
          hide_track_market: hide_track_market,
          wc_bt_show: wc_bt_show,
          wc_icons_color: wc_icons_color,
          wc_icons_bg_color: wc_icons_bg_color,
          view_icons_alltime: view_icons_alltime,
          popover_icons_store: popover_icons_store,
          tracklist_icons_color: tracklist_icons_color,
          tracklist_icons_spacing: tracklist_icons_spacing,
          tracklist_icons_size: tracklist_icons_size,
          title_soundwave_show: title_soundwave_show,
          title_html_tag_soundwave: title_html_tag_soundwave,
          title_soundwave_color: title_soundwave_color,
          soundwave_show: soundwave_show,
          soundWave_progress_bar_color: soundWave_progress_bar_color,
          soundWave_bg_bar_color: soundWave_bg_bar_color,
          progressbar_inline: progressbar_inline,
          duration_soundwave_show: duration_soundwave_show,
          duration_soundwave_color: duration_soundwave_color,
          audio_player_controls_spacebefore: audio_player_controls_spacebefore,
          store_title_btshow: store_title_btshow,
          store_title_text: store_title_text,
          store_title_color: store_title_color,
          store_title_align: store_title_align,
          album_stores_align: album_stores_align,
          button_text_color: button_text_color,
          background_color: background_color,
          button_hover_color: button_hover_color,
          button_background_hover_color: button_background_hover_color,
          button_hover_border_color: button_hover_border_color,
          button_border_style: button_border_style,
          button_border_width: button_border_width,
          button_border_color: button_border_color,
          button_border_radius: button_border_radius,
          store_icon_show: store_icon_show,
          icon_font_size: icon_font_size,
          icon_indent: icon_indent,
          album_stores_padding: album_stores_padding,
          artwork_width: artwork_width,
          artwork_padding: artwork_padding,
          artwork_radius: artwork_radius,
          audio_player_artwork_controls_color: audio_player_artwork_controls_color,
          audio_player_artwork_controls_scale: audio_player_artwork_controls_scale,
          audio_player_controls_color: audio_player_controls_color,
          playlist_justify: playlist_justify,
          artwork_align: artwork_align,
          playlist_width: playlist_width,
          playlist_margin: playlist_margin,
          tracklist_margin: tracklist_margin,
          enable_sticky_player: enable_sticky_player,
          enable_shuffle: enable_shuffle,
          enable_scrollbar: enable_scrollbar,
          scrollbar_height: scrollbar_height
        }
      })];
    },
    save: function save() {
      return null; //save has to exist. This all we need
    }
  });
})(window.wp);

/***/ }),

/***/ "@wordpress/element":
/*!******************************************!*\
  !*** external {"this":["wp","element"]} ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

(function() { module.exports = this["wp"]["element"]; }());

/***/ })

/******/ });
//# sourceMappingURL=index.js.map